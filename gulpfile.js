const elixir = require('laravel-elixir');

require('laravel-elixir-imagemin');


// Config
var config = elixir.config;
config.versioning.buildFolder = '';


elixir(mix => {
  elixir.config.versioning.buildFolder = 'build';

  // Images
  mix.imagemin('resources/assets/images/**/*', config.publicPath + '/images', {
    optimizationLevel: 5,
    progressive: true,
    interlaced: true
  });

  // Styles
  mix.sass('app.scss');

  // Scripts
  mix.webpack('app.js');
  mix.webpack('home.js');



  // Browser Sync
  mix.browserSync({
    middleware: function (req, res, next) {
      res.setHeader('Access-Control-Allow-Origin', '*');
      next();
    },
    proxy: {
      target: 'localhost:8000',
      middleware: function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        next();
      }
    }
  });

  // Fonts
  mix.copy('resources/assets/fonts/**/*.*', config.publicPath + '/fonts');

  // Video
  mix.copy('resources/assets/video/*.*', config.publicPath + '/video');

  // Audio
  mix.copy('resources/assets/audio/*.*', config.publicPath + '/audio');

  /** VERSIONS */
  // mix.version([
  //   'js/app.js',
  //   'js/home.js',
  //   'js/catalog.js',
  //   'css/app.css'
  // ]);



});
