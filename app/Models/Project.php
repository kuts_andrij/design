<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'as_projects';
	
	protected $guarded = [];

	// Name
	public function getNameAttribute()
	{
		$locale = \App::getLocale();
		$column = 'name_' . $locale;
		return $this->{$column};
    }

    // Object
	public function getObjectAttribute()
	{
		$locale = \App::getLocale();
		$column = 'object_' . $locale;
		return $this->{$column};
    }

    // Location
	public function getLocationAttribute()
	{
		$locale = \App::getLocale();
		$column = 'location_' . $locale;
		return $this->{$column};
    }

    // Space
	public function getSpaceAttribute()
	{
		$locale = \App::getLocale();
		$column = 'space_' . $locale;
		return $this->{$column};
    }

	// Client
	public function getClientAttribute()
	{
		$locale = \App::getLocale();
		$column = 'client_' . $locale;
		return $this->{$column};
    }
	
	// Photographer
	public function getPhotographerAttribute()
	{
		$locale = \App::getLocale();
		$column = 'photographer_' . $locale;
		return $this->{$column};
    }

    // Annotation
	public function getAnnotationAttribute()
	{
		$locale = \App::getLocale();
		$column = 'annotation_' . $locale;
		return $this->{$column};
    }

    // Body slider
	public function getBodySliderAttribute()
	{
		$locale = \App::getLocale();
		$column = 'body_slider_' . $locale;
		return $this->{$column};
    }

    // Body gallery
	public function getBodyGalleryAttribute()
	{
		$locale = \App::getLocale();
		$column = 'body_gallery_' . $locale;
		return $this->{$column};
    }

    // Body video
	public function getBodyVideoAttribute()
	{
		$locale = \App::getLocale();
		$column = 'body_video_' . $locale;
		return $this->{$column};
    }
	
	// Meta title
	public function getMetaTitleAttribute()
	{
		$locale = \App::getLocale();
		$column = 'meta_title_' . $locale;
		return $this->{$column};
    }
	
	// Meta keywords
	public function getMetaKeywordsAttribute()
	{
		$locale = \App::getLocale();
		$column = 'meta_keywords_' . $locale;
		return $this->{$column};
    }
	
	// Meta description
	public function getMetaDescriptionAttribute()
	{
		$locale = \App::getLocale();
		$column = 'meta_description_' . $locale;
		return $this->{$column};
    }

    // Upload video screen
    public static function upload_screen( $file, $id )
    {
		if( $old_screen = self::find( $id )->video_screen )
			\File::delete( public_path() . '/uploads/projects/video/screenshots/' . $old_screen );
		
		$file_name = time() . '_' . \Slug::make(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension())) . '.' . $file->getClientOriginalExtension();
		
		\Image::make( $file )->save( 'uploads/projects/video/screenshots/' . $file_name );
		
		self::find( $id )->update(['video_screen' => $file_name]);
    }

    // Upload video file
    public static function upload_video( $file, $id )
    {
    	if( $old_video = self::find( $id )->video_file )
			\File::delete( public_path() . '/uploads/projects/video/' . $old_video );
		
		$file_name = time() . '_' . \Slug::make(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension())) . '.' . $file->getClientOriginalExtension();
		
		$file->move( public_path() . '/uploads/projects/video/', $file_name );

		self::find( $id )->update(['video_file' => $file_name]);
    }

    // All images project
	public function images()
	{
		return $this->hasMany( 'App\Models\ProjectImage' , 'project_id' );
	}

	// All images project JSON
	public static function json_images( $images )
	{
		$images_json = [];
		if( count($images) )
		{
			foreach( $images as $item )
			{
				$images_json[] = [
									'src'  => \URL::to('uploads/projects/images/original/' . $item->image),
									'opts' => ['caption' => $item->caption]
								 ];
			}
		}
		return json_encode($images_json);
	}

	// Project youtube video code
	public static function video_code( $video_url )
	{
		parse_str( parse_url($video_url, PHP_URL_QUERY), $code );
		return $code['v'];
	}

	// Projects JSON data Google Map
	public static function json_points_map()
	{
		$projects = self::select('object_point', 'name_' . \App::getLocale(), 'slug' )
					  	->whereVisible( 1 )
					  	->get();

		$points_json = [];
		if( count($projects) )
		{
			foreach( $projects as $project )
			{
				if( $project->object_point )
				{
					$point = explode(' ', $project->object_point);

					$points_json[] = [
										'location' => ['lat' => trim($point[0]), 'lng' => trim($point[1])],
										'title'	   => $project->name,
										'url'	   => \LaravelLocalization::getLocalizedURL( \App::getLocale(), '/project/' . $project->slug )
									 ];
				}
			}
		}
		return json_encode($points_json);
	}
	
	// Project check honors
	public static function check_project_honors( $images )
	{
		$flag = FALSE;
		
		if( count($images) )
		{
			foreach( $images as $image )
			{
				if( $image->type == 'honors' )
				{
					$flag = TRUE;
				}
			}
		}
		
		return $flag;
	}
}
