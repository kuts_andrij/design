<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectImage extends Model
{
    protected $table = 'as_projects_images';
	
	protected $guarded = [];

	// Alt
	public function getAltAttribute()
	{
		$locale = \App::getLocale();
		$column = 'alt_' . $locale;
		return $this->{$column};
    }

    // Title
	public function getTitleAttribute()
	{
		$locale = \App::getLocale();
		$column = 'title_' . $locale;
		return $this->{$column};
    }

    // Caption
	public function getCaptionAttribute()
	{
		$locale = \App::getLocale();
		$column = 'caption_' . $locale;
		return $this->{$column};
    }

    // Upload images project
    public static function upload_images( $type, $images, $project_id )
    {
    	$errors  = '';
    	$counter = 1;

		$exist_active_image = self::whereProjectId( $project_id )
								  ->whereType( $type )
								  ->whereActive( 1 )
								  ->count();

		foreach( $images as $image )
		{
			$validator = \Validator::make( ['images' => $image], ['images' => 'required|image']);
			
			if( $validator->passes() )
			{
				$image_name = time() . '_' . \Slug::make(basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension())) . '.' . $image->getClientOriginalExtension();

				\Image::make( $image )->save( 'uploads/projects/images/original/' . $image_name );
				\Image::make( $image )->widen( 580 )->save( 'uploads/projects/images/middle/' . $image_name );
				
				$thumbnail_width = intval( 200 );
				switch( $type )
				{
					case 'slider':
						$thumbnail_width = intval( 152 );
					break;
					case 'honors':
						$thumbnail_width = intval( 94 );
					break;
				}

				\Image::make( $image )->widen( $thumbnail_width )->save( 'uploads/projects/images/thumbnail/' . $image_name );

				// General image
				$active = 0;
				if( $counter == 1 && !$exist_active_image )
				{
					$active = 1;
				}

				self::create(['project_id' => $project_id, 'type' => $type, 'image' => $image_name, 'active' => $active ]);
			}
			else
				$errors = $validator->messages();

			$counter++;
		}
		
		return $errors;
	}

	// General image project
	public static function active_image( $project_id, $image_id, $type )
	{
		$active_image = self::find( $image_id );
			
		$active = (empty($active_image->active) ? 1 : 0);
			
		$active_image->update(['active' => $active]);

		$images = self::whereType( $type )
					  ->whereProjectId( $project_id )
					  ->get();
		
		foreach( $images as $image )
		{
			if( $image->id != $image_id )
				self::find( $image->id )->update(['active' => 0]);
		}
	}

	// Sortable images project
	public static function sortable_images( $ids )
	{
		(int)$i = 1;
		
		foreach( $ids as $id )
		{
			self::find( $id )->update(['position' => $i]);
			$i++;
		}
	}

	// Delete images project
	public static function delete_images( $ids )
	{
		$images = self::whereIn('id', $ids)->get();

		foreach( $images as $item )
		{
			\File::delete( public_path() . '/uploads/projects/images/original/' . $item->image );
			\File::delete( public_path() . '/uploads/projects/images/middle/' . $item->image );
			\File::delete( public_path() . '/uploads/projects/images/thumbnail/' . $item->image );
		}
		
		self::destroy( $ids );
	}
}
