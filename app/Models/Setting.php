<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'as_settings';
	
	protected $guarded = [];
	
	// Адрес
	public function getAddressAttribute()
	{
		$locale = \App::getLocale();
		$column = 'address_' . $locale;
		return $this->{$column};
    }
}
