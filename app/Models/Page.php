<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'as_pages';
	
	protected $guarded = [];

	// Name
	public function getNameAttribute()
	{
		$locale = \App::getLocale();
		$column = 'name_' . $locale;
		return $this->{$column};
    }

    // Annotation
	public function getAnnotationAttribute()
	{
		$locale = \App::getLocale();
		$column = 'annotation_' . $locale;
		return $this->{$column};
    }

    // Body
	public function getBodyAttribute()
	{
		$locale = \App::getLocale();
		$column = 'body_' . $locale;
		return $this->{$column};
    }
	
	// Meta title
	public function getMetaTitleAttribute()
	{
		$locale = \App::getLocale();
		$column = 'meta_title_' . $locale;
		return $this->{$column};
    }
	
	// Meta keywords
	public function getMetaKeywordsAttribute()
	{
		$locale = \App::getLocale();
		$column = 'meta_keywords_' . $locale;
		return $this->{$column};
    }
	
	// Meta description
	public function getMetaDescriptionAttribute()
	{
		$locale = \App::getLocale();
		$column = 'meta_description_' . $locale;
		return $this->{$column};
    }

    // Upload image
    public static function upload_image( $file, $id )
    {
		if( $old_image = self::find( $id )->image )
			\File::delete( public_path() . '/uploads/pages/images/' . $old_image );
		
		$file_name = time() . '_' . \Slug::make(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension())) . '.' . $file->getClientOriginalExtension();
		
		\Image::make( $file )->save( 'uploads/pages/images/' . $file_name );
		
		self::find( $id )->update(['image' => $file_name]);
    }

    // Upload video file
    public static function upload_video( $file, $id )
    {
    	if( $old_video = self::find( $id )->video_file )
			\File::delete( public_path() . '/uploads/pages/video/' . $old_video );
		
		$file_name = time() . '_' . \Slug::make(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension())) . '.' . $file->getClientOriginalExtension();
		
		$file->move( public_path() . '/uploads/pages/video/', $file_name );

		self::find( $id )->update(['video_file' => $file_name]);
    }
}
