<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutPage extends Model
{
    protected $table = 'as_about_page';
	
	protected $guarded = [];

	// Текст для первого года
	public function getYear1TextAttribute()
	{
		$locale = \App::getLocale();
		$column = 'year_1_text_' . $locale;
		return $this->{$column};
    }

    // Текст для второго года
	public function getYear2TextAttribute()
	{
		$locale = \App::getLocale();
		$column = 'year_2_text_' . $locale;
		return $this->{$column};
    }

	// Текст для третьего года
	public function getYear3TextAttribute()
	{
		$locale = \App::getLocale();
		$column = 'year_3_text_' . $locale;
		return $this->{$column};
    }

    // Обучение и развитие (заглавие)
    public function getEducationDevTitleAttribute()
    {
    	$locale = \App::getLocale();
		$column = 'education_dev_title_' . $locale;
		return $this->{$column};
    }

	// Обучение и развитие (текст)
    public function getEducationDevTextAttribute()
    {
    	$locale = \App::getLocale();
		$column = 'education_dev_text_' . $locale;
		return $this->{$column};
    }

    // Философия (1 текст)
    public function getPhilosophy1TextAttribute()
    {
    	$locale = \App::getLocale();
		$column = 'philosophy_1_text_' . $locale;
		return $this->{$column};
    }

    // Философия (2 текст)
    public function getPhilosophy2TextAttribute()
    {
    	$locale = \App::getLocale();
		$column = 'philosophy_2_text_' . $locale;
		return $this->{$column};
    }

	// Философия (3 текст)
    public function getPhilosophy3TextAttribute()
    {
    	$locale = \App::getLocale();
		$column = 'philosophy_3_text_' . $locale;
		return $this->{$column};
    }

    // Философия (4 текст)
    public function getPhilosophy4TextAttribute()
    {
    	$locale = \App::getLocale();
		$column = 'philosophy_4_text_' . $locale;
		return $this->{$column};
    }

    // Как мы работаем? (1 текст)
    public function getHowWork1TextAttribute()
    {
    	$locale = \App::getLocale();
		$column = 'how_work_1_text_' . $locale;
		return $this->{$column};
    }

    // Как мы работаем? (2 текст)
    public function getHowWork2TextAttribute()
    {
    	$locale = \App::getLocale();
		$column = 'how_work_2_text_' . $locale;
		return $this->{$column};
    }

    // Как мы работаем? (3 текст)
    public function getHowWork3TextAttribute()
    {
    	$locale = \App::getLocale();
		$column = 'how_work_3_text_' . $locale;
		return $this->{$column};
    }

    // Как мы работаем? (4 текст)
    public function getHowWork4TextAttribute()
    {
    	$locale = \App::getLocale();
		$column = 'how_work_4_text_' . $locale;
		return $this->{$column};
    }
}
