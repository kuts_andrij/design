<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;

class Menu extends Node
{
	protected $table = 'as_menu';
	
	protected $guarded = [];

	// Название меню
	public function getNameAttribute()
	{
		$locale = \App::getLocale();
		$column = 'name_' . $locale;
		return $this->{$column};
    }

    # Frontend

    // Item menu url
    public static function itemMenuURL( $url )
    {
    	switch( $url )
    	{
			case '':
				$url = \LaravelLocalization::getLocalizedURL( \App::getLocale(), '/' );
			break;
			case '#':
				$url = '#';
			break;
			default:
				$url = \LaravelLocalization::getLocalizedURL( \App::getLocale(), $url );
		}

		return $url;
    }

    // Active menu element
    public static function itemMenuActive( $url, $page_id = null )
    {
    	$class = '';
    	
    	$localized_url = \LaravelLocalization::getLocalizedURL( \App::getLocale(), $url );
    	$localized_current_url = \LaravelLocalization::getLocalizedURL( \App::getLocale() );

        // if GET parametr?
        if( strpos($localized_current_url, '?') !== false )
            $localized_current_url = strtok($localized_current_url, '?');

    	// $page_id -> 2 - Home page
    	if( ($localized_url == $localized_current_url && $url != '#' && $url != '') || ($page_id == 2 && !$url) )
    		$class = 'is-active';

    	return $class;
    }
}
