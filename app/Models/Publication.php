<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
	protected $table = 'as_publications';
	
	protected $guarded = [];

	// Alt
	public function getAltAttribute()
	{
		$locale = \App::getLocale();
		$column = 'alt_' . $locale;
		return $this->{$column};
    }

    // Title
	public function getTitleAttribute()
	{
		$locale = \App::getLocale();
		$column = 'title_' . $locale;
		return $this->{$column};
    }

    // Upload images
    public static function upload_images( $images )
    {
		$errors = '';

		foreach( $images as $image )
		{
			$validator = \Validator::make( ['images' => $image], ['images' => 'required|image']);
			
			if( $validator->passes() )
			{
				$image_name = time() . '_' . \Slug::make(basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension())) . '.' . $image->getClientOriginalExtension();

				\Image::make( $image )->save( 'uploads/publications/original/' . $image_name );
				\Image::make( $image )->widen( 135 )->save( 'uploads/publications/thumbnail/' . $image_name );

				self::create(['image' => $image_name]);
			}
			else
				$errors = $validator->messages();
		}
		
		return $errors;
	}

	// Sortable images
	public static function sortable_images( $ids )
	{
		(int)$i = 1;
		
		foreach( $ids as $id )
		{
			self::find( $id )->update(['position' => $i]);
			$i++;
		}
	}

	// Delete images
	public static function delete_images( $ids )
	{
		$images = self::whereIn('id', $ids)->get();

		foreach( $images as $item )
		{
			\File::delete( public_path() . '/uploads/publications/original/' . $item->image );
			\File::delete( public_path() . '/uploads/publications/thumbnail/' . $item->image );
		}
		
		self::destroy( $ids );
	}
}
