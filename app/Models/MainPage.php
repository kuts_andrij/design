<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainPage extends Model
{
	protected $table = 'as_main_page';
	
	protected $guarded = [];

	// Текст секции #2
	public function getSection2ContentAttribute()
	{
		$locale = \App::getLocale();
		$column = 'section_2_content_' . $locale;
		return $this->{$column};
    }

    // Текст секции #6
	public function getSection6ContentAttribute()
	{
		$locale = \App::getLocale();
		$column = 'section_6_content_' . $locale;
		return $this->{$column};
    }
}
