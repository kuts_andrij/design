<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Templates
Route::pattern('id', '[0-9]+');
Route::pattern('page_url', '[0-9a-z-_]+');

// Dashboard
Route::group(['namespace' => 'Admin', 'prefix' => 'master', 'middleware' => 'auth'], function(){

	// Main page
	Route::get('/', 'AdminHomeController@getIndex');
	Route::post('/', 'AdminHomeController@postIndex');
	Route::post('/upload-image', 'AdminHomeController@postUploadImage');

	// Other controllers
	Route::controllers([
        'settings'				=> 'AdminSettingsController',
        'menu'					=> 'AdminMenuController',
        'pages'					=> 'AdminPagesController',
        'projects-categories'	=> 'AdminProjectsCategoriesController',
        'projects'				=> 'AdminProjectsController',
        'publications'			=> 'AdminPublicationsController',
        'about-page'			=> 'AdminAboutPageController',
        'feedbacks'				=> 'AdminFeedbacksController',
        'users'					=> 'AdminUsersController',
    ]);

	// Logout
	Route::get('auth/logout', 'Auth\AuthController@logout');
});

// Default auth
Route::controllers([
	'auth' 		=> 'Auth\AuthController',
	'password' 	=> 'Auth\PasswordController',
]);

// Frontend
Route::group(['namespace' => 'Frontend', 'prefix' => LaravelLocalization::setLocale()], function(){

	// Main page
	Route::get('/', 'HomeController@getIndex');

	// Projects category
	Route::get('/category/{page_url}', 'CategoriesController@getIndex');

	// Projects media
	foreach( ['photo', 'video', 'honors'] as $route )
	{
		Route::get('/projects-' . $route, 'ProjectsController@getMedia');
	}

	// Project
	Route::get('/project/{page_url}', 'ProjectsController@getProject');

	// Publications
	Route::get('/publications', 'PublicationsController@getIndex');

	// Page about
	Route::get('/about', 'PagesController@getAbout');

	// Page contacts
	Route::get('/contacts', 'PagesController@getContacts');
	Route::post('/contacts', 'PagesController@postContacts');

	// Other pages
	Route::get('/{page_url}', 'PagesController@getIndex');
});
