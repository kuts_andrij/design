<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\User;

use Auth;
use Hash;
use URL;

class AdminUsersController extends AdminBaseController
{
	public function __construct(){
        parent::__construct();
    }
	
	// Users list
	public function getIndex(){
		
		$title = 'Пользователи';
		
		$users = User::paginate(25);
		
		return view('admin.Users.showUsers', compact(['title', 'users']));
    }

	// Delete users
	public function postIndex( Request $request ){
		
		$ids = $request->get('check');
		
		if( $request->get('action') == 'delete' )
			User::destroy( $ids );

		return redirect()->back();
	}

	// Add user (form)
	public function getAdd() {

		$title = 'Добавление пользователя';

		$post  = new User;

		return view('admin.Users.editUser', compact(['title', 'post']));
	}
	
	// Add user
	public function postAdd( UserRequest $request ){

		$user = User::create([
			'name'			=> $request->get('name'),
			'email'   		=> $request->get('email'),
			'password' 		=> Hash::make($request->get('password'))
		]);

		return redirect( URL::to('master/users/edit/' . $user->id) )->with('success', 'Пользователь добавлен');
	}
	
	// Edit user (form)
	public function getEdit( $id ) {

		$title = 'Редактирование пользователя';
		
		$post  = User::find( $id );

		return view('admin.Users.editUser', compact(['title', 'post']));
	}
	
	// Edit user
	public function postEdit( UserRequest $request, $id ){

		$user = User::find($id);
		
		$user->name	 = $request->get('name');
		$user->email = $request->get('email');

		if( $request->get('password') != '' )
			$user->password = Hash::make( $request->get('password') );

		$user->save();

		return redirect()->back()->with('success', 'Информация успешно обновлена');
	}
}
