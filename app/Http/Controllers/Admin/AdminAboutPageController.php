<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\AboutPage;

class AdminAboutPageController extends AdminBaseController
{
    public function __construct(){
        parent::__construct();
    }
	
	// Show about page
	public function getIndex(){
       
		$title = 'Страница "Обо мне"';
		
		$post = AboutPage::find( 1 );
		
		return view('admin.showAboutPage', compact(['title', 'post']));
    }
	
	// Action about page
	public function postIndex( Request $request ){
		
		AboutPage::find( 1 )->update( $request->except('_token') );
		
		return redirect()->back()->with('success', 'Данные сохранены');
	}
}
