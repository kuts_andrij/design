<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Feedback;

class AdminBaseController extends Controller
{
	public function __construct(){
		
		// Feedback count
		$feedback_count = Feedback::count();

		view()->share(compact(['feedback_count']));
	}
}
