<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PageRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Page;

class AdminPagesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    // Pages list
    public function getIndex()
    {
        $title = 'Страницы';
        
        $posts = Page::paginate(25);
        
        return view('admin.Pages.showPages', compact(['title', 'posts']));
    }

    // Delete pages
    public function postIndex( Request $request )
    {
        $ids = $request->get('check');
        
        if( $request->get('action') == 'delete' )
        {
            // Delete pages video/images
            $pages = Page::whereIn('id', $ids)->get();
            foreach( $pages as $page )
            {
                if( $page->video_file )
                    \File::delete( public_path() . '/uploads/pages/video/' . $page->video_file );

                if( $page->image )
                    \File::delete( public_path() . '/uploads/pages/images/' . $page->image );
            }

            Page::destroy( $ids );
        }

        return redirect()->back();
    }

    // Add page (form)
    public function getAdd()
    {
        $title = 'Добавление страницы';

        $post  = new Page;

        return view('admin.Pages.editPage', compact(['title', 'post']));
    }

    // Add page
    public function postAdd( PageRequest $request )
    {
        $post = Page::create( $request->except(['_token', 'image', 'video_file']) );
        
        // If upload image
        if( $request->file('image') )
            Page::upload_image( $request->file('image'), $post->id );

        // If upload video file
        if( $request->file('video_file') )
            Page::upload_video( $request->file('video_file'), $post->id );

        return redirect( \URL::to('master/pages/edit/' . $post->id) )->with('success', 'Страница добавлена');
    }

    // Edit page (form)
    public function getEdit( $id )
    {
        $title = 'Редактирование страницы';

        $post  = Page::find( $id );

        return view('admin.Pages.editPage', compact(['title', 'post']));
    }

    // Edit page
    public function postEdit( PageRequest $request, $id )
    {
        Page::find ($id )->update($request->except(['_token', 'image', 'video_file']));
        
        // If upload image
        if( $request->file('image') )
            Page::upload_image( $request->file('image'), $id );

        // If upload video file
        if( $request->file('video_file') )
            Page::upload_video( $request->file('video_file'), $id );

        return redirect()->back()->with('success', 'Страница обновлена');
    }
}