<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\MenuRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Menu;

use URL;

class AdminMenuController extends AdminBaseController
{
	public function __construct(){
        parent::__construct();
    }

	// Show menu
    public function getIndex(){
		
		$title = 'Меню';
		
		$menu  = json_encode(array_values( Menu::select(['id', 'parent_id', 'lft', 'rgt', 'depth', 'slug', 'name_ru'])->get()->toHierarchy()->toArray() ));
		
		$menu_count = Menu::count();
		
		return view('admin.Menu.showMenu', compact(['title', 'menu', 'menu_count']));
    }
	
	// Action menu
	public function postIndex( Request $request ){

		$ids = $request->get('check');

		switch( $request->get('action') )
		{
			case 'delete':
				Menu::destroy( $ids );
			break;
			
			case 'rebuild':
				Menu::rebuildTree( $request->get('data') );
				return 'rebuilded';
			break;
		}
		
		return redirect()->back();
	}
	
	// Add menu item (form)
	public function getAdd(){
		
		$title = 'Добавление пункта меню';

		$post  = new Menu;

		$tree = Menu::getNestedList('name_ru', NULL, '&nbsp;&nbsp;&nbsp;');
		$tree = ['0' => '-'] + $tree;
		$parent_id = 0;
		
		return view('admin.Menu.editMenu', compact(['title', 'post', 'tree', 'parent_id']));
	}
	
	// Add menu item
	public function postAdd( MenuRequest $request ){
		
		if( $request->get('parent_id') == 0 )
			$input = $request->except(['_token', 'parent_id']);
		else
			$input = $request->except(['_token']); 

		$menu = Menu::create( $input );

		return redirect( URL::to('master/menu/edit/' . $menu->id) )->with('success', 'Пункт меню добавлен');
	}
	
	// Edit munu item (form)
	public function getEdit( $id ){

		$title = 'Редактирование пункта меню';

		$post = Menu::find( $id );
		$tree = Menu::getNestedList('name_ru', NULL, '&nbsp;&nbsp;&nbsp;');

		// Remove yourself from the list
		unset( $tree[$id] );
		$tree = ['0' => '-'] + $tree;

		$parent_id = $post->parent_id;
		
		return view('admin.Menu.editMenu', compact(['title', 'post', 'tree', 'parent_id']));
	}
	
	// Edit munu item
	public function postEdit( MenuRequest $request, $id ){

		if( $request->get('parent_id') == 0 )
			$input = $request->except(['_token', 'parent_id']);
		else
			$input = $request->except(['_token']);

		Menu::find( $id )->update( $input );

		return redirect()->back()->with('success', 'Пункт меню обновлён');
	}
}
