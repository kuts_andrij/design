<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Feedback;

class AdminFeedbacksController extends AdminBaseController
{
    // Feedbacks show
    public function getIndex()
    {
        $title = 'Обратная связь';

        $posts = Feedback::orderBy('created_at', 'DESC')->paginate(25);

        return view('admin.showFeedbacks', compact(['title', 'posts']));
    }

    // Feedbacks action
    public function postIndex( Request $request )
    {
        $ids = $request->get('check');
        
        if( $request->get('action') == 'delete' )
            Feedback::destroy( $ids );

        return redirect()->back();
    }
}
