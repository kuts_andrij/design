<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Publication;

class AdminPublicationsController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    // Show publications
    public function getIndex()
    {
        $title = 'Публикации';

        $images = Publication::orderBy('position')->get();

        return view('admin.Publications.showImages', compact(['title', 'images']));
    }

    // Images action
    public function postIndex( Request $request )
    {
        switch( $request->get('action') )
        {
            case 'delete':
                Publication::delete_images( $request->get('check') );

                return redirect()->back();
            break;
            case 'visible':
                if( $request->ajax() )
                {
                    $visible_image = Publication::find( (int)$request->get('id') );
            
                    $visible = (empty($visible_image->visible) ? 1 : 0);
                        
                    $visible_image->update(['visible' => $visible]);
                    
                    return \Response::json(['visible' => 'changed']);
                }
            break;
            case 'sortable':
                if( $request->ajax() )
                {
                    Publication::sortable_images( $request->get('position') );

                    return \Response::json(['sortable' => 'changed']);
                }
            break;
            default:
                if( $errors = Publication::upload_images( $request->file('images') ) )
                    return redirect()->back()->with('errors', $errors);
                else
                    return redirect()->back();
        }
    }

    // Image info
    public function getImageInfo( $image_id )
    {
        $title = 'Редактирование информации изображения';

        $post = Publication::find( $image_id );

        return view('admin.Publications.editImageInfo', compact(['title', 'post']));
    }

    // Image info action
    public function postImageInfo( Request $request, $image_id )
    {
        Publication::find( $image_id )->update($request->except(['_token']));

        return redirect()->back()->with('success', 'Информация обновлена');
    }
}
