<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProjectCategoryRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Project;
use App\Models\ProjectImage;
use App\Models\ProjectCategory;

class AdminProjectsCategoriesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    // Categories list
    public function getIndex()
    {
        $title = 'Категории проектов';
        
        $posts = ProjectCategory::paginate(25);
        
        return view('admin.ProjectsCategories.showCategories', compact(['title', 'posts']));
    }

    // Delete categories
    public function postIndex( Request $request )
    {
        $ids = $request->get('check');
        
        if( $request->get('action') == 'delete' )
        {
            // Delete categories video/video screen
            $projects_categories = ProjectCategory::whereIn('id', $ids)->get();
            foreach( $projects_categories as $category )
            {
                if( $category->video_file )
                    \File::delete( public_path() . '/uploads/projects_categories/video/' . $category->video_file );

                if( $category->video_screen )
                    \File::delete( public_path() . '/uploads/projects_categories/video/screenshots/' . $category->video_screen ); 
            }

            // Delete projects video/video screen
            $projects = Project::whereIn('category_id', $ids)->get();
            if( count($projects) )
            {
                $project_ids = [];
                foreach( $projects as $project )
                {
                    if( $project->video_file )
                        \File::delete( public_path() . '/uploads/projects/video/' . $project->video_file );

                    if( $project->video_screen )
                        \File::delete( public_path() . '/uploads/projects/video/screenshots/' . $project->video_screen ); 
                
                    $project_ids[] = $project->id;
                }

                // Delete projects images
                $images = ProjectImage::whereIn('project_id', $project_ids)->get();
                if( count($images) )
                {
                    foreach( $images as $item )
                    {
                        \File::delete( public_path() . '/uploads/projects/images/original/' . $item->image );
                        \File::delete( public_path() . '/uploads/projects/images/middle/' . $item->image );
                        \File::delete( public_path() . '/uploads/projects/images/thumbnail/' . $item->image );
                    }
                }
            }

            ProjectCategory::destroy( $ids );
        }

        return redirect()->back();
    }

    // Add category (form)
    public function getAdd()
    {
        $title = 'Добавление категории';

        $post  = new ProjectCategory;

        return view('admin.ProjectsCategories.editCategory', compact(['title', 'post']));
    }

    // Add category
    public function postAdd( ProjectCategoryRequest $request )
    {
        $post = ProjectCategory::create( $request->except(['_token', 'video_screen', 'video_file']) );
        
        // If upload video screen
        if( $request->file('video_screen') )
            ProjectCategory::upload_screen( $request->file('video_screen'), $post->id );

        // If upload video file
        if( $request->file('video_file') )
            ProjectCategory::upload_video( $request->file('video_file'), $post->id );

        return redirect( \URL::to('master/projects-categories/edit/' . $post->id) )->with('success', 'Категория добавлена');
    }

    // Edit category (form)
    public function getEdit( $id )
    {
        $title = 'Редактирование категории';

        $post  = ProjectCategory::find( $id );

        return view('admin.ProjectsCategories.editCategory', compact(['title', 'post']));
    }

    // Edit category
    public function postEdit( ProjectCategoryRequest $request, $id )
    {
        ProjectCategory::find ($id )->update($request->except(['_token', 'video_screen', 'video_file']));
        
        // If upload video screen
        if( $request->file('video_screen') )
            ProjectCategory::upload_screen( $request->file('video_screen'), $id );

        // If upload video file
        if( $request->file('video_file') )
            ProjectCategory::upload_video( $request->file('video_file'), $id );

        return redirect()->back()->with('success', 'Категория обновлена');
    }
}
