<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Setting;

class AdminSettingsController extends AdminBaseController
{
    public function __construct(){
        parent::__construct();
    }

	// Show settings
	public function getIndex(){
       
		$title = 'Настройки';
		
		$settings = Setting::find( 1 );
		
		return view('admin.showSettings', compact(['title', 'settings']));
    }

	// Update settings except images 
	public function postIndex( Request $request ){
		
		Setting::find( 1 )->update( $request->except('_token') );
		
		return redirect()->back()->with('success', 'Настройки сохранены');
    }
}