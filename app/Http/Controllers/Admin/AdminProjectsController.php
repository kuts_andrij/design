<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProjectRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\ProjectImage;

class AdminProjectsController extends AdminBaseController
{
    private $categories = [];
    private $types = [];

    public function __construct()
    {
        parent::__construct();

		// Categories
		$categories = ProjectCategory::all();
        foreach( $categories as $category )
        {
        	$this->categories[$category->id] = $category->name;
        }
        $this->categories = [0 => 'Выберете категорию проекта...'] + $this->categories;

        // Types
        $this->types = [
        					0 => 'Выберете тип проекта...', 
        					'appartment' => 'Appartment', 
        					'house'		 => 'House', 
        					'office'	 => 'Office'
        			   ];
    }

    // Project list
    public function getIndex( Request $request )
    {
    	$title = 'Проекты';
		
		$positions = [
						'default' => 'Позиционирование проектов по умолчанию', 
						'photos'  => 'Позиционирование проектов для раздела "Фото"',
						'videos'  => 'Позиционирование проектов для раздела "Видео"',
						'honors'  => 'Позиционирование проектов для раздела "Награды"'
					 ];

		$categories = $this->categories;
        $types = $this->types;
		
		// Count slider and honors images
		$sql_count_slider = "SUM(CASE WHEN `type` = 'slider' THEN 1 ELSE 0 END) AS count_slider";
		$sql_count_honors = "SUM(CASE WHEN `type` = 'honors' THEN 1 ELSE 0 END) AS count_honors";

		$projects = Project::with(['images' => function( $query ) use ( $sql_count_slider, $sql_count_honors ){
									$query->select([
														'project_id',
														\DB::raw( $sql_count_slider ),
														\DB::raw( $sql_count_honors )
												   ])
										  ->groupBy('project_id');
								 }]);

		// Type
		if( $request->get('type') && !empty($request->get('type')) )
			$projects = $projects->whereType( $request->get('type') );

		// Category
		if( $request->get('category') && !empty($request->get('category')) )
			$ids = [ (int)$request->get('category') ];
		else
		{
			$ids = array_keys($categories);
			$ids = array_except($ids, [0]);
		}

		$projects = $projects->whereIn('category_id', $ids);

		// Position
		if( $request->get('position') && $request->get('position') != 'default' )
			$projects = $projects->orderBy('position_' . $request->get('position'));
		else
			$projects = $projects->orderBy('position');
		
		$projects = $projects->get();
		
		return view('admin.Projects.showProjects', compact(['title', 'projects', 'positions', 'categories', 'types']));
    }

	// Delete projects
    public function postIndex( Request $request )
    {
		$ids = $request->get('check');
        
        if( $request->get('action') == 'delete' )
        {
            $projects = Project::whereIn('id', $ids)->get();
            foreach( $projects as $project )
            {
                if( $project->video_file )
                    \File::delete( public_path() . '/uploads/projects/video/' . $project->video_file );

                if( $project->video_screen )
                    \File::delete( public_path() . '/uploads/projects/video/screenshots/' . $project->video_screen ); 
            }

            $images = ProjectImage::whereIn('project_id', $ids)->get();
            if( count($images) )
            {
				foreach( $images as $item )
				{
					\File::delete( public_path() . '/uploads/projects/images/original/' . $item->image );
					\File::delete( public_path() . '/uploads/projects/images/middle/' . $item->image );
					\File::delete( public_path() . '/uploads/projects/images/thumbnail/' . $item->image );
				}
			}

            Project::destroy( $ids );
        }

        return redirect()->back();
    }

    // Add project (form)
    public function getAdd()
    {
    	$title = 'Добавление проекта';

        $post = new Project;
        $post->visible = 1;

        $categories = $this->categories;
        $types = $this->types;

        return view('admin.Projects.editProject', compact(['title', 'post', 'categories', 'types']));
    }

    // Add project
    public function postAdd( ProjectRequest $request )
    {
		$post = Project::create( $request->except(['_token', 'video_screen', 'video_file']) );
        
        // If upload video screen
        if( $request->file('video_screen') )
            Project::upload_screen( $request->file('video_screen'), $post->id );

        // If upload video file
        if( $request->file('video_file') )
            Project::upload_video( $request->file('video_file'), $post->id );

        // If visible honors
        if( $request->get('visible_main_honors') != 0 )
        {
        	$projects = Project::all();
			foreach( $projects as $project )
			{
				if( $project->id != $post->id )
					Project::find( $project->id )->update(['visible_main_honors' => 0]);
			}
        }

        return redirect( \URL::to('master/projects/edit/' . $post->id) )->with('success', 'Проект добавлен');
    }

    // Edit project (form)
    public function getEdit( $id )
    {
    	$title = 'Редактирование проекта';

        // Count slider and honors images
		$sql_count_slider = "SUM(CASE WHEN `type` = 'slider' THEN 1 ELSE 0 END) AS count_slider";
		$sql_count_honors = "SUM(CASE WHEN `type` = 'honors' THEN 1 ELSE 0 END) AS count_honors";

        $post  = Project::with(['images' => function( $query ) use ($sql_count_slider, $sql_count_honors){
									$query->select([
														'project_id',
														\DB::raw( $sql_count_slider ),
														\DB::raw( $sql_count_honors )
												   ])
										  ->groupBy('project_id');
							 }])
        					->find( $id );

        $categories = $this->categories;
        $types = $this->types;

		return view('admin.Projects.editProject', compact(['title', 'post', 'categories', 'types']));
    }

    // Edit project
    public function postEdit( ProjectRequest $request, $id )
    {
        Project::find( $id )->update($request->except(['_token', 'video_screen', 'video_file']));
        
        // If upload video screen
        if( $request->file('video_screen') )
            Project::upload_screen( $request->file('video_screen'), $id );

        // If upload video file
        if( $request->file('video_file') )
            Project::upload_video( $request->file('video_file'), $id );

         // If visible honors
        if( $request->get('visible_main_honors') != 0 )
        {
        	$projects = Project::all();
			foreach( $projects as $project )
			{
				if( $project->id != $id )
					Project::find( $project->id )->update(['visible_main_honors' => 0]);
			}
        }

        return redirect()->back()->with('success', 'Проект обновлён');
    }

    // Visible project
    public function postVisible( Request $request )
    {
    	if( $request->ajax() )
		{
			$id = (int)$request->get('id');
			
			$field = $request->get('field');

			$visible_type = Project::find( $id );
			
			$visible = (empty($visible_type->{$field}) ? 1 : 0);
			
			$visible_type->update([$field => $visible]);
			
			return \Response::json([$field => 'changed']);
		}
    }

    // Visible project honors
    public function postVisibleHonors( Request $request )
    {
    	if( $request->ajax() )
		{
			$id = (int)$request->get('id');

			$visible_honors = Project::find( $id );
			
			$visible = (empty($visible_honors->visible_main_honors) ? 1 : 0);
			
			$visible_honors->update(['visible_main_honors' => $visible]);

			$projects = Project::all();
			foreach( $projects as $project )
			{
				if( $project->id != $id )
					Project::find( $project->id )->update(['visible_main_honors' => 0]);
			}
			
			return \Response::json(['visible_main_honors' => 'changed']);
		}
    }

    // Sortable projects
    public function postSortable( Request $request )
    {
    	if( $request->ajax() )
		{
			(int)$i = 1;
			
			$field = 'position';
			if( $request->get('position_type') != 'default' )
				$field = 'position_' . $request->get('position_type');

			foreach( $request->get('position') as $item )
			{
				Project::find( $item )->update([$field => $i]);
				$i++;
			}
		}
    }
	
	// Project delete video or background
	public function postRemoveResource( Request $request )
	{
		if( $request->ajax() )
		{
			if( $request->get('action') != 'image' )
			{
				Project::find( $request->get('id') )->update(['video_file' => '', 'visible_main_slider' => 0, 'visible_main_honors' => 0]);
				\File::delete( public_path() . '/uploads/projects/video/' . $request->get('resource') );
			}
			else
			{
				Project::find( $request->get('id') )->update(['video_screen' => '']);
				\File::delete( public_path() . '/uploads/projects/video/screenshots/' . $request->get('resource') );
			}
		}
	}

    // Project images list
    public function getImages( $type, $id )
    {
    	$types = ['slider' => 'Изображения', 'gallery' => 'Галерея', 'honors' => 'Награды'];

    	$title = $types[$type];

    	$project_name = Project::find( $id )->name;

    	$images = ProjectImage::whereType( $type )
    						  ->whereProjectId( $id )
    						  ->orderBy('position')
    						  ->get();

    	return view('admin.Projects.showProjectImages', compact(['title', 'project_name', 'images']));
    }

	// Project images action
	public function postImages( Request $request, $type, $id )
    {
    	switch( $request->get('action') )
		{
			case 'delete':
				ProjectImage::delete_images( $request->get('check') );

				return redirect()->back();
			break;
			case 'active':
				if( $request->ajax() )
				{
					ProjectImage::active_image( $id, (int)$request->get('id'), $type );
					
					return \Response::json(['active' => 'changed']);
				}
			break;
			case 'visible':
				if( $request->ajax() )
				{
					$visible_image = ProjectImage::find( (int)$request->get('id') );
			
					$visible = (empty($visible_image->visible) ? 1 : 0);
						
					$visible_image->update(['visible' => $visible]);
					
					return \Response::json(['visible' => 'changed']);
				}
			break;
			case 'sortable':
				if( $request->ajax() )
				{
					ProjectImage::sortable_images( $request->get('position') );

					return \Response::json(['sortable' => 'changed']);
				}
			break;
			default:
				if( $errors = ProjectImage::upload_images( $type, $request->file('images'), $id ) )
					return redirect()->back()->with('errors', $errors);
				else
					return redirect()->back();
		}
    }

    // Project image info
    public function getImageInfo( $type, $project_id, $image_id )
    {
    	$title = 'Редактирование информации изображения';

        $post = ProjectImage::find( $image_id );

        $project = Project::find( $project_id );

        $types = ['slider' => 'Изображения', 'gallery' => 'Галерея', 'honors' => 'Награды'];
        $type_name = $types[$type];

        return view('admin.Projects.editProjectImage', compact(['title', 'post', 'project', 'type', 'type_name']));
    }

    // Project image info action
    public function postImageInfo( Request $request, $type, $project_id, $image_id )
    {
    	ProjectImage::find( $image_id )->update($request->except(['_token']));

    	return redirect()->back()->with('success', 'Информация обновлена');
    }
}
