<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\MainPage;

use Slug;
use File;
use Image;

class AdminHomeController extends AdminBaseController
{
    public function __construct(){
        parent::__construct();
    }
	
	// Show main page
	public function getIndex(){
       
		$title = 'Панель администратора';
		
		$post = MainPage::find( 1 );
		
		return view('admin.showMainPage', compact(['title', 'post']));
    }
	
	// Update date except image
	public function postIndex( Request $request ){
		
		MainPage::find( 1 )->update( $request->except('_token') );
		
		return redirect()->back()->with('success', 'Данные сохранены');
	}
	
	// Update and upload image
	public function postUploadImage( Request $request ){
		
		if( $file = $request->except('_token') )
		{
			$file_name = key( $file );
		
			if( $old_image = MainPage::find( 1 )->{$file_name} )
				File::delete( public_path() . '/uploads/main_page/' . $old_image );
			
			$new_image = time() . '_' . Slug::make(basename($file[$file_name]->getClientOriginalName(), '.' . $file[$file_name]->getClientOriginalExtension())) . '.' . $file[$file_name]->getClientOriginalExtension();
			
			Image::make( $file[$file_name] )->save( 'uploads/main_page/' . $new_image );
			
			MainPage::find( 1 )->update([$file_name => $new_image]);
			
			return redirect()->back()->with('success', 'Изображение обновлено');
		}
		else
			return redirect()->back()->with('error', 'Изображение не выбрано');
	}
}
