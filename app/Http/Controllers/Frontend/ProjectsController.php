<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Project;
use App\Models\ProjectCategory;

class ProjectsController extends BaseController
{
    public function __contstruct()
    {
    	parent::__contstruct();
    }

    // Projects media
    public function getMedia( Request $request )
    {
		// URL page
		$slug = \LaravelLocalization::getNonLocalizedURL();
		$slug = strtok($slug, '?');
		$slug = str_replace([url(), '/'], '', $slug);

		switch( $slug )
		{
			case 'projects-photo':
				$position = 'position_photos';
				$view = 'frontend.showProjectsPhoto';
				$images_type = 'slider';
			break;
			case 'projects-video':
				$position = 'position_videos';
				$view = 'frontend.showProjectsVideo';
				$images_type = 'slider';
			break;
			case 'projects-honors':
				$position = 'position_honors';
				$view = 'frontend.showProjectsHonors';
				$images_type = 'honors';
			break;
		}

		$page = Page::whereSlug( $slug )->firstOrFail();

		$project_type = 'all';
		if( $request->get('type') )
			$project_type = $request->get('type');

		// Projects
		$projects = Project::with(['images' => function( $query ) use ( $images_type ){
									if( $images_type != 'honors' )
										$query->whereType( $images_type )
											  ->whereVisible( 1 )
											  ->orderBy('position');
									else
										$query->whereVisible( 1 )
											  ->orderBy('position');
								 }])
						   ->whereVisible( 1 );

		if( $project_type != 'all' )
			$projects = $projects->whereType( $project_type );

		$projects = $projects->orderBy( $position )
							 ->get();

		// Types
		$types = [
    				'appartment' => trans('design.filter_apartment'), 
    				'house' 	 => trans('design.filter_house'), 
    				'office' 	 => trans('design.filter_office'), 
    				'all' 		 => trans('design.filter_all')
    			 ];

		// Background url
		$background_url = '';
		if( isset($page->image) && $page->image )
			$background_url = \URL::to('uploads/pages/images/' . $page->image);
		else
			$background_url = 'http://placehold.it/1920x645?text=No background';

		// Video file
		$video_file = '';
		if( isset($page->video_file) && $page->video_file )
			$video_file = \URL::to('uploads/pages/video/' . $page->video_file);

		// Points map
		$points = Project::json_points_map();

		return view($view, compact(['page', 'projects', 'types', 'background_url', 'video_file', 'points']));
    }

    // Project
    public function getProject( $slug )
    {
    	$page = Project::with(['images' => function( $query ){
									$query->whereVisible( 1 )
									      ->orderBy('position');
						   }])
    				   ->whereSlug( $slug )
    				   ->firstOrFail();

    	// Current category project
    	$category = ProjectCategory::find( $page->category_id );

    	// Prev/Next projects
    	$next_project_id = Project::where('id', '>', $page->id)
    							  ->whereVisible( 1 )
    							  ->min('id');
		$prev_project_id = Project::where('id', '<', $page->id)
								  ->whereVisible( 1 )
								  ->max('id');

		$pn_ids = [];
		if( $next_project_id )
			$pn_ids[] = $next_project_id;
		else
			$pn_ids[] = Project::whereVisible( 1 )
							   ->orderBy('id', 'ASC')
							   ->first()
							   ->id;
		if( $prev_project_id )
			$pn_ids[] = $prev_project_id;
		else
			$pn_ids[] = Project::whereVisible( 1 )
							   ->orderBy('id', 'DESC')
							   ->first()
							   ->id;

		$pn_ids_str = implode(',', $pn_ids);

		$prev_next_projects = Project::select('slug')
									 ->whereIn('id', $pn_ids)
							 		 ->whereVisible( 1 )
							 		 ->orderBy( \DB::raw('FIELD(id, ' . $pn_ids_str . ')'), 'DESC' )
									 ->get();

		$prev_next_urls = [];
		if( count($prev_next_projects) )
		{
			foreach( $prev_next_projects as $pn )
			{
				$prev_next_urls[] = \LaravelLocalization::getLocalizedURL( \App::getLocale(), '/project/' . $pn->slug );
			}
		}

    	// Background url
		$background_url = '';
		if( isset($page->video_screen) && $page->video_screen )
			$background_url = \URL::to('uploads/projects/video/screenshots/' . $page->video_screen);
		else
			$background_url = 'http://placehold.it/1920x645?text=No background';

		// Video file
		$video_file = '';
		if( isset($page->video_file) && $page->video_file )
			$video_file = \URL::to('uploads/projects/video/' . $page->video_file);

    	return view('frontend.showProject', compact(['page', 'category', 'background_url', 'video_file', 'prev_next_urls']));
    }
}
