<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Menu;
use App\Models\Setting;
use App\Models\MainPage;

class BaseController extends Controller
{
	protected $request;
	
	public function __construct(){

		$this->request = \App::make('Illuminate\Http\Request');

		// Current locale
		$locale = \App::getLocale();

		// All languages
		$langs = \LaravelLocalization::getSupportedLocales();
		$langs = array_keys( $langs );
		$langs = array_values(array_sort($langs, function( $value ) use ( $locale ){	
			return ($locale != $value);
		}));
		
		// Menu
		$menu = Menu::all()->toHierarchy();

		// Settings
		$settings = Setting::find(1);

		view()->share(compact(['menu', 'settings', 'locale', 'langs']));
	}
}
