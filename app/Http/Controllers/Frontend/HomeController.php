<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Project;
use App\Models\MainPage;

class HomeController extends BaseController
{
    public function __contstruct(){
    	parent::__contstruct();
    }

    // Show main page
    public function getIndex(){
        
		$page = Page::whereSlug(' ')->firstOrFail();

		$main_page = MainPage::find( 1 );

		// Images
		$images = [
					'section_2_photo'	=> '480x480',
					'section_3_image_1' => '360x265',
					'section_3_image_2' => '360x265',
					'section_3_image_3' => '360x265',
					'section_3_image_4' => '360x265',
					'section_6_photo'	=> '649x274' 
				  ];

		foreach( $images as $key => $value )
		{
			if( $main_page->{$key} )
				$main_page->{$key} = \URL::to('uploads/main_page/' . $main_page->{$key});
			else
				$main_page->{$key} = 'http://placehold.it/' . $value . '?text=No photo';
		}

		$projects_left = Project::with(['images' => function( $query ){
										$query->whereType('slider')
											  ->whereActive( 1 )
											  ->whereVisible( 1 );
								  }])
								->whereVisibleMainThumbs( 1 )
								->whereVisible( 1 )
								->orderBy('position')
								->take( 3 )
								->get();

		$projects_slider = Project::whereVisibleMainSlider( 1 )
								  ->whereVisible( 1 )
								  ->orderBy('position')
								  ->get();

		$project_honors = Project::with(['images' => function( $query ){
										$query->whereType('honors')
											  ->whereVisible( 1 )
											  ->orderBy('position');
								  }])
								->whereVisibleMainHonors( 1 )
								->whereVisible( 1 )
								->first();

		return view('frontend.Main', compact(['page', 'main_page', 'projects_left', 'projects_slider', 'project_honors']));
    }
}