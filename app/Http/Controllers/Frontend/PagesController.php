<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Feedback;
use App\Models\Setting;
use App\Models\AboutPage;

class PagesController extends BaseController
{
    public function __contstruct()
    {
    	parent::__contstruct();
    }

    // Page default
    public function getIndex( $slug )
    {
		$page = Page::whereSlug( $slug )->firstOrFail();

		// Background
		if( $page->image )
			$page->image = \URL::to('uploads/pages/images/' . $page->image);
		else
			$page->image = 'http://placehold.it/1920x645?text=No background';

		return view('frontend.showPage', compact(['page']));
    }

    // Page about
    public function getAbout()
    {
		$page = Page::whereSlug('about')->firstOrFail();

		$about_page = AboutPage::find( 1 );

        // Background
		if( $page->image )
			$page->image = \URL::to('uploads/pages/images/' . $page->image);
		else
			$page->image = 'http://placehold.it/1920x645?text=No background';

		return view('frontend.showPageAbout', compact(['page', 'about_page']));
    }

    // Page contacts
    public function getContacts()
    {
		$page = Page::whereSlug('contacts')->firstOrFail();

		// Background
		if( $page->image )
			$page->image = \URL::to('uploads/pages/images/' . $page->image);
		else
			$page->image = 'http://placehold.it/1920x645?text=No background';

		return view('frontend.showPageContacts', compact(['page']));
    }

    // Page contacts action
    public function postContacts( Request $request  )
    {
    	if( $request->ajax() )
        {
	    	$validation_fields = [
	    							'name' 	  => 'required', 
	    							'email'	  => 'required|email', 
	    							'phone'	  => 'required', 
	    							'comment' => 'required|min:10'
	    						 ];

			$fields = $request->except(['_token']);

            $validator = \Validator::make( $fields, $validation_fields );

            if( !$validator->fails() )
            {
                Feedback::create( $fields );

                $email = Setting::find( 1 )->email;

                \Mail::send('emails.feedback', $fields, function( $message ) use ( $email ){
                    $message->to($email, 'Grabovskiy')->subject('Обратная связь');
                });

                return \Response::json(['success' => trans('design.message_success')]);
            }
            else
                return \Response::json(['errors'  => $validator->errors()->getMessages()]);
	    }
    }
}
