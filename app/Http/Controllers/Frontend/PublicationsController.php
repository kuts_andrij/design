<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Publication;

class PublicationsController extends BaseController
{
    public function __contstruct()
    {
    	parent::__contstruct();
    }

    public function getIndex()
    {
		$page = Page::whereSlug('publications')->firstOrFail();

		$publications = Publication::whereVisible( 1 )
								   ->orderBy('position')
								   ->get();

		// Background
		if( $page->image )
			$page->image = \URL::to('uploads/pages/images/' . $page->image);
		else
			$page->image = 'http://placehold.it/1920x645?text=No background';

		return view('frontend.showPublications', compact(['page', 'publications']));
    }
}
