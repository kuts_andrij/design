<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\Project;
use App\Models\ProjectCategory;

class CategoriesController extends BaseController
{
    public function __contstruct()
    {
    	parent::__contstruct();
    }

    // Show category
    public function getIndex( Request $request, $slug )
    {
		$project_type = 'all';
		if( $request->get('type') )
			$project_type = $request->get('type');

		// Projects
		$projects = Project::with(['images' => function( $query ){
									$query->whereType('slider')
										  ->whereActive( 1 )
										  ->whereVisible( 1 );
								 }]);

		if( $slug != 'projects-all' )
		{
			$page = ProjectCategory::whereSlug( $slug )->firstOrFail();

			$projects = $projects->whereCategoryId( $page->id )
								 ->whereVisible( 1 );
		}
		else
		{
			$page = Page::whereSlug( $slug )->firstOrFail();

			$projects = $projects->whereVisible( 1 );
		}

		if( $project_type != 'all' )
			$projects = $projects->whereType( $project_type );

		$projects = $projects->orderBy('position')
							 ->get();

		// Types
		$types = [
    				'appartment' => trans('design.filter_apartment'), 
    				'house' 	 => trans('design.filter_house'), 
    				'office' 	 => trans('design.filter_office'), 
    				'all' 		 => trans('design.filter_all')
    			 ];

		// Background url
		$background_url = '';
		if( isset($page->image) && $page->image )
			$background_url = \URL::to('uploads/pages/images/' . $page->image);
		elseif( isset($page->video_screen) && $page->video_screen )
			$background_url = \URL::to('uploads/projects_categories/video/screenshots/' . $page->video_screen);
		else
			$background_url = 'http://placehold.it/1920x645?text=No background';

		// Video file
		$video_file = '';
		if( isset($page->video_file) && $page->video_file )
		{
			if( isset($page->image) )
				$video_file = \URL::to('uploads/pages/video/' . $page->video_file);
			else
				$video_file = \URL::to('uploads/projects_categories/video/' . $page->video_file);
		}

		// Points map
		$points = Project::json_points_map();

		return view('frontend.showCategory', compact(['page', 'projects', 'types', 'background_url', 'video_file', 'points']));
    }
}
