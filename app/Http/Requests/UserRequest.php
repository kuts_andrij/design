<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'		=> 'required',
			'email'		=> 'required|email|' . (!empty(Request::segment(4)) ? 'unique:users,email,' . (int)Request::segment(4) : 'unique:users'),
            'password'	=> (!empty(Request::segment(4)) ? '' : 'required|min:6')
        ];
    }
}
