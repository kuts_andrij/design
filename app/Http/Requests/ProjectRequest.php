<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ru'       => 'required',
            'category_id'   => 'not_in:0',
            'type'          => 'not_in:0',
            'slug'          => 'required|' . (!empty(Request::segment(4)) ? 'unique:as_projects,slug,' . (int)Request::segment(4) : 'unique:as_projects'),
            'video_file'    => 'mimetypes:video/mp4',
            'video_screen'  => 'image',
            'video_youtube' => 'url'
        ];
    }
}
