<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjectCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ru'       => 'required',
            'slug'          => 'required|' . (!empty(Request::segment(4)) ? 'unique:as_projects_categories,slug,' . (int)Request::segment(4) : 'unique:as_projects_categories'),
            'video_screen'  => 'image',
            'video_file'    => 'mimetypes:video/mp4'
        ];
    }
}
