<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
	<h2>Обратная связь</h2>
	<p><strong>Имя:</strong> {{ $name }}</p>
	
	@if( isset($phone) )
		<p><strong>Номер телефона:</strong>  {{ $phone }}</p>
	@endif

	@if( isset($email) )
		<p><strong>E-mail:</strong> {{ $email }}</p>
	@endif

	@if( isset($comment) )
		<p><strong>Текст сообщения:</strong></p> 
		<p>{{ $comment }}</p>
	@endif
</body>
</html>