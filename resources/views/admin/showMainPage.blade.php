@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center">Главная страница</h2>
			
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif
			
			@if(Session::has('error'))
				<div class="alert alert-danger" role="alert">{{ Session::get('error') }}</div>
			@endif
			
			<!-- Section 2 -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Секция №2</h3>
				</div>
				<div class="panel-body">
				
					{!! Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/master/upload-image']) !!}
						
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<div class="panel panel-warning not-bottom">
									<div class="panel-heading">Размер фото не должен превышать <strong>480x480</strong> px.</div>
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label">Фото</label>
							<div class="col-sm-10">
								<div class="input-group">
									<span class="input-group-btn">
										<span class="btn btn-primary btn-file">
											<i class="fa fa-picture-o"></i> <input type="file" name="section_2_photo" accept="image/gif, image/jpeg, image/png" />
										</span>
									</span>
									<input type="text" class="form-control" value="{{ $post->section_2_photo }}" placeholder="Выбирите фотографию..." readonly>
								</div>
							</div>		
						</div>
						
						@if( $post->section_2_photo )
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-6">
									<div class="thumbnail not-bottom">
										<img src="/uploads/main_page/{{ $post->section_2_photo }}" alt="" />
									</div>
								</div>
							</div>
						@endif

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-success">Загрузить</button>
							</div>
						</div>

					{!! Form::close() !!}

					{!! Form::open(['class' => 'form-horizontal']) !!}
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Текст</label>
							<div class="col-sm-10">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									<li class="active"><a href="#section-2-content-ru-tab" role="tab" data-toggle="tab">RU</a></li>
									<li><a href="#section-2-content-en-tab" role="tab" data-toggle="tab">EN</a></li>
								</ul>
								<!-- Tab panes -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="section-2-content-ru-tab">
										<textarea class="form-control" rows="4" name="section_2_content_ru">{!! $post->section_2_content_ru !!}</textarea>
									</div>
									<div role="tabpanel" class="tab-pane" id="section-2-content-en-tab">
										<textarea class="form-control" rows="4" name="section_2_content_en">{!! $post->section_2_content_en !!}</textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-success">Сохранить</button>
							</div>
						</div>
					
					{!! Form::close() !!}
					
				</div>
			</div>
			<!-- End Section 2 -->

			<!-- Section 3 -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Секция №3</h3>
				</div>
				<div class="panel-body">
				
					<div class="row">
						<div class="col-sm-offset-2 col-sm-10">
							<div class="panel panel-warning">
								<div class="panel-heading">Размер изображений не должен превышать <strong>360x265</strong> px.</div>
							</div>
						</div>
					</div>

					@for( $i = 1; $i <= 4; $i++ )

						{!! Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/master/upload-image']) !!}
							
							<div class="form-group">	
								<label class="col-sm-2 control-label">Изображение №{{ $i }}</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-picture-o"></i> <input type="file" name="section_3_image_{{ $i }}" accept="image/gif, image/jpeg, image/png" />
											</span>
										</span>
										<input type="text" class="form-control" value="{{ $post->{'section_3_image_' . $i}  }}" placeholder="Выбирите изображение..." readonly>
									</div>
								</div>		
							</div>
							
							@if( $post->{'section_3_image_' . $i} )
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-6">
										<div class="thumbnail not-bottom">
											<img src="/uploads/main_page/{{ $post->{'section_3_image_' . $i} }}" alt="" />
										</div>
									</div>
								</div>
							@endif

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-success">Загрузить</button>
								</div>
							</div>

						{!! Form::close() !!}

						@if( $i != 4 )
							<hr/>
						@endif

					@endfor

				</div>
			</div>
			<!-- End Section 3 -->

			<!-- Section 6 -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Секция №6</h3>
				</div>
				<div class="panel-body">
					
					{!! Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/master/upload-image']) !!}
						
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<div class="panel panel-warning not-bottom">
									<div class="panel-heading">Размер изображения не должен превышать <strong>649x274</strong> px.</div>
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label">Изображение</label>
							<div class="col-sm-10">
								<div class="input-group">
									<span class="input-group-btn">
										<span class="btn btn-primary btn-file">
											<i class="fa fa-picture-o"></i> <input type="file" name="section_6_photo" accept="image/gif, image/jpeg, image/png" />
										</span>
									</span>
									<input type="text" class="form-control" value="{{ $post->section_6_photo }}" placeholder="Выбирите изображение..." readonly>
								</div>
							</div>		
						</div>
						
						@if( $post->section_6_photo )
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-6">
									<div class="thumbnail not-bottom">
										<img src="/uploads/main_page/{{ $post->section_6_photo }}" alt="" />
									</div>
								</div>
							</div>
						@endif

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-success">Загрузить</button>
							</div>
						</div>

					{!! Form::close() !!}

					{!! Form::open(['class' => 'form-horizontal']) !!}
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Текст</label>
							<div class="col-sm-10">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									<li class="active"><a href="#section-6-content-ru-tab" role="tab" data-toggle="tab">RU</a></li>
									<li><a href="#section-6-content-en-tab" role="tab" data-toggle="tab">EN</a></li>
								</ul>
								<!-- Tab panes -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="section-6-content-ru-tab">
										<textarea class="form-control" rows="4" name="section_6_content_ru">{!! $post->section_6_content_ru !!}</textarea>
									</div>
									<div role="tabpanel" class="tab-pane" id="section-6-content-en-tab">
										<textarea class="form-control" rows="4" name="section_6_content_en">{!! $post->section_6_content_en !!}</textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-success">Сохранить</button>
							</div>
						</div>
					
					{!! Form::close() !!}

				</div>
			</div>
			<!-- End Section 6 -->

		</div>
	</div>

@stop

@section('scripts')
    @parent
	<script>
		$(function(){
			
			// File input
			$(document).on('change', '.btn-file :file', function(){
				var input = $(this),
				numFiles = input.get(0).files ? input.get(0).files.length : 1,
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				input.trigger('fileselect', [numFiles, label]);
			});
			$(document).ready(function(){
				$('.btn-file :file').on('fileselect', function(event, numFiles, label){
					var input = $(this).parents('.input-group').find(':text'),
						log = numFiles > 1 ? numFiles + ' файла(ов) выбрано' : label;
					
					if( input.length )
						input.val(log);
					else
						if( log ) 
							alert(log);
				});
			});
		});
	</script>
@endsection