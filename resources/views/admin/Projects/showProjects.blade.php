@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ URL::to('master') }}">Главная</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<a href="{{ URL::to('master/projects/add') }}" class="thumbnail text-center">
				<i class="fa fa-plus-circle fa-5x"></i>
				<div class="title">Добавить проект</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if( count($projects) > 0 )
			
				<div class="row">
					<div class="col-md-12">
						<form class="form-horizontal" method="GET">
							<div class="form-group">
								<div class="col-sm-12">
									{!! Form::select('category', $categories, (!empty((int)Request::get('category')) ? (int)Request::get('category') : 0), ['class' => 'form-control']); !!}
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									{!! Form::select('type', $types, (!empty(Request::get('type')) ? Request::get('type') : 0), ['class' => 'form-control']); !!}
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-10">
									{!! Form::select('position', $positions, (!empty(Request::get('position')) ? Request::get('position') : 'default'), ['class' => 'form-control']); !!}
								</div>
								<div class="col-sm-2">
									<button type="submit" class="btn btn-block btn-success"><i class="fa fa-filter"></i> Фильтр</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="title-module">
					<div class="pull-left">
						<strong>Название</strong>
					</div>
					<div class="pull-right">
						<strong class="text-control">Управление</strong>
					</div>
					<div class="clear"></div>
				</div>
				
				{!! Form::open(['class' => 'projects-list']) !!}
					<div class="list-group">
						
						@foreach($projects as $project)
							<div class="list-group-item" id="{{ $project->id }}">
								<div class="pull-left">
									<span class="item-element-menu menu-sortable" title="Сортировка">
										<i class="fa fa-bars"></i>
									</span>
									<span class="item-element-menu">
										<input type="checkbox" name="check[]" value="{{ $project->id }}" />
									</span>
									<span class="item-element-menu">
										{{ $project->name }}
									</span>
								</div>
								<div class="pull-right">
									<div class="btn-group" role="group">
										<button type="button" class="btn {{ !empty($project->visible) ? 'btn-success' : 'btn-default' }} visible" title="Показать/Скрыть проект" data-id="{{ $project->id }}">
											<i class="fa {{ !empty($project->visible) ? 'fa-star' : 'fa-star-o' }}"></i>
										</button>
										@if( $project->video_file )
											<button type="button" class="btn {{ !empty($project->visible_main_slider) ? 'btn-success' : 'btn-default' }} visible-main-slider" title="Отображать/Не отображать видео проекта в слайдере на главной странице" data-id="{{ $project->id }}">
												<i class="fa fa-video-camera"></i>
											</button>
										@endif
										@if( count($project->images) )
											@foreach( $project->images as $item )
												@if( $item->count_slider )
													<button type="button" class="btn {{ !empty($project->visible_main_thumbs) ? 'btn-success' : 'btn-default' }} visible-main-thumbs" title="Отображать/Не отображать миниатюру проекта в левой колонке на главной странице" data-id="{{ $project->id }}">
														<i class="fa fa-camera"></i>
													</button>
													<?php break; ?>
												@endif
											@endforeach
										@endif
										@if( count($project->images) )
											@foreach( $project->images as $item )
												@if( $item->count_honors )
													<button type="button" class="btn {{ !empty($project->visible_main_honors) ? 'btn-success' : 'btn-default' }} visible-main-honors" title="Отображать/Не отображать награды проекта на главной странице" data-id="{{ $project->id }}">
														<i class="fa fa-certificate"></i>
													</button>
													<?php break; ?>
												@endif
											@endforeach
										@endif
										<a href="{{ URL::to('master/projects/images/slider/' . $project->id) }}" class="btn btn-primary" title="Изображения проекта"><i class="fa fa-picture-o"></i></a>
										<a href="{{ URL::to('master/projects/images/gallery/' . $project->id) }}" class="btn btn-primary" title="Галерея проекта"><i class="fa fa-camera-retro"></i></a>
										<a href="{{ URL::to('master/projects/images/honors/' . $project->id) }}" class="btn btn-primary" title="Награды проекта"><i class="fa fa-globe"></i></a>
										<a href="{{ URL::to('master/projects/edit/' . $project->id) }}" class="btn btn-warning" title="Редактировать проект"><i class="fa fa-pencil"></i></a>
										<button type="button" class="btn btn-danger delete" title="Удалить проект" data-id="{{ $project->id }}"><i class="fa fa-times"></i></button>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						@endforeach
						
					</div>
					<div class="select_form">
						<label id="check_all" class="link">Выбрать все</label>
						<select name="action" class="form-control">
							<option value="delete">Удалить</option>
						</select>
						<button type="submit" class="btn btn-success delete-all" disabled>Применить</button>
					</div>	
				{!! Form::close() !!}
			@else
				<div class="alert alert-warning">Проекты еще не добавлены</div>
			@endif
		</div>
	</div>
@stop

@section('scripts')
    @parent
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script>
		$(function(){
			// Удаление проекта
			$('.delete').click( function() {
				$('input[type="checkbox"][name*="check"]').prop('checked', false);
				$(this).closest('.list-group-item').find('input[type="checkbox"][name*="check"]').prop('checked', true);
				$(this).closest('form').find('select[name="action"] option[value=delete]').attr('selected', true);
				$(this).closest('form').submit();
			});

			// Удаление проектов
			$('form.projects-list').submit(function(){
				if( $('select[name="action"]').val() == 'delete' && !confirm('Подтвердите удаление') )
					return false;
			});

			// Выделить все
			$('#check_all').on('click', function(){
				$('input[type="checkbox"][name*="check"]:enabled').prop('checked', $('input[type="checkbox"][name*="check"]:enabled:not(:checked)').length > 0 );
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Активность кнопки "Удалить выбранные" 
			$('input[type="checkbox"][name*="check"]').change(function(){
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Показать/Скрыть проект
			$('.visible').on('click', function(event){
				event.preventDefault();
				if( $(this).hasClass('btn-success') )
					$(this).removeClass('btn-success').addClass('btn-default').find('i').removeClass('fa-star').addClass('fa-star-o');
				else
					$(this).removeClass('btn-default').addClass('btn-success').find('i').removeClass('fa-star-o').addClass('fa-star');
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id'), field: 'visible' };
				$.post('{{ URL::to("master/projects/visible") }}', data, function(data){ console.log(data) }, 'JSON');
			});

			// Отображать/Не отображать видео проекта в слайдере на главной странице
			$('.visible-main-slider').on('click', function(event){
				event.preventDefault();
				if( $(this).hasClass('btn-success') )
					$(this).removeClass('btn-success').addClass('btn-default');
				else
					$(this).removeClass('btn-default').addClass('btn-success');
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id'), field: 'visible_main_slider' };
				$.post('{{ URL::to("master/projects/visible") }}', data, function(data){ console.log(data) }, 'JSON');
			});

			// Отображать/Не отображать миниатюру проекта в левой колонке на главной странице
			$('.visible-main-thumbs').on('click', function(event){
				event.preventDefault();
				if( $(this).hasClass('btn-success') )
					$(this).removeClass('btn-success').addClass('btn-default');
				else
					$(this).removeClass('btn-default').addClass('btn-success');
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id'), field: 'visible_main_thumbs' };
				$.post('{{ URL::to("master/projects/visible") }}', data, function(data){ console.log(data) }, 'JSON');
			});

			// Отображать/Не отображать награды проекта на главной странице
			$('.visible-main-honors').on('click', function(event){
				event.preventDefault();
				
				var project_id = $(this).data('id'),
					data = { _token: '{{ csrf_token() }}', id: project_id };
				
				if( $(this).hasClass('btn-success') )
					$(this).removeClass('btn-success').addClass('btn-default');
				else
					$(this).removeClass('btn-default').addClass('btn-success');
				
				$('.list-group .btn-group').each(function(){
					if( $(this).find('.visible-main-honors').data('id') != project_id )
						$(this).find('.visible-main-honors').removeClass('btn-success').addClass('btn-default');
				});
			
				$.post('{{ URL::to("master/projects/visible-honors") }}', data, function(data){ console.log(data) }, 'JSON');
			});
			
			// Позиционирование проектов
			$('.list-group').sortable({
				handle: '.menu-sortable',
				opacity: 0.7,
				stop: function(){
					var element_sort = $(this).sortable('toArray');
					$.post('{{ URL::to("master/projects/sortable") }}', { _token: '{{ csrf_token() }}', position: element_sort, position_type: '{{ (!empty(Request::get("position")) ? Request::get("position") : "default") }}' });	
				}
			});
		});
	</script>
@endsection