@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ URL::to('master') }}">Главная</a></li>
				<li><a href="{{ URL::to('master/projects') }}">Проекты</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="languages-container">
		
		@if( $id = (int)Request::segment(4) )
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="btn-group" role="group">
								<a href="{{ URL::to('master/projects/images/slider/' . $id) }}" class="btn btn-primary"><i class="fa fa-picture-o"></i> Изображения</a>
								<a href="{{ URL::to('master/projects/images/gallery/' . $id) }}" class="btn btn-primary"><i class="fa fa-camera-retro"></i> Галерея</a>
								<a href="{{ URL::to('master/projects/images/honors/' . $id) }}" class="btn btn-primary"><i class="fa fa-globe"></i> Награды</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif

		<div class="row">
			<div class="col-md-12">
			
				@if(Session::has('success'))
					<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
				@endif
				
				@if( $errors->any() )
					<ul class="alert alert-danger">
						@foreach( $errors->all() as $error )
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				@endif
				
				{!! Form::open(['class' => 'form-horizontal', 'role' => 'form', 'files' => true]) !!}
					
					<div class="form-group">
						<div class="col-sm-12">
							{!! Form::select('category_id', $categories, $post->category_id, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							{!! Form::select('type', $types, $post->type, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							{!! Form::select('visible', [0 => 'Скрыть проект', 1 => 'Отображать проект'], $post->visible, ['class' => 'form-control']) !!}
						</div>
					</div>
					
					@if( !empty($post->video_file) )
						<div class="form-group">
							<div class="col-sm-12">
								{!! Form::select('visible_main_slider', [0 => 'НЕ отображать видео проекта в слайдере на главной странице', 1 => 'Отображать видео проекта в слайдере на главной странице'], $post->visible_main_slider, ['class' => 'form-control']) !!}
							</div>
						</div>
					@endif

					@if( count($post->images) )
						@foreach( $post->images as $item )
							@if( $item->count_slider )
								<div class="form-group">
									<div class="col-sm-12">
										{!! Form::select('visible_main_thumbs', [0 => 'НЕ отображать миниатюру проекта в левой колонке на главной странице', 1 => 'Отображать миниатюру проекта в левой колонке на главной странице'], $post->visible_main_thumbs, ['class' => 'form-control']) !!}
									</div>
								</div>
								<?php break; ?>
							@endif
						@endforeach
					@endif

					@if( count($post->images) )
						@foreach( $post->images as $item )
							@if( $item->count_honors )
								<div class="form-group">
									<div class="col-sm-12">
										{!! Form::select('visible_main_honors', [0 => 'НЕ отображать награды проекта на главной странице', 1 => 'Отображать награды проекта на главной странице'], $post->visible_main_honors, ['class' => 'form-control']) !!}
									</div>
								</div>
								<?php break; ?>
							@endif
						@endforeach
					@endif	

					<div class="alert alert-warning">
						Максимальный размер загружаемого файла: <strong>{{ ini_get('upload_max_filesize') }}</strong>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-btn">
									<span class="btn btn-primary btn-file">
										<i class="fa fa-film"></i> <input type="file" name="video_file" />
									</span>
								</span>
								<input type="text" class="form-control" id="video" value="{{ $post->video_file }}" placeholder="Выберите видео файл" readonly>
							</div>
						</div>
					</div>

					@if( !empty($post->video_file) )
						<div class="form-group">
							<div class="col-sm-3">
								<div class="panel panel-default video-prev">
									<div class="panel-body">
										<video controls="controls" @if( $post->video_screen ) poster="{{ URL::to('uploads/projects/video/screenshots/' . $post->video_screen) }}" @endif>
											<source type="video/mp4" src="{{ URL::to('uploads/projects/video/' . $post->video_file) }}">
										</video>
									</div>
								</div>
								<a href="#" class="btn btn-danger btn-block remove-resource" data-id="{{ (int)Request::segment(4) }}" data-action="video" data-resource="{{ $post->video_file }}">
									<i class="fa fa-trash" aria-hidden="true"></i> Удалить видео
								</a>
							</div>
						</div>
					@endif

					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-btn">
									<span class="btn btn-primary btn-file">
										<i class="fa fa-picture-o"></i> <input type="file" name="video_screen" />
									</span>
								</span>
								<input type="text" class="form-control" id="image" value="{{ $post->video_screen }}" placeholder="Выберите фон для видео" readonly>
							</div>
						</div>
					</div>
					
					@if( !empty($post->video_screen) )
						<div class="form-group">
							<div class="col-sm-3">
								<div class="panel panel-default image-prev">
									<div class="panel-body">
										<img src="{{ URL::to('uploads/projects/video/screenshots/' . $post->video_screen) }}" alt="" />
									</div>
								</div>
								<a href="#" class="btn btn-danger btn-block remove-resource" data-id="{{ (int)Request::segment(4) }}" data-action="image" data-resource="{{ $post->video_screen }}">
									<i class="fa fa-trash" aria-hidden="true"></i> Удалить фоновое изображение
								</a>
							</div>
						</div>
					@endif
					
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-youtube-play"></i></span>
								{!! Form::text('video_youtube', $post->video_youtube, ['class' => 'form-control', 'placeholder' => 'Ссылка на Youtube видео']) !!}
							</div>
						</div>
					</div>

					@if( !empty($post->video_youtube) )
						<div class="form-group">
							<div class="col-sm-3">
								<div class="panel panel-default iframe-prev">
									<div class="panel-body">
										<iframe src="https://www.youtube.com/embed/{!! App\Models\Project::video_code( $post->video_youtube ) !!}" allowfullscreen></iframe>
									</div>
								</div>
							</div>
						</div>
					@endif

					<div class="form-group">
						<div class="col-sm-12">
							<div class="alert alert-info">
								Координаты вводятся через пробел, пример: <strong>-34.397 150.644</strong><br/>
								Где <strong>-34.397</strong> - широта, <strong>150.644</strong> - долгота

							</div>
							<div class="input-group">
								<span class="input-group-addon" style="padding: 6px 17px !important;"><i class="fa fa-map-marker"></i></span>
								{!! Form::text('object_point', $post->object_point, ['class' => 'form-control', 'placeholder' => 'Координаты на карте']) !!}
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								{!! Form::number('object_year', $post->object_year, ['class' => 'form-control', 'placeholder' => 'Год реализации проекта', 'max' => date('Y'), 'step' => 1]) !!}
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon">/project/</span>
								{!! Form::text('slug', $post->slug, ['class' => 'form-control', 'placeholder' => 'URL адрес']) !!}
							</div>
						</div>
					</div>
				
					<!-- Languages controls -->
					<div class="form-group">
						<ul class="languages-caption">
							<li class="col-md-3 active">
								<a href="#ru" class="thumbnail text-center">
									<i class="fa fa-language fa-5x"></i>
									<div class="title">Русский язык</div>
								</a>
							</li>
							<li class="col-md-3">
								<a href="#en" class="thumbnail text-center">
									<i class="fa fa-language fa-5x"></i>
									<div class="title">English language</div>
								</a>
							</li>
						</ul>
					</div>
					
					<!-- RU -->
					<div class="languages-content active">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#record_ru" aria-controls="record" role="tab" data-toggle="tab">Запись</a>
							</li>
							<li role="presentation">
								<a href="#options_ru" aria-controls="options" role="tab" data-toggle="tab">Параметры записи</a>
							</li>
						</ul>
						<!-- End nav tabs -->
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="record_ru">
								<div class="form-group">
									{!! Form::label('name_ru', 'Название проекта', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('name_ru',  $post->name_ru, ['class' => 'form-control']) !!}
									</div>
								</div>
								<hr/>
								<div class="form-group">
									{!! Form::label('object_ru', 'Объект', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('object_ru',  $post->object_ru, ['class' => 'form-control', 'placeholder' => 'Пример: Residential private house']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('location_ru', 'Место нахождения', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('location_ru',  $post->location_ru, ['class' => 'form-control', 'placeholder' => 'Пример: Kiev, Ukraine']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('space_ru', 'Пространство', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('space_ru',  $post->space_ru, ['class' => 'form-control', 'placeholder' => 'Пример: 5 hectares']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('client_ru', 'Клиент', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('client_ru',  $post->client_ru, ['class' => 'form-control', 'placeholder' => 'Пример: Private person']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('photographer_ru', 'Фотограф', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('photographer_ru',  $post->photographer_ru, ['class' => 'form-control']) !!}
									</div>
								</div>
								<hr/>
								<div class="form-group">
									{!! Form::label('annotation_ru', 'Аннотация', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										<textarea name="annotation_ru" id="annotation_ru" class="form-control" rows="3">{{ $post->annotation_ru }}</textarea>
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('body_slider_ru', 'Описание (слайдер)', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::textarea('body_slider_ru',  $post->body_slider_ru, ['class' => 'form-control editor']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('body_gallery_ru', 'Описание (галерея)', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::textarea('body_gallery_ru',  $post->body_gallery_ru, ['class' => 'form-control editor']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('body_video_ru', 'Описание (видео)', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::textarea('body_video_ru',  $post->body_video_ru, ['class' => 'form-control editor']) !!}
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="options_ru">
								<div class="form-group">
									{!! Form::label('meta_title_ru', 'Meta Title', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('meta_title_ru',  $post->meta_title_ru, ['class' => 'form-control']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('meta_keywords_ru', 'Meta Keywords', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('meta_keywords_ru',  $post->meta_keywords_ru, ['class' => 'form-control']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('meta_description_ru', 'Meta Description', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										<textarea name="meta_description_ru" id="meta_description_ru" class="form-control" rows="3">{{ $post->meta_description_ru }}</textarea>
									</div>
								</div>
							</div>
						</div>
						<!-- End panes -->
					</div>
				
					<!-- EN -->
					<div class="languages-content">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#record_en" aria-controls="record" role="tab" data-toggle="tab">Запись</a>
							</li>
							<li role="presentation">
								<a href="#options_en" aria-controls="options" role="tab" data-toggle="tab">Параметры записи</a>
							</li>
						</ul>
						<!-- End nav tabs -->
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="record_en">
								<div class="form-group">
									{!! Form::label('name_en', 'Название проекта', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('name_en',  $post->name_en, ['class' => 'form-control']) !!}
									</div>
								</div>
								<hr/>
								<div class="form-group">
									{!! Form::label('object_en', 'Объект', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('object_en',  $post->object_en, ['class' => 'form-control', 'placeholder' => 'Пример: Residential private house']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('location_en', 'Место нахождения', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('location_en',  $post->location_en, ['class' => 'form-control', 'placeholder' => 'Пример: Kiev, Ukraine']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('space_en', 'Пространство', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('space_en',  $post->space_en, ['class' => 'form-control', 'placeholder' => 'Пример: 5 hectares']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('client_en', 'Клиент', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('client_en',  $post->client_en, ['class' => 'form-control', 'placeholder' => 'Пример: Private person']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('photographer_en', 'Фотограф', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('photographer_en',  $post->photographer_en, ['class' => 'form-control']) !!}
									</div>
								</div>
								<hr/>
								<div class="form-group">
									{!! Form::label('annotation_en', 'Аннотация', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										<textarea name="annotation_en" id="annotation_en" class="form-control" rows="3">{{ $post->annotation_en }}</textarea>
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('body_slider_en', 'Описание (слайдер)', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::textarea('body_slider_en',  $post->body_slider_en, ['class' => 'form-control editor']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('body_gallery_en', 'Описание (галерея)', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::textarea('body_gallery_en',  $post->body_gallery_en, ['class' => 'form-control editor']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('body_video_en', 'Описание (видео)', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::textarea('body_video_en',  $post->body_video_en, ['class' => 'form-control editor']) !!}
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="options_en">
								<div class="form-group">
									{!! Form::label('meta_title_en', 'Meta Title', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('meta_title_en',  $post->meta_title_en, ['class' => 'form-control']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('meta_keywords_en', 'Meta Keywords', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										{!! Form::text('meta_keywords_en',  $post->meta_keywords_en, ['class' => 'form-control']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label('meta_description_en', 'Meta Description', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-10">
										<textarea name="meta_description_en" id="meta_description_en" class="form-control" rows="3">{{ $post->meta_description_en }}</textarea>
									</div>
								</div>
							</div>
						</div>
						<!-- End panes -->
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-success">Сохранить</button>
						</div>
					</div>
				
				{!! Form::close() !!}
				
			</div>
		</div>
	</div>
@stop

@section('scripts')
    @parent
	<script>
		// Переключение языков
		$('ul.languages-caption').on('click', 'li:not(.active)', function(){
			$(this).addClass('active').siblings().removeClass('active').closest('div.languages-container')
			.find('div.languages-content').removeClass('active').eq($(this).index()).addClass('active');
		});
		
		@if( !Request::segment(4) )
			// Генерация данных
			meta_title_touched = false;
			url_touched = false;

			$('input[name="slug"]').change(function(){ url_touched = true; });

			// RU
			$('input[name="name_ru"]').keyup(function(){
				if( !url_touched )
					$('input[name="slug"]').val(generate_url()); 
				if( !meta_title_touched )
					$('input[name="meta_title_ru"]').val( $('input[name="name_ru"]').val() );
			});
			$('input[name="meta_title_ru"]').change(function(){ meta_title_touched = true; });

			function generate_url(){
				url = $('input[name="name_ru"]').val();
				url = url.replace(/[\s]+/gi, '-');
				url = translit(url);
				url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();	
				return url;
			}

			function translit(str){
				var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")   
				var en=("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")   
				var res = '';
				for(var i=0, l=str.length; i<l; i++){ 
					var s = str.charAt(i), n = ru.indexOf(s);
					if(n >= 0) { res += en[n]; } 
					else { res += s; } 
				} 
				return res;
			}
		@endif
		
		// File input
		$(document).on('change', '.btn-file :file', function(){
			var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		});
		$(document).ready(function(){
			$('.btn-file :file').on('fileselect', function(event, numFiles, label){
				var input = $(this).parents('.input-group').find(':text'),
					log = numFiles > 1 ? numFiles + ' files selected' : label;
				
				if( input.length )
					input.val(log);
				else
					if( log ) 
						alert(log);
			});
		});
		
		// Remove resource
		$('.remove-resource').click(function(event){
			event.preventDefault();
			$(this).parent().parent().remove();
			
			if( $(this).data('action') != 'image' )
			{
				$('#video').val('');
				$('select[name="visible_main_slider"]').parent().parent().remove();
			}
			else
				$('#image').val('');
			
			data = { _token: '{{ csrf_token() }}', id: $(this).data('id'), action: $(this).data('action'), resource: $(this).data('resource') }
			$.post('{{ URL::to("master/projects/remove-resource") }}', data);	
		});
	</script>
	
	@include('admin.tinymceInit')
@endsection