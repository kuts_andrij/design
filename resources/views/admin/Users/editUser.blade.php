@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ URL::to('master') }}">Главная</a></li>
				<li><a href="{{ URL::to('master/users') }}">Пользователи</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif
			
			@if( $errors->any() )
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
			
			{!! Form::open(['class' => 'form-horizontal', 'role' => 'form']) !!}
			
				<div class="form-group">
					{!! Form::label('name', 'Имя', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-8">
						{!! Form::text('name',  $post->name, ['class' => 'form-control']) !!}
					</div>
				</div>
				
				<div class="form-group">
					{!! Form::label('email', 'E-mail', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-8">
						{!! Form::text('email',  $post->email, ['class'=>'form-control']) !!}
					</div>
				</div>

				<div class="form-group">
					{!! Form::label('password', 'Пароль', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-8">
						{!! Form::password('password', ['class' => 'form-control']) !!}
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>
			
			{!! Form::close() !!}
			
		</div>
	</div>
	
@stop