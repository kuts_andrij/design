<li @if( Request::segment(2) == 'menu' ) class="active" @endif>
	<a href="{{ URL::to('master/menu') }}"><i class="fa fa-bars"></i> Меню</a>
</li>
<li @if( Request::segment(2) == 'pages' ) class="active" @endif>
	<a href="{{ URL::to('master/pages') }}"><i class="fa fa-file"></i> Страницы</a>
</li>
<li @if( Request::segment(2) == 'about-page' ) class="active" @endif>
	<a href="{{ URL::to('master/about-page') }}"><i class="fa fa-star"></i> Обо мне</a>
</li>
<li @if( Request::segment(2) == 'projects-categories' ) class="active" @endif>
	<a href="{{ URL::to('master/projects-categories') }}"><i class="fa fa-object-ungroup"></i> Категории проектов</a>
</li>
<li @if( Request::segment(2) == 'projects' ) class="active" @endif>
	<a href="{{ URL::to('master/projects') }}"><i class="fa fa-object-group"></i> Проекты</a>
</li>
<li @if( Request::segment(2) == 'publications' ) class="active" @endif>
	<a href="{{ URL::to('master/publications') }}"><i class="fa fa-clipboard"></i> Публикации</a>
</li>
<li @if( Request::segment(2) == 'feedbacks' ) class="active" @endif>
	<a href="{{ URL::to('master/feedbacks') }}"><i class="fa fa-envelope"></i> Обратная связь @if( $feedback_count ) <span class="label label-danger">{{ $feedback_count }}</span> @endif</a>
</li>
<li @if( Request::segment(2) == 'users' ) class="active" @endif>
	<a href="{{ URL::to('master/users') }}"><i class="fa fa-users"></i> Пользователи</a>
</li>