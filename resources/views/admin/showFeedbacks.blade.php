@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ URL::to('master') }}">Главная</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if( count($posts) > 0 )
				{!! Form::open() !!}
					<table class="table">
						<thead>
							<tr>
								<th width="70%">Название</th>
								<th class="text-right" width="30%">Управление</th>
							</tr>
						</thead>
						<tbody>
							@foreach($posts as $post)
								<tr>
									<td>
										<input type="checkbox" name="check[]" value="{{ $post->id }}" />
										&ensp;Сообщение за {!! date('d.m.Y H:i', strtotime($post->created_at)) !!}
									</td>
									<td class="text-right">
										<div class="btn-group" role="group">
											<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#message-{{ $post->id }}" title="Посмотреть сообщение">
												<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
											</button>
											<button type="button" class="btn btn-danger delete" title="Удалить сообщение" data-id="{{ $post->id }}">
												<i class="fa fa-times"></i>
											</button>
										</div>
										<!-- Modal -->
										<div class="modal fade" id="message-{{ $post->id }}" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
														<h4 class="modal-title text-center">Сообщение за {!! date('d.m.Y', strtotime($post->created_at)) !!}</h4>
													</div>
													<div class="modal-body">
														<table class="table table-striped">
															<tr>
																<td><strong>Имя:</strong></td>
																<td>{{ $post->name }}</td>
															</tr>
															@if( $post->phone )
																<tr>
																	<td><strong>Номер телефона:</strong></td>
																	<td>{{ $post->phone }}</td>
																</tr>
															@endif
															@if( $post->email )
																<tr>
																	<td><strong>E-mail:</strong></td>
																	<td>{{ $post->email }}</td>
																</tr>
															@endif
															@if( $post->comment )
																<tr>
																	<td><strong>Сообщение:</strong></td>
																	<td>{{ $post->comment }}</td>
																</tr>
															@endif
														</table>
													</div>
												</div>
											</div>
										</div>
										<!-- End Modal -->
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					
					{!! $posts->render() !!}
					
					<div class="select_form">
						<label id="check_all" class="link">Выбрать все</label>
						<select name="action" class="form-control">
							<option value="delete">Удалить</option>
						</select>
						<button type="submit" class="btn btn-success delete-all" disabled>Применить</button>
					</div>

				{!! Form::close() !!}
			@else
				<div class="alert alert-warning">Сообщений нет :(</div>
			@endif
		</div>
	</div>
	
@stop

@section('scripts')
    @parent
	<script>
		$(function(){
			
			// Удаление сообщения
			$('.delete').click(function(){
				$('input[type="checkbox"][name*="check"]').prop('checked', false);
				$(this).closest('tr').find('input[type="checkbox"][name*="check"]').prop('checked', true);
				$(this).closest('form').find('select[name="action"] option[value=delete]').prop('selected', true);
				$(this).closest('form').submit();
			})

			// Удаление сообщений
			$('form').submit(function(){
				if( $('select[name="action"]').val() == 'delete' && !confirm('Подтвердите удаление') )
					return false;
			});

			// Выделить все
			$('#check_all').on('click', function(){
				$('input[type="checkbox"][name*="check"]:enabled').prop('checked', $('input[type="checkbox"][name*="check"]:enabled:not(:checked)').length > 0 );
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Активность кнопки "Удалить выбранные" 
			$('input[type="checkbox"][name*="check"]').change(function(){
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
		});
	</script>
@endsection