@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ URL::to('master') }}">Главная</a></li>
				<li><a href="{{ URL::to('master/menu') }}">Меню</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif
			
			@if( $errors->any() )
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
			
			{!! Form::open(['class' => 'form-horizontal', 'role' => 'form']) !!}
				<div class="form-group">
					{!! Form::label('slug', 'URL aдрес', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">&nbsp;/&nbsp;</span>
							{!! Form::text('slug',  $post->slug, ['class' => 'form-control']) !!}
						</div>
					</div>
				</div>

				<div class="form-group">
					{!! Form::label('parent_id', 'Родительский пункт меню', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::select('parent_id', $tree, $parent_id, ['class' => 'form-control']) !!}
					</div>
				</div>

				<div class="form-group">
					{!! Form::label('', 'Название', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li class="active"><a href="#name-ru-tab" role="tab" data-toggle="tab">RU</a></li>
							<li><a href="#name-en-tab" role="tab" data-toggle="tab">EN</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="name-ru-tab">
								{!! Form::text('name_ru',  $post->name_ru, ['class' => 'form-control']) !!}
							</div>
							<div role="tabpanel" class="tab-pane" id="name-en-tab">
								{!! Form::text('name_en',  $post->name_en, ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>
			{!! Form::close() !!}
			
		</div>
	</div>
	
@stop

@if( !Request::segment(4) )
	@section('scripts')
	    @parent
		<script>
			// Генерация данных
			url_touched = false;

			$('input[name="slug"]').change(function(){ url_touched = true; });

			$('input[name="name_ru"]').keyup(function(){
				if( !url_touched )
					$('input[name="slug"]').val(generate_url()); 
			});

			function generate_url(){
				url = $('input[name="name_ru"]').val();
				url = url.replace(/[\s]+/gi, '-');
				url = translit(url);
				url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();	
				return url;
			}

			function translit(str){
				var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")   
				var en=("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")   
				var res = '';
				for(var i=0, l=str.length; i<l; i++){ 
					var s = str.charAt(i), n = ru.indexOf(s);
					if(n >= 0) { res += en[n]; } 
					else { res += s; } 
				} 
				return res;
			}
		</script>
	@endsection
@endif