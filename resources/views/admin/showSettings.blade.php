@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ URL::to('master') }}">Главная</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif

			@if(Session::has('error'))
				<div class="alert alert-danger" role="alert">{{ Session::get('error') }}</div>
			@endif
			
			<h3>Контакты</h3>
			<hr/>

			{!! Form::open(['class' => 'form-horizontal', 'role' => 'form']) !!}
				<div class="form-group">
					{!! Form::label('email', 'E-mail', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::email('email', $settings->email, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('phone', 'Телефон', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::text('phone', $settings->phone, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('', 'Адрес', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li class="active"><a href="#address-ru-tab" role="tab" data-toggle="tab">RU</a></li>
							<li><a href="#address-en-tab" role="tab" data-toggle="tab">EN</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="address-ru-tab">
								{!! Form::text('address_ru', $settings->address_ru, ['class' => 'form-control']) !!}
							</div>
							<div role="tabpanel" class="tab-pane" id="address-en-tab">
								{!! Form::text('address_en', $settings->address_en, ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>
			{!! Form::close() !!}

			<h3>Социальные сети</h3>
			<hr/>

			{!! Form::open(['class' => 'form-horizontal', 'role' => 'form']) !!}

				<div class="form-group">
					{!! Form::label('facebook', 'Facebook', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::text('facebook', $settings->facebook, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('youtube', 'Youtube', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::text('youtube', $settings->youtube, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('instagram', 'Instagram', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::text('instagram', $settings->instagram, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>

			{!! Form::close() !!}

		</div>
	</div>
	
@stop