@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center">{{ $title }}</h2>
			
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif

			<div class="alert alert-warning">
				Редактирование описания этой страницы, доступно в модуле "Страницы" по ссылке: <a href="{{ URL::to('/master/pages/edit/4') }}">Александр Грабовский</a>
			</div>
			
			{!! Form::open(['class' => 'form-horizontal']) !!}

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Хронология</h3>
					</div>
					<div class="panel-body">
					
						@for( $i = 1; $i <= 3; $i++ )
							<div class="form-group">
								<label class="col-sm-2 control-label">Год #{{ $i }} (номер)</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="year_{{ $i }}_number" value="{{ $post->{'year_' . $i . '_number'} }}" />
								</div>
							</div>
						@endfor

						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li class="active"><a href="#chronology-ru-tab" role="tab" data-toggle="tab">RU</a></li>
							<li><a href="#chronology-en-tab" role="tab" data-toggle="tab">EN</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="chronology-ru-tab">
								
								@for( $i = 1; $i <= 3; $i++ )
									<div class="form-group">
										<label class="col-sm-2 control-label">Год #{{ $i }} (текст)</label>
										<div class="col-sm-10">
											<textarea class="form-control" rows="4" name="year_{{ $i }}_text_ru">{!! $post->{'year_' . $i . '_text_ru'} !!}</textarea>
										</div>
									</div>
								@endfor

							</div>
							<div role="tabpanel" class="tab-pane" id="chronology-en-tab">
								
								@for( $i = 1; $i <= 3; $i++ )
									<div class="form-group">
										<label class="col-sm-2 control-label">Год #{{ $i }} (текст)</label>
										<div class="col-sm-10">
											<textarea class="form-control" rows="4" name="year_{{ $i }}_text_en">{!! $post->{'year_' . $i . '_text_en'} !!}</textarea>
										</div>
									</div>
								@endfor

							</div>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Обучение и развитие</h3>
					</div>
					<div class="panel-body">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li class="active"><a href="#education-dev-ru-tab" role="tab" data-toggle="tab">RU</a></li>
							<li><a href="#education-dev-en-tab" role="tab" data-toggle="tab">EN</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="education-dev-ru-tab">
								<div class="form-group">
									<label class="col-sm-2 control-label">Заглавие</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="education_dev_title_ru" value="{!! $post->education_dev_title_ru !!}" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Текст</label>
									<div class="col-sm-10">
										<textarea class="form-control" rows="4" name="education_dev_text_ru">{!! $post->education_dev_text_ru !!}</textarea>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="education-dev-en-tab">
								<div class="form-group">
									<label class="col-sm-2 control-label">Заглавие</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="education_dev_title_en" value="{!! $post->education_dev_title_en !!}" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Текст</label>
									<div class="col-sm-10">
										<textarea class="form-control" rows="4" name="education_dev_text_en">{!! $post->education_dev_text_en !!}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Философия</h3>
					</div>
					<div class="panel-body">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li class="active"><a href="#philosophy-ru-tab" role="tab" data-toggle="tab">RU</a></li>
							<li><a href="#philosophy-en-tab" role="tab" data-toggle="tab">EN</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="philosophy-ru-tab">
								
								@for( $i = 1; $i <= 4; $i++ )
									<div class="form-group">
										<label class="col-sm-2 control-label">Текст #{{ $i }}</label>
										<div class="col-sm-10">
											<textarea class="form-control" rows="4" name="philosophy_{{ $i }}_text_ru">{!! $post->{'philosophy_' . $i . '_text_ru'} !!}</textarea>
										</div>
									</div>
								@endfor

							</div>
							<div role="tabpanel" class="tab-pane" id="philosophy-en-tab">
								
								@for( $i = 1; $i <= 4; $i++ )
									<div class="form-group">
										<label class="col-sm-2 control-label">Текст #{{ $i }}</label>
										<div class="col-sm-10">
											<textarea class="form-control" rows="4" name="philosophy_{{ $i }}_text_en">{!! $post->{'philosophy_' . $i . '_text_en'} !!}</textarea>
										</div>
									</div>
								@endfor

							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Как мы работаем?</h3>
					</div>
					<div class="panel-body">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li class="active"><a href="#how-work-ru-tab" role="tab" data-toggle="tab">RU</a></li>
							<li><a href="#how-work-en-tab" role="tab" data-toggle="tab">EN</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="how-work-ru-tab">
								
								@for( $i = 1; $i <= 4; $i++ )
									<div class="form-group">
										<label class="col-sm-2 control-label">Текст #{{ $i }}</label>
										<div class="col-sm-10">
											<textarea class="form-control" rows="4" name="how_work_{{ $i }}_text_ru">{!! $post->{'how_work_' . $i . '_text_ru'} !!}</textarea>
										</div>
									</div>
								@endfor

							</div>
							<div role="tabpanel" class="tab-pane" id="how-work-en-tab">
								
								@for( $i = 1; $i <= 4; $i++ )
									<div class="form-group">
										<label class="col-sm-2 control-label">Текст #{{ $i }}</label>
										<div class="col-sm-10">
											<textarea class="form-control" rows="4" name="how_work_{{ $i }}_text_en">{!! $post->{'how_work_' . $i . '_text_en'} !!}</textarea>
										</div>
									</div>
								@endfor

							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>

			{!! Form::close() !!}

		</div>
	</div>

@stop