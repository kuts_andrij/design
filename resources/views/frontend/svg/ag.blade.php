<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 		 viewBox="0 0 1240 750" xml:space="preserve">
				 <defs>
				  <clipPath id="vertical-dashed-mask">
				    <path fill="none" d="M600.4,132.5h-1v-6h1V132.5z M600.4,116.5h-1v-6h1V116.5z M600.4,100.5h-1v-6h1V100.5z M600.4,84.5h-1v-6h1
				      V84.5z M600.4,68.5h-1v-6h1V68.5z M600.4,52.5h-1v-6h1V52.5z M600.4,36.5h-1v-6h1V36.5z M600.4,20.5h-1v-6h1V20.5z M600.4,4.5h-1v-6
				      h1V4.5z"/>
				  </clipPath>

				  <clipPath id="circle-mask">
				    <path fill="none" d="M458.8,184.5c-27.9,0-50.5-22.7-50.5-50.5c0-27.9,22.7-50.5,50.5-50.5
				      c27.9,0,50.5,22.7,50.5,50.5C509.3,161.8,486.7,184.5,458.8,184.5z M458.8,84.4c-27.3,0-49.5,22.2-49.5,49.5s22.2,49.5,49.5,49.5
				      s49.5-22.2,49.5-49.5S486.1,84.4,458.8,84.4z"/>
				  </clipPath>

				  <clipPath id="arc-mask">
				    <path fill="none" d="M221.6,202.8c-0.9,0-1.8,0-2.7-0.1l0.1-2c2.7,0.1,5.4,0.1,8.1-0.2c2.7-0.2,5.4-0.6,8.1-1.1l0.4,2
				      c-2.7,0.6-5.5,0.9-8.3,1.2C225.3,202.7,223.4,202.8,221.6,202.8z M202.3,200c-5.4-1.6-10.5-3.8-15.4-6.6l1-1.7
				      c4.7,2.7,9.7,4.9,14.9,6.4L202.3,200z M251.3,196.1l-0.9-1.8c4.9-2.3,9.5-5.2,13.7-8.7l1.3,1.5C261.1,190.7,256.4,193.7,251.3,196.1
				      z M277.1,175.1l-1.6-1.2c3.3-4.3,6-9.1,8.1-14.1l1.8,0.8C283.2,165.8,280.4,170.6,277.1,175.1z M290.1,144.6l-2-0.3
				      c0.6-3.6,0.9-7.2,0.9-10.8c0-1.8-0.1-3.6-0.2-5.4l0-0.1l2-0.2l0,0.1c0.1,1.9,0.2,3.7,0.2,5.6C291,137.2,290.7,140.9,290.1,144.6z
				       M285.5,112c-1.7-5.1-4.1-10.1-7-14.7l1.7-1.1c3,4.7,5.4,9.8,7.2,15.1L285.5,112z M268.3,84.7c-3.9-3.8-8.3-7-13-9.8l1-1.7
				      c4.8,2.8,9.3,6.2,13.4,10L268.3,84.7z M240.3,68.6c-5.2-1.5-10.6-2.4-16-2.6l0.1-2c5.6,0.2,11.2,1.1,16.5,2.7L240.3,68.6z"/>
				  </clipPath>

				  <clipPath id="diag-dashed-mask">
				    <path fill="none" d="M0.9,373.5l-1.7-1l1.5-2.6l1.7,1L0.9,373.5z M14,351l-1.7-1l8.1-13.8l1.7,1L14,351z M33.7,317.4l-1.7-1
				      l8.1-13.8l1.7,1L33.7,317.4z M53.4,283.7l-1.7-1l8.1-13.8l1.7,1L53.4,283.7z M73,250l-1.7-1l8.1-13.8l1.7,1L73,250z M92.7,216.3
				      l-1.7-1l8.1-13.8l1.7,1L92.7,216.3z M112.4,182.7l-1.7-1l8.1-13.8l1.7,1L112.4,182.7z M132.1,149l-1.7-1l8.1-13.8l1.7,1L132.1,149z
				       M151.8,115.3l-1.7-1l8.1-13.8l1.7,1L151.8,115.3z M171.4,81.7l-1.7-1l8.1-13.8l1.7,1L171.4,81.7z M191.1,48l-1.7-1l8.1-13.8l1.7,1
				      L191.1,48z M210.8,14.3l-1.7-1l8.1-13.8l1.7,1L210.8,14.3z"/>
				  </clipPath>

				  <clipPath id="ag-mask" >
				    <path fill="none" d="M72.6,111.2v45h19.9v1.8H70.8v-46.7H72.6z M98.9,111.2v46.7h22.8v-1.8h-21v-20.8h13.5v-1.7h-13.5V113h21v-1.8H98.9z
				       M214.5,111.2v43.3l-25.2-43.3h-2.7v46.7h1.8v-45l25.1,43.3l1.1,1.7h1.7v-46.7H214.5z M514.7,111.4l-24.7,45.7v-45.7h-1.8v46.5h3.3
				      l25.2-46.5H514.7z M580.4,157.9h1.8v-46.7h-1.8V157.9z M442.7,151.4c4.6,4.7,10.3,7,16.8,7c6.6,0,12.2-2.3,16.8-7
				      c4.7-4.6,7-10.3,7-16.8c0-6.6-2.3-12.2-7-16.8c-4.6-4.7-10.3-7-16.8-7c-6.6,0-12.2,2.3-16.8,7c-4.7,4.6-7,10.3-7,16.8
				      C435.7,141.1,438.1,146.7,442.7,151.4 M444,119c4.3-4.3,9.5-6.5,15.6-6.5c6.1,0,11.3,2.2,15.6,6.5c4.3,4.3,6.5,9.5,6.5,15.6
				      c0,6.1-2.2,11.3-6.5,15.6c-4.3,4.3-9.5,6.5-15.6,6.5c-6.1,0-11.3-2.2-15.6-6.5c-4.3-4.3-6.5-9.5-6.5-15.6
				      C437.5,128.5,439.7,123.3,444,119 M422.2,157.9c3.3,0,6.2-1.2,8.6-3.6c2.4-2.4,3.6-5.3,3.6-8.6c0-2.9-0.9-5.4-2.7-7.6
				      c-1.8-2.2-4-3.6-6.7-4.2c-0.8-0.1-0.9-0.2-1.5-0.2c2-1,3.5-2.9,4.3-4.2c1.2-1.9,1.8-4,1.8-6.3c0-3.3-1.2-6.2-3.5-8.5
				      c-2.3-2.4-5.2-3.5-8.5-3.5h-9.7v46.7H422.2z M409.5,113h8c2.8,0,5.2,1,7.3,3c2,2,3,4.4,3,7.3c0,2.8-1,5.3-3,7.3c-2,2-4.4,3-7.3,3h-8
				      V113z M409.5,135.3h12.7c2.9,0,5.3,1,7.4,3c2,2,3.1,4.5,3.1,7.4c0,2.9-1,5.3-3.1,7.4c-2,2-4.5,3.1-7.4,3.1h-12.7V135.3z
				       M281.5,157.9l-11.7-22.4c0,0-0.1-0.3-0.1-0.3c2.4-0.5,4.7-1.8,6.3-3.7c2-2.4,3.1-5,3.1-8.1c0-3.4-1.2-6.2-3.6-8.6
				      c-2.4-2.4-5.3-3.6-8.6-3.6h-10.2v46.7h1.8v-22.4h9.4l11.6,22.4H281.5z M258.5,113h8.5c2.9,0,5.3,1,7.4,3.1c2,2,3.1,4.5,3.1,7.4
				      c0,2.9-1,5.3-3.1,7.4c-2,2-4.5,3.1-7.4,3.1h-8.5V113z M222.7,111.2v46.7h6.1c6.4,0,11.9-2.3,16.5-6.9c4.6-4.6,6.8-10.1,6.8-16.5
				      c0-6.4-2.3-11.9-6.8-16.5c-4.6-4.6-10.1-6.9-16.5-6.9H222.7z M224.4,156.2V113h4.3c5.9,0,11,2.1,15.3,6.3c4.2,4.2,6.3,9.3,6.3,15.3
				      c0,6-2.1,11.1-6.3,15.3c-4.2,4.2-9.3,6.3-15.3,6.3H224.4z M398,111.2l-29.5,46.7h2.1l29-45.9v45.9h1.7v-46.7H398z M400.6,133.6
				      h-15.5l-1.1,1.8h16.6L400.6,133.6z M177,111.2l-29.6,46.7h2.1l29-45.9v45.9h1.7v-46.7H177z M179.6,133.6H164l-1.1,1.8h16.6
				      L179.6,133.6z M61.2,111.2l-29.5,46.7h2.1l29-45.9v45.9h1.7v-46.7H61.2z M63.8,133.6H48.3l-1.1,1.8h16.6L63.8,133.6z M542.9,139
				      c-2.4-2.2-6-4-10.7-5.3c-8.8-2.5-13.1-6.2-12.9-11.2c0.1-2.3,0.7-4.2,1.9-5.7l0,0c2.4-2.6,5.7-4,9.8-4.3c0.4,0,0.7,0,1.1,0
				      c0.4,0,0.8,0,1.1,0c4,0.3,7.3,1.6,9.7,4.2c0.4,0.5,0.7,1,1,1.5l1.6-0.7c-0.3-0.7-0.7-1.3-1.2-1.9l0,0c-0.1-0.1-0.2-0.2-0.3-0.2
				      c-0.7-0.9-1.6-1.7-2.7-2.4c-2.3-1.4-5-2.1-8.1-2.2c-0.1,0-0.2,0-0.3,0c-0.3,0-0.6,0-0.9,0c-0.3,0-0.6,0-0.9,0c-0.1,0-0.2,0-0.3,0
				      c-3.1,0.1-5.9,0.8-8.1,2.2c-1.2,0.7-2.2,1.6-3,2.6c0,0,0,0,0,0l0,0c-1.4,1.8-2.2,4.1-2.4,6.8c-0.1,2.9,1,5.4,3.4,7.6
				      c2.4,2.2,6,3.9,10.7,5.3c8.8,2.5,13.1,6.2,12.9,11.3c0,0.9-0.2,1.7-0.5,2.5l0,0c-0.8,2.3-2.5,4.1-5,5.5c-2.2,1.2-4.6,1.9-7.2,2
				      c-0.2,0-0.3,0-0.5,0c-0.2,0-0.3,0-0.5,0c-2.6-0.1-5-0.8-7.2-2c-2.5-1.4-4.1-3.2-5-5.4c-0.2-0.5-0.3-1-0.4-1.4l-1.7,0.3
				      c0.1,0.6,0.3,1.1,0.5,1.7l0,0c0,0,0,0,0,0.1c0.7,1.9,2,3.7,3.9,5.2c2.8,2.2,6.1,3.4,10.1,3.4c0.1,0,0.2,0,0.3,0c0.1,0,0.2,0,0.3,0
				      c3.9,0,7.3-1.1,10.1-3.4c2.9-2.3,4.4-5.1,4.5-8.3C546.5,143.8,545.3,141.2,542.9,139 M575.7,111.2h-2.3L552,137.8v-26.6h-1.8v46.7
				      h1.8v-17.3l4.2-5.2l18.5,22.5h2.3l-19.7-23.9L575.7,111.2z M611,111.2l-10.8,18.3h-0.6l-10.8-18.3h-2L599,132v25.9h1.8V132
				      l12.2-20.8H611z M149,146.9l-7.9-13.5l13.2-22.4h-2l-12.2,20.7L127.8,111h-2l13.2,22.4l-14.4,24.4h2l13.4-22.7l7.9,13.4L149,146.9z
				       M368.5,149.3l-7.1-13.7h-9.4v22.4h-1.8v-46.7h10.2c3.3,0,6.2,1.2,8.6,3.6c2.4,2.4,3.6,5.3,3.6,8.6c0,3.1-1.1,5.8-3.1,8.1
				      c-1.6,1.9-3.9,3.2-6.3,3.7c0,0,0.1,0.3,0.1,0.3l6.3,12.1L368.5,149.3z M360.4,133.8c2.9,0,5.3-1,7.4-3.1c2-2,3.1-4.5,3.1-7.4
				      c0-2.9-1-5.3-3.1-7.4c-2-2-4.5-3.1-7.4-3.1h-8.5v20.9H360.4 M317.3,151.3c4.6,4.7,10.3,7,16.8,7c3.3,0,6.4-0.6,9.3-1.9l0.6-0.2
				      v-22.7h-13.5v1.8h11.7V155c-2.6,1-5.3,1.5-8.1,1.5c-6.1,0-11.3-2.2-15.6-6.5c-4.3-4.3-6.5-9.5-6.5-15.6c0-6.1,2.2-11.3,6.5-15.6
				      c4.3-4.3,9.5-6.5,15.6-6.5c3,0,5.9,0.6,8.6,1.7l0.7-1.6c-3-1.3-6-1.9-9.3-1.9c-6.6,0-12.2,2.3-16.8,7c-4.7,4.6-7,10.3-7,16.8
				      C310.3,141.1,312.6,146.7,317.3,151.3"/>
				  </clipPath>
				 </defs>


				<g fill="none" stroke="#fff" style="opacity:.1" stroke-width="2">
				<path id="l-d-1" d="M0 209.6 L131.8 0"/>
				<path id="l-v-1" d="M63.7 0  L63.7 753"/>
				<path id="l-d-2" d="M0 392 L248.5 0 "/>
				<path id="l-d-3" d="M294 397.8 L62.3 0"/>
				<path id="l-d-4" d="M268 134  L410 400"/>
				<path id="l-v-3" d="M179.3 -0.2  L179.3 198.9"/>
				<path id="l-v-4" d="M187.5 0  L186.9 284"/>
				<path id="l-v-5" d="M215.4 0  L214.6 157.9"/>
				<path id="l-v-6" d="M257.6 0  L258 267"/>
				<path id="l-h-1" d="M0 134.5 L1243 134.5"/>
				</g>

				<g fill="none" stroke="#fff" style="opacity:.1" stroke-width="1">
				<path id="l-v-2" d="M71.7 0 L71.7 157.9" />
				<path id="l-h-2" d="M70.8 157 L1073 157"/>
				</g>

				<g stroke="#fff" style="opacity:.1" stroke-width="3">
				<path id="vertical-dashed"  d="M599.8 138.3 L599.8 0" clip-path="url(#vertical-dashed-mask)"/>
				<path id="circle" class="st3" clip-path="url(#circle-mask)" d="M460.3,83.8c10.1,0,20,3.8,27.9,10c7.9,6.2,13.8,14.8,17.3,24.3c3.5,9.4,4.6,19.8,2,29.5
				  c-4.1,15-16.8,26.5-31,33c-2.7,1.2-5.5,2.3-8.4,2.7c-2.4,0.3-4.8,0.1-7.1,0.3c-2.4,0.2-4.7,0.8-7.1,0.7c-3.1,0-6.2-1.2-9.1-2.3
				  c-6.5-2.4-13.1-5-18.3-9.6c-3-2.7-5.4-6-7.7-9.4c-3.5-5.2-6.6-10.8-8.1-16.9c-1-3.9-1.3-7.9-1.6-11.8c-0.3-3.8-0.6-7.7,0.8-11.3
				c0.8-2.1,2-3.9,2.7-6.1c0.5-1.6,0.6-3.3,1.1-4.8c0.5-1.6,1.4-3,2.3-4.4c10-14.9,27.6-24.9,45.5-24.1"/>
				</g>

				<g stroke="#fff" style="opacity:.1" stroke-width="5">
				<path id="arc" clip-path="url(#arc-mask)"  d="M188.5,193.1c13.6,4.9,27.9,9.9,42.2,7.3c4.3-0.8,8.4-2.2,12.5-3.8c11.1-4.2,22.1-9.6,29.9-18.5
				c5.9-6.7,9.6-15,12.7-23.3c2.4-6.5,4.5-13.3,4.3-20.2c-0.1-4.7-1.2-9.4-2.3-14c-1.3-5.4-2.6-10.8-4.7-15.9
				c-4.7-11.8-13.2-21.9-23.8-28.9c-10.6-7-23.1-10.8-35.8-11.5"/>

				<path id="diag-dashed" clip-path="url(#diag-dashed-mask)"  d="M-1,374.1c6.1-11.7,12.8-23.2,19.5-34.6c20.1-34.5,40.2-68.9,60.4-103.4
				c32.1-54.9,64.1-109.8,96.2-164.8c14.3-24.4,28.5-48.8,43.8-72.6"/>
				</g>

				<g id="ag" clip-path="url(#ag-mask)" stroke="#ffffff" stroke-width="4" stroke-miterlimit="10" fill="none">
				<path id="a1" d="M31,159.2c2.3-4.9,6.5-8.6,9.4-13.2c1.2-2,2.2-4.1,3.2-6.1c4.6-9.1,10.7-17.3,16.7-25.4
				  c1-1.4,2.1-2.8,3.4-3.8c-0.1,1.1-0.1,2.2-0.2,3.2c-0.9,14.9-0.9,29.8-0.1,44.6"/>
				<path id="l" d="M72.1,109.3c2,15.9-1.5,32,0,48c1.1-0.7,2.4-0.7,3.7-0.6c5.7,0.2,11.5,0.3,17.2,0.5"/>
				<path id="a2" d="M60.8,134.3c-3.8,0.1-7.7,0.2-11.5,0.2c-0.2-0.2-0.2-0.4-0.5-0.4"/>
				<path id="e1" d="M122.8,111.8c-7.9,0.6-15.8,0.5-23.7-0.1c-0.5,15.2,1.9,30.5,0.6,45.6c7.3-0.5,14.7-1,22-0.3"/>
				<path id="e2" d="M101.7,134.1c4.2,0.9,8.6-0.7,12.9,0.1"/>
				<path id="x2" d="M126.1,110.2c8.9,12,13.4,26.9,22.4,38.7"/>
				<path id="x1" d="M125.1,159.2c0.2-1.4,1.2-2.5,2-3.6c2.1-2.6,3.8-5.6,5.4-8.5c7-12.3,13.9-24.6,20.9-36.9"/>
				<path id="aa1" d="M147.8,159.1c2.8-4.9,5.6-9.9,8.4-14.8c1.7-2.9,3.3-5.8,5.2-8.5c1.4-2.1,3-4,4.4-6.1
				  c1.9-2.9,3.4-5.9,5-8.9c1.9-3.4,3.9-6.7,6.2-9.9c0.4-0.5,1-1.1,1.5-0.8c0.2,0.1,0.3,0.3,0.4,0.6c0.6,1.3,0.5,2.8,0.4,4.3
				  c-0.7,14.7-0.7,29.4,0.1,44.1"/>
				<path id="aa2" d="M164.5,133.7c0.2,0,0.3,0.6,0.5,0.6c3.8-0.1,7.6-0.1,11.3,0.2c0.1,0,0.4-0.2,0.5-0.2"/>
				<path id="n" d="M188,158.5v-48.2c0.6,0,1.3,0.1,1.9,0.2c0.6,4.8,4.3,8.6,6.8,12.7c3.2,5.2,4.7,11.2,8.2,16.2
				  c1.3,1.8,2.7,3.3,3.9,5.1c2.8,4.4,3.6,9.9,6.8,14v-48.3"/>
				<path id="d" d="M222.6,111.3c2.9,1.1,6.1,0.7,9.2,1c6.4,0.6,12.3,4.3,15.9,9.7c3.5,5.3,4.6,12.2,3,18.4
				  c-1.6,6.2-5.8,11.6-11.3,14.9c-1.3,0.8-2.7,1.5-4.2,1.8c-1.9,0.5-3.8,0.5-5.7,0.5c-1.5,0-3.1,0-4.6,0c-0.3,0-0.7,0-0.9-0.2
				  c-0.5-0.3-0.6-0.9-0.6-1.5c-0.4-5.4-0.2-10.8,0-16.3c0.1-1.9,0.1-3.9,0.2-5.8c0.2-5.1,0.4-10.3,0.6-15.4c0.1-2.3,0.2-4.6-0.3-6.8"/>
				<path id="r" d="M280.8,158.2c-3.4-6.6-6.9-13.2-10.3-19.8c-0.5-1-1.1-2.1-2-2.8c-2.6-2.1-6.5-0.4-9.8-1.2
				  c4.1,0.5,8.3,0.2,12.2-0.8c0.8-0.2,1.7-0.5,2.4-1c0.8-0.6,1.4-1.5,2-2.4c0.8-1.3,1.6-2.7,2.1-4.1c1.3-3.9,0-8.5-3.1-11.2
				  c-2.6-2.2-6.1-3.1-9.5-3.4c-2.1-0.2-4.3-0.2-6.3,0.4c-0.4,0.1-0.7,0.3-1,0.6c-0.3,0.4-0.3,0.9-0.3,1.3c0,4.3,0.1,8.6,0.7,12.9
				  c0.3,1.8,0.6,3.6,0.7,5.4c0.1,1.6-0.1,3.2-0.3,4.8c-0.6,6.8-1,13.7-0.9,20.5c0.2,0.3,0,1.1,0,1.5"/>
				<path id="g" d="M330,134.5c4.7,0.1,9.4,0.2,14.2,0.3c-1.5,5.5-1.8,11.3-1.1,16.9c0.2,1.2,0.3,2.5-0.2,3.5
				  c-0.9,1.6-3,2-4.8,2.2c-5.3,0.5-11,0.9-15.6-1.8c-3-1.8-5.1-4.7-6.8-7.7c-2.1-3.5-3.8-7.4-4-11.5c-0.1-1.9,0.2-3.9,0.6-5.8
				  c0.8-3.9,2.2-7.8,4.7-11c3-3.9,7.5-6.6,12.3-7.7c4.8-1.1,9.9-0.7,14.5,1"/>
				<path id="rr" d="M351.1,159.5c0.1-12.8,0.1-25.7,0.2-38.5c0-2.8,0-5.5,0-8.3c0-0.3,0-0.7,0.2-0.9
				  c0.2-0.2,0.6-0.3,0.9-0.3c3.3-0.1,6.6,0,9.9,0.3c2,0.2,4.1,0.4,5.5,1.8c0.5,0.5,0.9,1.1,1.2,1.7c1.4,2.3,2.7,4.9,2.8,7.6
				  c0.1,4.4-3,8.4-7,10.3c-4,1.9-8.6,1.8-12.9,1c2.6,0.3,5.2,0.6,7.8,0.9c0.5,0.1,1,0.1,1.4,0.4c0.4,0.3,0.7,0.7,1,1.1
				  c2.5,4.1,5,8.1,7.5,12.2"/>
				<path id="aaa1" d="M368.9,158.4c2.3-4.1,4.7-8.2,7.1-12.2c1.4-2.4,2.8-4.8,4.5-7c1.7-2.2,3.7-4.3,5.3-6.6
				  c1.2-1.7,2.1-3.6,3.1-5.4c3.2-5.8,6.8-11.3,10.9-16.5c-0.2,5.4-0.4,10.9,0.1,16.3c0.2,2.7,0.6,5.5,0.3,8.2c-0.3,1.9-1,3.8-1.1,5.8
				  c-0.2,2.1,0.3,4.1,0.6,6.2c0.5,3.9,0.6,7.9,0.1,11.9"/>
				<path id="aaa2" d="M386.3,134.9c4.1,0,8.1-0.3,12.2-0.7"/>
				<path id="b" d="M408.5,158.6c0-0.3,0-0.7,0-1c0.3-14.9,0.6-29.8,0.9-44.8c0-0.4,0-0.7,0.3-1c0.3-0.3,0.7-0.3,1.1-0.3
				  c4.1-0.2,8.3,0.1,11.9,2.1c3.6,2,6.3,5.9,5.8,10c-0.3,2.4-1.7,4.5-3,6.6c-0.7,1-1.4,2-2.4,2.7c-1.1,0.7-2.4,0.9-3.7,1
				  c-2.8,0.3-5.6,0.4-8.4,0.3c3.4-0.2,6.9-0.1,10.3,0.2c2.4,0.2,5,0.6,7,2.1c2.4,1.8,3.4,4.7,4.1,7.6c0.3,1.2,0.5,2.5,0.5,3.8
				  c0,2.5-1.1,5-2.9,6.7c-3.7,3.6-9.4,3.6-14.5,3.4c-2.3-0.1-4.6-0.1-6.8-0.2"/>
				<path id="o" d="M460.1,111.3c2.8-0.5,5.6,0.5,8.2,1.7c4.2,1.9,8.3,4.4,10.5,8.3c0.7,1.2,1.2,2.6,1.6,3.9
				  c0.8,2.3,1.5,4.7,1.8,7.1c0.7,4.8-0.2,9.9-2.6,14.1c-2.4,4.3-6.3,7.7-10.9,9.3c-3.8,1.4-7.9,1.5-11.9,1.6c-2.6,0-5.3-0.1-7.5-1.4
				  c-1.2-0.7-2.2-1.8-3.3-2.6c-1.1-0.8-2.4-1.4-3.4-2.3c-2.8-2.2-4.2-5.6-5.2-8.9c-0.7-2.2-1.2-4.5-1.3-6.8c-0.4-6.3,2.4-12.6,7-16.9
				  s10.7-6.8,17-7.4"/>
				<path id="v" d="M489,110.7v46c0.5-0.4,1.4,0.4,1.2,0.9c0.9,0.4,1.6-0.7,1.9-1.6c1-3.1,1.8-6.4,3.8-9
				  c1.4-1.7,3.2-3.1,4.3-5c0.8-1.4,1.1-2.9,1.7-4.4c1.2-2.9,3.5-5.2,5.2-7.9c3.8-5.9,4.7-13.4,8.9-19"/>
				<path id="s" d="M516.9,147.8c1,0.9,1.2,2.3,1.7,3.6c1.3,3.3,4.7,5.1,8.1,6.1c2.8,0.8,5.8,1.2,8.6,0.7s5.6-2,7.3-4.4
				  c0.6-0.9,1-1.8,1.4-2.8c1.1-2.8,1.5-6.1,0.2-8.9c-1.7-3.5-5.6-5.2-9.2-6.5c-3.4-1.3-6.9-2.6-10.3-3.9c-1-0.4-2.1-0.8-2.9-1.6
				  c-0.5-0.5-0.9-1.2-1.2-1.8c-0.8-1.5-1.6-3-1.8-4.7c-0.5-3.6,1.8-7.2,4.9-9.1c3.1-1.9,6.9-2.3,10.5-2.3c2.2,0,4.5,0.3,6.5,1.3
				  c2,1,3.8,2.7,4.2,4.9"/>
				<path id="k1" d="M550.8,110.3c0.2,16.5,0.5,33,0.7,49.5"/>
				<path id="k2" d="M576.4,109.7c-7.6,9.5-15.5,18.9-23.6,28.1c-0.2,0.2-0.3,0.2-0.5,0.4"/>
				<path id="k3" d="M557,135.2c3,3.3,6.1,6.6,9.1,9.9c3.9,4.2,7.8,8.4,10.3,13.4"/>
				<path id="i" d="M581.8,159.5c-0.7-0.5-0.8-3.2-0.7-4c0.2-15,0.5-30.1,0.7-45.1"/>
				<path id="y1" d="M612.7,110.2c-0.3,0.3-0.3,0.9-0.5,1.2c-2.1,3.5-4.1,6.9-6.2,10.4c-0.5,0.9-1,1.7-1.5,2.6
				  c-1.1,1.9-2.3,3.9-3.7,5.6c-0.1,0.1-0.2,0.3-0.4,0.4c-0.5,0.2-1-0.4-1.2-0.9c-3.2-6.5-7-12.7-11.3-18.6c-0.2-0.2-0.4-0.7-0.7-0.9"/>
				<path id="y2" d="M600.1,159.8c-0.5-3.2-0.8-6.4-0.9-9.7c-0.2-6.1,0.4-12.2,1-18.3"/>
				</g>
				</svg>