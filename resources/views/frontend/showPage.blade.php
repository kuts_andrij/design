@extends('frontend.layout')

@section('main')

	<section class="section section--inner   is-section-preanimated is-transition-enabled"  >

		<div class="cover">
			<div class="cover__items">
				<div class="cover__item cover__item--left-top">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/' ) }}" class="logo-text">
						<img src="{{ URL::to('images/logo-text.png') }}" alt="" />
					</a>
					@include('frontend.partials.audio')

					@include('frontend.partials.lang')
				</div>

				<div class="cover__item cover__item--center-bottom">
					<button class="btn btn--square btn--dashed-bottom is-animated js-scroll-down"><span class="fi-arrow-down scroll-down"></span></button>
				</div>


				<div class="cover__item cover__item--left-bottom">
					<h2 class="cover__heading">{{ $page->name }}</h2>
				</div>

			</div>

			<div class="cover__media" style="background-image: url('{{ $page->image }}');">
				@if( $page->video_file )
					<video muted loop preload="none" id="inner-video">
						<source type="video/mp4" src="{{ URL::to('uploads/pages/video/' . $page->video_file) }}" >
					</video>
				@endif
			</div>
    	</div>

		<div class="section__line"></div>
  	</section>

	<section class="map">

		@if( $page->body )
			<footer class="map__footer">
				{!! $page->body !!}
			</footer>
		@endif

	</section>

	@include('frontend.partials.footer', ['footerClass' => 'footer--dark'])
@endsection