<!DOCTYPE html>
<html>
    <head>
        @if( isset( $page->meta_title ) )
          <title>{{ $page->meta_title }}</title>
        @endif
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        @if( isset( $page->meta_keywords ) )
          <meta name="keywords" content="{{ $page->meta_keywords }}">
        @endif
        @if( isset( $page->meta_description ) )
          <meta name="description" content="{{ $page->meta_description }}">
        @endif
		<!-- Styles -->
		@section('styles')
			<link href="{{ asset('css/app.css') }}" media="all" rel="stylesheet"  />
		@show

		<!-- Disabled right mouse click -->
		<script>
			document.oncontextmenu = new Function("return false;");
		</script>
    </head>

      <body oncontextmenu="return false" class="is-preloader-visible">
        <script type="text/javascript">
          if (sessionStorage.getItem('preloader') || location.pathname !== '/') {
            document.body.classList.remove('is-preloader-visible');
          }
        </script>
        @include('frontend.partials.preloader')

        <audio class="js-audio-hover" >
          <source src="{{ URL::to('audio/hover.mp3') }}" type="audio/mpeg">
        </audio>

        <div class="wrapper" >
          @include('frontend.partials.menu')



  		    @yield('main')

        </div>

  		<!-- Scripts -->
  		@section('scripts')
  			<script src="{{ asset('js/app.js') }}"></script>
  		@show
    </body>
</html>
