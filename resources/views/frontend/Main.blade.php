@extends('frontend.layout')

@section('main')


<div class="js-fp">
	<section class="section section--home is-section-preanimated is-transition-enabled" data-transitionend=".menu__open" style="display: no ne ;">
		@include('frontend.partials.section-overlay')

		@if( count($projects_left) )
			@include('frontend.partials.projects')
		@endif

		<div class="cover">
			<div class="cover__items">
				<div class="cover__item cover__item--left-top">
					@include('frontend.partials.audio')

					@include('frontend.partials.lang')
				</div>

				@if( count($projects_left) )
					<div class="cover__item cover__item--left-center">
						<button class="btn btn--dashed-left btn--vertical js-projects-open"><span>{{ trans('design.projects') }}</span></button>
					</div>
				@endif

				<div class="cover__item cover__item--left-bottom">
					@include('frontend.partials.social')
				</div>

				<div class="cover__item cover__item--center-top">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/' ) }}">
						<img src="{{ URL::to('images/logo-text.png') }}" alt="" />
					</a>
				</div>

				<div class="cover__item cover__item--center-bottom">
					<button class="btn btn--square btn--dashed-bottom js-fp-down"><span class="fi-arrow-down scroll-down"></span></button>
				</div>

				@if( count($projects_slider) )
					<div class="cover__item cover__item--right-bottom">
						@if( count($projects_slider) > 1 )
							<div class="slider__nav btn-group btn-group--end">
								<button class="btn btn--square js-video-slider-prev"><i class="fi-arrow-left"></i></button>
								<button class="btn btn--square js-video-slider-next"><i class="fi-arrow-right"></i></button>
							</div>
						@endif
						@foreach( $projects_slider as $project )
							<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/project/' . $project->slug ) }}" class="cover__link btn btn--md btn--dashed-right js-video-slider-link">{{ $project->name }}</a>
							<?php break; ?>
						@endforeach
					</div>
				@endif

			</div>

	    <div class="slider js-video-slider">

			@if( count($projects_slider) )
				@foreach( $projects_slider as $project )
					<div class="slider__slide" data-url="{{ LaravelLocalization::getLocalizedURL( $locale, '/project/' . $project->slug ) }}" data-text="{{ $project->name }}">
						<?php
							if( $project->video_screen )
								$project->video_screen = URL::to('uploads/projects/video/screenshots/' . $project->video_screen);
							else
								$project->video_screen = 'http://placehold.it/1920x645?text=No background';
						?>
						<div class="cover__media" style="background-image: url('{{ $project->video_screen }}');">
							@if( $project->video_file )
								<video muted loop  preload="none">
									<source type="video/mp4" src="{{ URL::to('uploads/projects/video/' . $project->video_file) }}" >
								</video>
							@endif
						</div>
						<div class="slider__overlay overlay">
							<div class="overlay__item overlay__item--black"></div>
							<div class="overlay__item overlay__item--green"></div>
						</div>
					</div>
				@endforeach
			@else
				<div class="slider__slide" data-url="#" data-text="No project">
					<div class="cover__media" style="background-image: url('http://placehold.it/1920x645?text=No background');"></div>
					<div class="slider__overlay overlay">
						<div class="overlay__item overlay__item--black"></div>
						<div class="overlay__item overlay__item--green"></div>
					</div>
				</div>
			@endif

	    </div>
    </div>
  </section>

	<section class="section section--about fp-auto-height-responsive is-section-preanimated is-transition-enabled" data-transitionend=".section--about .section__image div" >
		@include('frontend.partials.section-overlay')

		<div class="section__heading">
			<div class="svg-wrap">
				@include('frontend.svg.ag')
			</div>
		</div>


		<div class="section__image">
      <div></div>
			<img src="{{ $main_page->section_2_photo }}" alt="" />
			@include('frontend.svg.sabout-border')
			<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/about' ) }}" class="btn btn--dashed-right">{{ trans('design.about_me') }}</a>
		</div>

		<div class="section__text">
			<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/publications' ) }}" class="btn btn--dashed-top">{{ trans('design.publications') }}</a>

			<blockquote>
				{!! $main_page->section_2_content !!}
			</blockquote>
		</div>

		<div class="section__btn">
			<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/projects-honors' ) }}" class="btn btn--dashed-left">{{ trans('design.rewards') }}</a>
		</div>

    <div class="section__scrollDown">
      <button class="btn btn--square btn--dashed-bottom js-fp-down"><span class="fi-arrow-down scroll-down"></span></button>
    </div>
	</section>

	<section class="section section--projects fp-auto-height-responsive is-section-preanimated is-transition-enabled" data-transitionend=".section__photo--4 .dot__title"   >
		@include('frontend.partials.section-overlay')
		<div class="section__item section__item--lamp">
			<img src="{{ URL::to('images/lamp.png') }}" alt="" />
		</div>

		{{-- 360x265 --}}
		<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/category/completed-projects' ) }}" class="section__photo section__photo--1">
			<div class="photo-card">
				<img src="{{ $main_page->section_3_image_1 }}" alt="" />
				{{--
					<img src="images/t_photocard2a.jpg" alt="" />
					<img src="images/t_photocard2b.jpg" alt="" />
				--}}
			</div>
      		<div class="dot">
        		<div class="dot__circle"></div>
        		<div class="dot__line">
          			<svg  xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 215 115"><path d="M0 115 L115 0 L215 0" stroke-dasharray="263"/></svg>
        		</div>
        		<div class="dot__title">{!! trans('design.completed_projects') !!}</div>
      		</div>
		</a>

		<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/category/interior-design' ) }}" class="section__photo section__photo--2">
			<div class="photo-card ">
				<img src="{{ $main_page->section_3_image_2 }}" alt="" />
				{{--
					<img src="images/t_photocard1a.jpg" alt="" />
					<img src="images/t_photocard1b.jpg" alt="" />
				--}}
			</div>
      		<div class="dot">
        		<div class="dot__circle"></div>
        		<div class="dot__line">
          			<svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 210 50"><path d="M0 50 L50 0 L210 0" stroke-dasharray="231"/></svg>
        		</div>
        		<div class="dot__title">{{ trans('design.interior') }}</div>
      		</div>
		</a>

		<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/category/architecture' ) }}" class="section__photo section__photo--3">
			<div class="photo-card ">
				<img src="{{ $main_page->section_3_image_3 }}" alt="" />
				{{--
					<img src="images/t_photocard3a.jpg" alt="" />
					<img src="images/t_photocard3b.jpg" alt="" />
				--}}
			</div>
      		<div class="dot">
        		<div class="dot__circle"></div>
        		<div class="dot__line">
          			<svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 235 50"><path d="M0 50 L50 0 L235 0" stroke-dasharray="256"/></svg>
        		</div>
        		<div class="dot__title">{{ trans('design.architecture') }}</div>
      		</div>
		</a>

		<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/category/product-design' ) }}" class="section__photo section__photo--4">
			<div class="photo-card">
				<img src="{{ $main_page->section_3_image_4 }}" alt="" />
				{{--
					<img src="images/t_photocard4a.jpg" alt="" />
					<img src="images/t_photocard4b.jpg" alt="" />
				--}}
			</div>
     		<div class="dot">
        		<div class="dot__circle"></div>
        		<div class="dot__line">
          			<svg  xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 180 60"><path d="M180 60 L160 0 L0 0" stroke-dasharray="228"/></svg>
        		</div>
        		<div class="dot__title">{{ trans('design.product_design') }}</div>
      		</div>
		</a>

		<div class="section__logo">
			<img src="{{ URL::to('images/logo-wood.png') }}" alt="" />
		</div>

		<div class="section__item section__item--macbook">
			<img src="{{ URL::to('images/macbook.png') }}" alt="" />
		</div>
		<div class="section__item section__item--copic">
			<img src="{{ URL::to('images/copic.png') }}" alt="" />
		</div>
		<div class="section__item section__item--ruler">
			<img src="{{ URL::to('images/ruler.png') }}" alt="" />
		</div>
		<div class="section__item section__item--pencil">
			<img src="{{ URL::to('images/pencil.png') }}" alt="" />
		</div>

	    <div class="section__scrollDown">
	      <button class="btn btn--square btn--dashed-bottom js-fp-down"><span class="fi-arrow-down scroll-down"></span></button>
	    </div>
	</section>

	<section class="section section--gallery is-section-preanimated is-transition-enabled" data-transitionend=".desktop__title--right .desktop__dotline"  >
		@include('frontend.partials.section-overlay')

		<div class="section__photos" >
			<div class="camera">
				<div class="camera__btn">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/projects-photo' ) }}" class="btn btn--dashed-right"><i class="fi-picture mr-1"></i>{{ trans('design.photo_gallery') }}</a>
				</div>
				<div class="camera__camera">
					<img src="{{ URL::to('images/camera.png') }}" alt="" />
					<div class="camera__image" style="background-image: url({{ URL::to('images/t_cover1.jpg') }});"></div>
				</div>
			</div>
		</div>

		@include('frontend.svg.lights')

		<div class="section__videos" >
			<div class="desktop">
				{{-- 601x434 72.21--}}

				@include('frontend.svg.desktop-border')

				<div class="desktop__image"></div>

				<div class="desktop__title desktop__title--left">
					<span class="desktop__text">
						<span>
							{{ trans('design.from_sketch') }}
							<span class="desktop__dotline desktop__dotline--left"></span>
						</span>
					</span>
				</div>
				<div class="desktop__title desktop__title--right">
					<span class="desktop__text">
						<span>
							{{ trans('design.to_completion') }}
							<span class="desktop__dotline"></span>
						</span>
					</span>
				</div>

				<div class="desktop__btn">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/projects-video' ) }}" class="btn btn--dashed-left"><i class="fi-play mr-1"></i>{{ trans('design.video') }}</a>
				</div>
			</div>
		</div>

		<div class="section__bottom"></div>

	    <div class="section__scrollDown">
	      <button class="btn btn--square btn--dashed-bottom js-fp-down"><span class="fi-arrow-down scroll-down"></span></button>
	    </div>
	</section>

	@if( count($project_honors) )
		<section class="section section--rewards is-section-preanimated is-transition-enabled"  data-transitionend=".section--rewards .section__line">
			@include('frontend.partials.section-overlay')

			<div class="section__cover">
				<div class="cover">
					<?php
						if( $project_honors->video_screen )
							$project_honors->video_screen = URL::to('uploads/projects/video/screenshots/' . $project_honors->video_screen);
						else
							$project_honors->video_screen = 'http://placehold.it/1920x645?text=No background';
					?>
					<div class="cover__media" style="background-image: url('{{ $project_honors->video_screen }}');">
						@if( $project_honors->video_file )
							<video muted loop  preload="none"  data-autoplay>
								<source type="video/mp4" src="{{ URL::to('uploads/projects/video/' . $project_honors->video_file) }}" >
							</video>
						@endif
					</div>
				</div>
			</div>

			<div class="section__footer">
				<div class="section__heading subheading">«{{ $project_honors->name }}»</div>

				@if( count($project_honors->images) )
					<div class="section__rewards">
						<div class="rewards-short">
							<div class="rewards-short__text">
								<h5 class="rewards-short__title">{{ trans('design.rewards') }}:</h5>
								{{--
									<p>Best residential private house</p>
									<p>House design in forest</p>
								--}}
							</div>
							@foreach( $project_honors->images as $item )
								<div class="rewards-short__image">
									<img src="{{ URL::to('uploads/projects/images/thumbnail/' . $item->image) }}" alt="{{{ $item->alt }}}" @if( $item->title ) title="{{{ $item->title }}}" @endif />
								</div>
							@endforeach
						</div>
					</div>
				@endif

				<div class="section__btn">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/projects-honors' ) }}" class="btn btn-dashed btn--dashed-right">{{ trans('design.all_rewards') }}</a>
				</div>
			</div>

			<div class="section__line"></div>
		</section>
	@endif

	<!--
		<section class="section section--media is-section-preanimated is-transition-enabled" data-transitionend=".interviews__btn"  >
			@include('frontend.partials.section-overlay')

			<div class="section__logo">
				<img src="{{ URL::to('images/logo-transparent.png') }}" alt="" />
			</div>

			<div class="articles">
				<div class="articles__items">
					{{-- 139x184 --}}
					<div class="articles__item">
						<img src="{{ URL::to('images/t_article1.jpg') }}" alt="" />
					</div>
					<div class="articles__item">
						<img src="{{ URL::to('images/t_article2.jpg') }}" alt="" />
					</div>
					<div class="articles__item">
						<img src="{{ URL::to('images/t_article3.jpg') }}" alt="" />
					</div>
				</div>

				<div class="articles__line"></div>
				<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/publications' ) }}" class="btn btn--dashed articles__btn">{{ trans('design.articles') }}</a>
			</div>

			<div class="diary">
				<div class="diary__ipad">
					<div class="diary__image">
						{{-- 399x314 --}}
						<img src="{{ URL::to('images/t_diary.jpg') }}" alt="" />
					</div>
				</div>

				<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/projects-video' ) }}" class="diary__btn btn btn--dashed">{{ trans('design.video_diary') }}</a>

				<div class="diary__line"></div>
			</div>

			<div class="interviews">
				<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/category/projects-all' ) }}" class="interviews__btn btn btn--dashed">{{ trans('design.interviews') }}</a>

				<div class="interviews__image">
					@include('frontend.svg.sinterview-border')
					{{-- 649x274 --}}
					<img src="{{ $main_page->section_6_photo }}" alt="" />
					<div class="interviews__line"></div>
				</div>
				<div class="interviews__text">
					<p>
						{!! $main_page->section_6_content !!}
					</p>
				</div>
			</div>

			<div class="section__bottom"></div>
			<div class="section__scrollDown">
				<button class="btn btn--square btn--dashed-bottom js-fp-down"><span class="fi-arrow-down scroll-down"></span></button>
			</div>
		</section>
	-->

	<section class="section section--contacts is-section-preanimated is-transition-enabled" data-transitionend=".section--contacts .anim-end" >
		@include('frontend.partials.section-overlay')

		<div class="section__cover">
			<div class="cover">
				<div class="cover__media" style="background-image: url({{ URL::to('images/bg-contacts.jpg') }});"></div>
			</div>
		</div>

		<div class="section__heading">
			<div class="heading is-heading-animated">
				<div class="heading__text subheading"><span>{{ trans('design.contacts') }}</span></div>
				<div class="heading__line"></div>

				<div class="heading__content">
					<p>Нужна помощь в создании интерьера своей мечты?</p>
					<h5>Давайте встретимся!</h5>
				</div>
			</div>
		</div>


		<div class="section__line"></div>

		<div class="section__bottom">
			@include('frontend.partials.footer', ['footerClass' => 'footer--white', 'socialClass'  => 'social--dark'])
			<div class="section__footer">
				<div class="section__copyright">
					@include('frontend.partials.copyright')
				</div>
				<div class="section__text">
					<h5>{{ trans('design.main_footer_text_1') }}</h5>
					<p>{{ trans('design.main_footer_text_2') }}</p>
				</div>
			</div>
		</div>

	</section>
</div>
@endsection

@section('scripts')
    @parent
	<script src="{{ asset('js/home.js') }}"></script>
@endsection
