@extends('frontend.layout')

@section('main')

<div class="contacts">
	<section class="section section--inner   is-section-preanimated is-transition-enabled"  >

		<div class="cover">
			<div class="cover__items">
				<div class="cover__item cover__item--left-top">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/' ) }}" class="logo-text">
						<img src="{{ URL::to('images/logo-text.png') }}" alt="" />
					</a>
					@include('frontend.partials.audio')

					@include('frontend.partials.lang')
				</div>

				<div class="cover__item cover__item--center-bottom">
					@include('frontend.partials.social')
				</div>

				<div class="cover__item cover__item--left-bottom">
					<h2 class="cover__heading">{{ $page->name }}</h2>
				</div>
			</div>

			<div class="cover__media" style="background-image: url('{{ $page->image }}');"></div>
		</div>

		<div class="section__line"></div>
	</section>

	<section class="contacts__body js-waypoint is-preanimated">
		<div class="contacts__top">
			<div class="contacts__container">
				<div class="contacts__row">
					
					@if( $settings->email )
						<div class="contacts__col">
							<div class="contacts__title"><i class="fi-mail"></i></div>
							<a href="mailto:{{ $settings->email }}" class="contacts__link">{{ $settings->email }}</a>
						</div>
					@endif

					@if( $settings->phone )
						<div class="contacts__col">
							<div class="contacts__title"><i class="fi-phone"></i></div>
							<a href="tel:{{ str_replace(['(', ')', ' ', '-'], '', $settings->phone) }}" class="contacts__link">{{ $settings->phone }}</a>
						</div>
					@endif

					@if( $settings->address )
						<div class="contacts__col">
							<div class="contacts__title"><i class="fi-pin"></i></div>
							<p>{{ $settings->address  }}</p>
						</div>
					@endif

				</div>

				<div class="contacts__row">
					<div class="contacts__col">
						<div class="contacts__social">
							<p>{{ trans('design.social_media') }}</p>
							@include('frontend.partials.social', ['socialClass' => '' ])
						</div>
					</div>

					<div class="contacts__col contacts__col--1">
						<button class="btn contacts__btn js-cf-open">{{ trans('design.contact_us') }}</button>
					</div>
				</div>
			</div>
		</div>

		{!! Form::open(['class' => 'contacts__form contact-form js-cf']) !!}
			<div class="contact-form__inner">
				<div class="contact-form__close">
					<a href="#" class="btn btn--square js-cf-close"><i class="fi-close"></i></a>
				</div>

				<div class="contact-form__row">
					<div class="contact-form__left">
						<input type="text" class="contact-form__input input" placeholder="{{ trans('design.input_name') }}" name="name" required>

						<input type="tel" class="contact-form__input input" placeholder="{{ trans('design.input_phone') }}" name="phone" required>

						<input type="email" class="contact-form__input input" placeholder="{{ trans('design.input_email') }}" name="email" required>
					</div>

					<div class="contact-form__right">
						<textarea name="comment" placeholder="{{ trans('design.input_comment') }}" required  class="contact-form__textarea input"></textarea>

						<div class="contact-form__footer">
							<button class="btn contact-form__submit">{{ trans('design.input_send') }}</button>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}

		<div class="contacts__svg">
		@include('frontend.svg.contacts')
    </div>

	</section>

	<section class="contacts__map">
		<div id="cmap" data-lat="48.462660" data-lng="35.050500" data-title="{{ $settings->address  }}"></div>
	</section>
</div>
@endsection

@section('styles')
	@parent
	<link href="{{ URL::to('css/sweetalert.css') }}" media="all" rel="stylesheet"  />
@endsection

@section('scripts')
	@parent
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-oDvfPlkf2fc1QAqsFI-zZnpIadN21og"></script>
	<script src="{{ URL::to('js/sweetalert.min.js') }}"></script>
	<script>
		// AJAX form
		$('form.contact-form').submit(function(event){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
				url: $(this).attr('action'),
				dataType: 'JSON',
				data: $(this).serialize(),
				success: function( response ){
					if( response.success )
					{
						$('form.contact-form')[0].reset();
						swal("{{ trans('design.message_thank_you') }}", response.success, 'success');
					}
					else
					{
						var	errorsHTML = '';
						$.each( response.errors, function( key, value ){
							errorsHTML += value + "\n\r";
						});
						swal("{{ trans('design.message_error') }}", errorsHTML, 'error');
					}
				},
				error: function( xhr, ajaxOptions, thrownError ){
					swal("{{ trans('design.message_sys_error') }}", xhr.status + ' ' + thrownError, 'error');
				}
			});
		});
	</script>
@endsection