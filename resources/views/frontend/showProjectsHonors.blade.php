@extends('frontend.layout')

@section('main')

	<section class="section section--inner   is-section-preanimated is-transition-enabled"  >

		<div class="cover">
			<div class="cover__items">
				<div class="cover__item cover__item--left-top">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/' ) }}" class="logo-text">
						<img src="{{ URL::to('images/logo-text.png') }}" alt="" />
					</a>
					@include('frontend.partials.audio')

					@include('frontend.partials.lang')
				</div>

				<div class="cover__item cover__item--center-bottom">
					<button class="btn btn--square btn--dashed-bottom is-animated js-scroll-down"><span class="fi-arrow-down scroll-down"></span></button>
				</div>


				<div class="cover__item cover__item--left-bottom">
					<h2 class="cover__heading">{{ $page->name }}</h2>
				</div>

			</div>

			<div class="cover__media" style="background-image: url('{{ $background_url }}');">
				@if( $video_file )
					<video muted loop preload="none" id="inner-video">
						<source type="video/mp4" src="{{ $video_file }}">
					</video>
				@endif
			</div>

    </div>

		<div class="section__line"></div>
  </section>

	<section class="catalog">
		
		@if( count($projects) )
			<div class="catalog__items">
				
				@foreach( $projects as $project )
					<!-- card -->
					@if( App\Models\Project::check_project_honors( $project->images ) )
						<div class="card card--award is-preanimated is-transition-enabled js-waypoint" data-transitionend=".card__aside" >
							<div class="card__aside">
								<div class="card__image">
									@if( count($project->images) )
										@foreach( $project->images as $item )
											@if( $item->type == 'slider' && $item->active == 1 )
												<img src="{{ URL::to('uploads/projects/images/middle/' . $item->image) }}" alt="{{{ !empty( $item->alt ) ? $item->alt : $project->name }}}" @if( $item->title ) title="{{{ $item->title }}}" @endif />
												<?php break; ?>
											@endif
										@endforeach
									@else
										<img src="http://placehold.it/576x384?text=No image" alt="{{{ $project->name }}}" />
									@endif
								</div>
							</div>
							<div class="card__content">
								<header class="card__header">
									<h2 class="card__heading">{{ $project->name }}</h2>
								</header>
								
								@if( $project->annotation )
									<div class="card__text">
										<p>{{ str_limit($project->annotation, 223) }}</p>
									</div>
								@endif

								@if( count($project->images) )
									<div class="card__awards">
										@foreach( $project->images as $item )
											@if( $item->type == 'honors' )
												<a href="{{ URL::to('uploads/projects/images/original/' . $item->image) }}" class="award" data-fancybox="awards1">
													<img src="{{ URL::to('uploads/projects/images/thumbnail/' . $item->image) }}" alt="{{{ !empty( $item->alt ) ? $item->alt : $project->name }}}" @if( $item->title ) title="{{{ $item->title }}}" @endif />
												</a>
											@endif
										@endforeach
									</div>
								@endif

							</div>
						</div>
					@endif
					<!-- //card -->
				@endforeach

			</div>
		@endif

	</section>

	<section class="map">
		
		@if( $points )
			<header class="map__header">
				<h2 class="map__heading">{!! trans('design.places_on_map') !!}</h2>
			</header>

			<div class="map__content">
				<div id="map"></div>
			</div>
		@endif

		@if( $page->body )
			<footer class="map__footer">
				{!! $page->body !!}
			</footer>
		@endif

	</section>
	@include('frontend.partials.footer', ['footerClass' => 'footer--dark'])
@endsection

@section('scripts')
	@parent
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-oDvfPlkf2fc1QAqsFI-zZnpIadN21og"></script>
	@if( $points )
		<script type="text/javascript">
			var agMapData = {!! $points !!};
		</script>
	@endif
@endsection