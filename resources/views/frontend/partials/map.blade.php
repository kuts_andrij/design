<script type="text/javascript">
  var agMapData = [
      {
        location : {
          lat : -34.397,
          lng : 150.644
        },
        title : 'House in Dnipro',
        url : '#'
      },
      {
        location : {
          lat : -35.397,
          lng : 151.644
        },
        title : 'House in Forest',
        url : '#'
      }
    ];
</script>
<section class="map">
  <header class="map__header">
    <h2 class="map__heading"><span>places on the map</span> with the completed projects</h2>
  </header>

  <div class="map__content">
    <div id="map"></div>
  </div>

  <footer class="map__footer">
    <h3>Давайте встретимся</h3>
    <p>Вы можете связаться с нами любым удобным способом</p>
  </footer>
</section>
