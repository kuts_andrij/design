<footer class="footer {{$footerClass or ''}}">
  <div class="footer__left">
    <a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/category/projects-all' ) }}" class="btn btn--dashed-left is-animated">{{ trans('design.all_projects') }}</a>
  </div>

  <div class="footer__center">
    <p>{{ trans('design.footer_slogan') }}</p>
    <img src="{{ URL::to('images/footer-logo.png') }}" width="180" alt="" />
  </div>

  <div class="footer__right">
    <ul class="contact">
      @if( $settings->email )
        <li class="contact__item">
          <a href="mailto:{{ $settings->email }}" class="contact__link"><i class="fi-mail"></i>{{ $settings->email }}</a>
        </li>
      @endif
      @if( $settings->phone )
        <li class="contact__item">
          <a href="tel:{{ str_replace(['(', ')', ' ', '-'], '', $settings->phone) }}" class="contact__link"><i class="fi-phone"></i>{{ $settings->phone }}</a>
        </li>
      @endif
      @if( $settings->address )
        <li class="contact__item">
          <span class="contact__link"><i class="fi-pin"></i>{{ $settings->address }}</span>
        </li>
      @endif
    </ul>

    <?php $socialClass =  isset($socialClass) ? $socialClass : ''; ?>
    @include('frontend.partials.social', ['socialClass' => $socialClass ])
  </div>

  @include('frontend.partials.copyright')
</footer>
