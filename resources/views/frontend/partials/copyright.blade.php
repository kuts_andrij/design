<div class="copyright">
  <p>&copy;Alexandr Grabovskiy {{ date('Y') }}. <br>{{ trans('design.rights_reserved') }}.</p>
  <p class="anim-end">{{ trans('design.copying_prohibited') }}.</p>
</div>
