<div class="social {{$socialClass or ''}} js-s">
  @if( $settings->facebook )
  	<a href="{{ $settings->facebook }}" class="social__item btn btn--square" data-speed="slow"><i class="fi-facebook"></i></a>
  @endif
  @if( $settings->youtube )
  	<a href="{{ $settings->youtube }}" class="social__item btn btn--square" data-speed="medium"><i class="fi-youtube"></i></a>
  @endif
  @if( $settings->instagram )
  	<a href="{{ $settings->instagram }}" class="social__item btn btn--square" data-speed="fast"><i class="fi-instagram"></i></a>
  @endif
</div>


{{--
remove js-s and data-speed on prod
--}}