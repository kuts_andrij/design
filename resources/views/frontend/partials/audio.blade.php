<div class="audio">
  <button class="audio__btn btn btn--square js-audio-btn">
    {{-- App\Helpers::getFile('images/eq.svg') --}}
    <div class="bar-c">
      <div id="bar-1" class="bar"></div>
      <div id="bar-2" class="bar"></div>
      <div id="bar-3" class="bar"></div>
      <div id="bar-4" class="bar"></div>
      <div id="bar-5" class="bar"></div>
      <div id="bar-6" class="bar"></div>
    </div>
  </button>

  <audio class="js-audio" preload="none" data-keepplaying>
    <source src="{{ URL::to('audio/audio.mp3') }}" type="audio/mpeg">
  </audio>
</div>
