<div class="projects">
  <button class="projects__close btn btn--square js-projects-close"><i class="fi-close"></i></button>

  <div class="projects__body">
    <div class="projects__list">
      
      @foreach( $projects_left as $project )
        <a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/project/' . $project->slug ) }}" class="projects__item">
          <span class="projects__title">{{ $project->name }}</span>
          @if( count($project->images) )
            @foreach( $project->images as $pi )
              <img src="{{ URL::to('uploads/projects/images/thumbnail/' . $pi->image) }}" alt="{{{ !empty( $pi->alt ) ? $pi->alt : $project->name }}}" @if( $pi->title ) title="{{{ $pi->title }}}" @endif />
              <?php break; ?>
            @endforeach
          @else
            <img src="http://placehold.it/152x101?text=No image" alt="{{{ $project->name }}}" /> 
          @endif
        </a>
      @endforeach

      <div class="projects__all">
        <a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/category/projects-all' ) }}" class=" btn btn--dashed-left">{{ trans('design.all_projects') }}</a>
      </div>
    </div>
  </div>


  <div class="projects__overlay overlay">
    <div class="overlay__item overlay__item--black"></div>
    <div class="overlay__item overlay__item--white"></div>
    <div class="overlay__item overlay__item--green"></div>
  </div>
</div>
