<div class="lang js-lang">
  @if( count($langs) )
  	@foreach( $langs as $lang )
      <a href="@if( $lang == $locale ){{ LaravelLocalization::getNonLocalizedURL() }}@else{{ LaravelLocalization::getLocalizedURL( $lang ) }}@endif" class="lang__btn btn btn--square @if( $lang == $locale ) is-active @endif">{{ $lang }}</a>
  	@endforeach
  @endif
</div>
