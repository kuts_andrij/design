<div class="menu__open is-invisible">
  <button class="btn btn--square js-menu-btn">
    <span class="menu__icon">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </span>
  </button>

  <div class="scrollup">
    <button class="btn btn--square js-scroll-up is-invisible"><span class="fi-arrow-up"></span></button>
  </div>
</div>

<div class="menu">
  <div class="menu__inner">
    <a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/' ) }}" class="menu__logo">
      <img src="{{ URL::to('images/menu-logo.png') }}" alt="" />
    </a>

    <nav class="menu__nav">
      @if( count($menu) )
        <ul class="menu__list">
          @foreach( $menu as $menu_item )
            @if( $menu_item->slug != 'publications' )
				<li class="menu__item">
				  <a href="{{ App\Models\Menu::itemMenuURL( $menu_item->slug ) }}" class="menu__link {{ App\Models\Menu::itemMenuActive( $menu_item->slug, $page->id ) }}">{{ $menu_item->name }}</a>
				  @if( count($menu_item->children) )
					<ul class="menu__sub">
					  @foreach( $menu_item->children as $child_item )
						<li class="menu__subitem">
						  <a href="{{ App\Models\Menu::itemMenuURL( $child_item->slug ) }}" class="menu__link">{{ $child_item->name }}</a>
						</li>
					  @endforeach
					</ul>
				  @endif
				</li>
			@endif
          @endforeach
        </ul>
      @endif
    </nav>

    <button class="menu__close btn btn--square js-menu-close"><i class="fi-close"></i></button>
  </div>


  <div class="menu__overlay overlay js-menu-close">
    <div class="overlay__item overlay__item--black"></div>
    <div class="overlay__item overlay__item--green"></div>
  </div>
</div>
