@extends('frontend.layout')

@section('main')

	<section class="section section--inner  is-section-preanimated is-transition-enabled"  >

		<div class="cover">
			<div class="cover__items">
				<div class="cover__item cover__item--left-top">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/' ) }}" class="logo-text">
						<img src="{{ URL::to('images/logo-text.png') }}" alt="" />
					</a>
					@include('frontend.partials.audio')

					@include('frontend.partials.lang')
				</div>

				{{--
					<div class="cover__item cover__item--center-bottom">
						<button class="btn btn--square btn--dashed-bottom js-scroll-down"><span class="fi-arrow-down scroll-down"></span></button>
					</div>
				--}}

				@if( $page->annotation )
					<div class="cover__item cover__item--left-bottom">
						<div class="cover__intro">
							<div class="cover__intro__line"></div>
							{!! $page->annotation !!}
						</div>
					</div>
				@endif

			</div>

			<div class="cover__media" style="background-image: url('{{ $page->image }}');">
				@if( $page->video_file )
					<video muted loop preload="none" id="inner-video">
						<source type="video/mp4" src="{{ URL::to('uploads/pages/video/' . $page->video_file) }}" >
					</video>
				@endif
			</div>
    	</div>

		<div class="section__line"></div>
  	</section>

	<section class="about">
		<div class="about__main js-main-anim">
			<div class="about__logo is-preanimated">
				<img src="{{URL::to('images/dashed-logo.svg') }}" alt="">
			</div>
			<div class="about__text">
				{!! $page->body !!}
			</div>
		</div>

		<div class="about__years js-years-anim">
			<div class="about__container">
				<ul class="about__yearlist ">
					<li class="about__year year is-preanimated">
						<div class="year__header">
							<p>{{ $about_page->year_1_number }}</p><span>{{ trans('design.year') }}</span>
						</div>
						<div class="year__text">
							{!! $about_page->year_1_text !!}
						</div>
					</li>

					<li class="about__year year is-preanimated">
						<div class="year__header">
							<p>{{ $about_page->year_2_number }}</p><span>{{ trans('design.year') }}</span>
						</div>
						<div class="year__text">
							{!! $about_page->year_2_text !!}
						</div>
					</li>

					<li class="about__year year is-preanimated">
						<div class="year__header">
							<p>{{ $about_page->year_3_number }}</p><span>{{ trans('design.year') }}</span>
						</div>
						<div class="year__text">
							{!! $about_page->year_3_text !!}
						</div>
					</li>
				</ul>
			</div>
		</div>

		<div class="about__cite js-cite-anim">
			<div class="about__container">
				<div class="cite is-preanimated">
					<div class="cite__main">{!! $about_page->education_dev_title !!}</div>
					<div class="cite__sep"></div>
					<div class="cite__text">
						{!! $about_page->education_dev_text !!}
					</div>
				</div>
			</div>
		</div>

		<div class="about__list  js-list-anim">
			<div class="list">
				<div class="list__header">
					<div class="list__headerline dline dline--right"></div>
					<div class="list__title title-ls">{{ trans('design.philosophy') }}</div>
					<div class="list__headerline dline dline--left"></div>
				</div>
				<div class="list__content">
					<ul class="list__items">
						<li class="list__item is-preanimated">
							<span class="cdot"></span>
							<p>{!! $about_page->philosophy_1_text !!}</p>
						</li>
						<li class="list__item is-preanimated">
							<span class="cdot"></span>
							<p>{!! $about_page->philosophy_2_text !!}</p>
						</li>
						<li class="list__item is-preanimated">
							<span class="cdot"></span>
							<p>{!! $about_page->philosophy_3_text !!}</p>
						</li>
						<li class="list__item is-preanimated is-preanimated-line">
							<span class="cdot"></span>
							<p>{!! $about_page->philosophy_4_text !!}</p>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="about__how">
			<div class="how">
				<div class="how__header">
					<div class="how__title title-ls">{{ trans('design.how_work') }}</div>
					<div class="how__headerline dline dline--left"></div>
				</div>
				<div class="how__items js-how-anim">
					<div class="how__item is-preanimated">
						<div class="card-how">
							<div class="card-how__text">{!! $about_page->how_work_1_text !!}</div>
							<div class="card-how__num">1</div>
							<span class="card-how__dot cdot"></span>
						</div>
					</div>
					<div class="how__item is-preanimated">
						<div class="card-how">
							<div class="card-how__text">{!! $about_page->how_work_2_text !!}</div>
							<div class="card-how__num">2</div>
							<span class="card-how__dot cdot"></span>
						</div>
					</div>
					<div class="how__item is-preanimated">
						<div class="card-how">
							<div class="card-how__text">{!! $about_page->how_work_2_text !!}</div>
							<div class="card-how__num">3</div>
							<span class="card-how__dot cdot"></span>
						</div>
					</div>
					<div class="how__item is-preanimated">
						<div class="card-how">
							<div class="card-how__text">{!! $about_page->how_work_2_text !!}</div>
							<div class="card-how__num">4</div>
							<span class="card-how__dot cdot"></span>
						</div>
					</div>
					<div class="how__line dline dline--bottom is-preanimated"></div>
				</div>
			</div>
		</div>
	</section>

	@include('frontend.partials.footer', ['footerClass' => 'footer--dark'])
@endsection
