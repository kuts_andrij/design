@extends('frontend.layout')

@section('main')

	<section class="section section--inner  is-section-preanimated is-transition-enabled"  >

		<div class="cover">
			<div class="cover__items">
				<div class="cover__item cover__item--left-top">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/' ) }}" class="logo-text">
						<img src="{{ URL::to('images/logo-text.png') }}" alt="" />
					</a>
					@include('frontend.partials.audio')

					@include('frontend.partials.lang')
				</div>

				{{--
					<div class="cover__item cover__item--center-bottom">
						<button class="btn btn--square btn--dashed-bottom js-scroll-down"><span class="fi-arrow-down scroll-down"></span></button>
					</div>
				--}}

				<div class="cover__item cover__item--left-bottom">
					<h2 class="cover__heading">{{ $page->name }}</h2>
				</div>

			</div>

			<div class="cover__media" style="background-image: url('{{ $page->image }}');">
				@if( $page->video_file )
					<video muted loop preload="none" id="inner-video">
						<source type="video/mp4" src="{{ URL::to('uploads/pages/video/' . $page->video_file) }}" >
					</video>
				@endif
			</div>

    	</div>

		<div class="section__line"></div>
  	</section>

	<section class="publications">
		
		@if( $page->body )
			<div class="publications__text">
				{!! $page->body !!}
			</div>
		@endif

		@if( count($publications) )
			<div class="publications__list">
				@foreach( $publications as $publication )
					<a href="{{ URL::to('uploads/publications/original/' . $publication->image) }}" class="publication" data-fancybox="publications">
						<img src="{{ URL::to('uploads/publications/thumbnail/' . $publication->image) }}" alt="{{{ $publication->alt }}}" @if( $publication->title ) title="{{{ $publication->title }}}" @endif />
					</a>
				@endforeach
			</div>
		@endif

	</section>
	@include('frontend.partials.footer', ['footerClass' => 'footer--dark'])
@endsection