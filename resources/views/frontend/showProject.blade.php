@extends('frontend.layout')

@section('main')

	<section class="section section--inner   is-section-preanimated is-transition-enabled"  >

		<div class="cover">
			<div class="cover__items">
				<div class="cover__item cover__item--left-top">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/' ) }}" class="logo-text">
						<img src="{{ URL::to('images/logo-text.png') }}" alt="" />
					</a>
					@include('frontend.partials.audio')

					@include('frontend.partials.lang')
				</div>

				<div class="cover__item cover__item--center-bottom">
					<button class="btn btn--square btn--dashed-bottom  is-animated js-scroll-down"><span class="fi-arrow-down scroll-down"></span></button>
				</div>


				<div class="cover__item cover__item--left-bottom">
					<h2 class="cover__heading">{{ $page->name }}</h2>
				</div>

				<div class="cover__item cover__item--right-bottom">
					<div class="btn-group btn-group--end">
						@if( $prev_project_url = (isset($prev_next_urls[0]) ? $prev_next_urls[0] : '') )
							<a href="{{ $prev_project_url }}" class="btn btn--square"><i class="fi-arrow-left"></i></a>
						@endif
						@if( $next_project_url = (isset($prev_next_urls[1]) ? $prev_next_urls[1] : '') )
							<a href="{{ $next_project_url }}" class="btn btn--square"><i class="fi-arrow-right"></i></a>
						@endif
					</div>
				</div>
			</div>

			<div class="cover__media" style="background-image: url('{{ $background_url }}');">
				@if( $video_file )
					<video muted loop preload="none" id="inner-video">
						<source type="video/mp4" src="{{ $video_file }}">
					</video>
				@endif
			</div>

    	</div>

		<div class="section__line"></div>
  </section>

	<div class="bg-dark">
		<div class="project js-waypoint is-preanimated is-transition-enabled" data-transitionend=".project__text">
			<div class="project__header">
				<div class="project__tags">
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/' ) }}" class="project__tag">{{ trans('design.home') }}</a>
					<a href="{{ LaravelLocalization::getLocalizedURL( $locale, '/category/' . $category->slug ) }}" class="project__tag">{{ $category->name }}</a>
					<span class="project__tag">{{ $page->name }}</span>
				</div>
			</div>
			<div class="project__content">
				<div class="project__image">
					@if( count($page->images) )
						@foreach( $page->images as $item )
							@if( $item->type == 'slider' && $item->active == 1 )
								<img src="{{ URL::to('uploads/projects/images/middle/' . $item->image) }}" alt="{{{ !empty( $item->alt ) ? $item->alt : $page->name }}}" @if( $item->title ) title="{{{ $item->title }}}" @endif />
								<?php break; ?>
							@endif
						@endforeach
					@else
						<img src="http://placehold.it/576x384?text=No image" alt="{{{ $page->name }}}" />
					@endif
				</div>
				<div class="project__desc">
					<div class="project__info">
						@if( $page->object )
							<p>{{ trans('design.an_object') }}: <span class="c-gray">{{ $page->object }}</span></p>
						@endif
						@if( $page->location )
							<p>{{ trans('design.location') }}: <span class="c-gray">{{ $page->location }}</span></p>
						@endif
						@if( $page->space )
							<p>{{ trans('design.space') }}: <span class="c-gray">{{ $page->space }}</span></p>
						@endif
						@if( $page->client )
							<p>{{ trans('design.client') }}: <span class="c-gray">{{ $page->client }}</span></p>
						@endif
						@if( $page->object_year )
							<p>{{ trans('design.object_year') }}: <span class="c-gray">{{ $page->object_year }}</span></p>
						@endif
						@if( $page->photographer )
							<p>{{ trans('design.photographer') }}: <span class="c-gray">{{ $page->photographer }}</span></p>
						@endif
					</div>
					@if( $page->annotation )
						<div class="project__text">
							<p>
								{{ $page->annotation }}
							</p>
						</div>
					@endif
				</div>
			</div>
		</div>

		@if( App\Models\Project::check_project_honors( $page->images ) )
			<div class="rewards-section js-waypoint is-preanimated is-transition-enabled" data-offset="70%">
				<h3 class="rewards-section__heading">{{ trans('design.rewards') }}</h3>

				<div class="rewards-section__content rewards-short">
					<div class="rewards-short__text">
						<h5 class="rewards-short__title">{{ trans('design.rewards') }}:</h5>
						@foreach( $page->images as $item )
							@if( $item->type == 'honors' && $item->caption )
								<p>{{ $item->caption }}</p>
							@endif
						@endforeach
					</div>

					@foreach( $page->images as $item )
						@if( $item->type == 'honors' )
							@if( $item->url )
								<a href="{!! $item->url !!}" target="_blank" class="rewards-short__image">
							@else
								<div class="rewards-short__image">
							@endif
								<img src="{{ URL::to('uploads/projects/images/thumbnail/' . $item->image) }}" alt="{{{ !empty( $item->alt ) ? $item->alt : $item->caption }}}" @if( $item->title ) title="{{{ $item->title }}}" @endif />
							@if( $item->url )
								</a>
							@else
								</div>
							@endif
						@endif
					@endforeach
					
				</div>
			</div>
		@endif

		<div class="photo-slider">
			<h3 class="photo-slider__heading">{{ trans('design.about_project') }}</h3>

			@if( $page->body_slider )
				<div class="photo-slider__text">
					{!! $page->body_slider !!}
				</div>
			@endif

			@if( count($page->images) )
				<div class="photo-slider__body">
					<div class="photo-slider__slider js-ps">
						@foreach( $page->images as $item )
							@if( $item->type == 'slider' )
								<div data-caption="{{ $item->caption }}">
									<div class="photo-slider__img">
										<div>
											<img src="{{ URL::to('uploads/projects/images/original/' . $item->image) }}" alt="{{{ !empty( $item->alt ) ? $item->alt : '' }}}" @if( $item->title ) title="{{{ $item->title }}}" @endif />
										</div>
									</div>
								</div>
							@endif
						@endforeach
					</div>
					<a href="#" class="photo-slider__nav photo-slider__nav--prev btn btn--square js-ps-prev"><i class="fi-arrow-left"></i></a>
					<a href="#" class="photo-slider__nav photo-slider__nav--next btn btn--square js-ps-next"><i class="fi-arrow-right"></i></a>
					{{--
					<div class="photo-slider__footer">
						<a href="#" class="btn btn--square js-ps-prev"><i class="fi-arrow-left"></i></a>
						<div class="photo-slider__desc">
							<div class="photo-slider__counter js-ps-status"></div>
							<div class="photo-slider__caption js-ps-caption"></div>
						</div>
						<a href="#" class="btn btn--square js-ps-next"><i class="fi-arrow-right"></i></a>
					</div>
					--}}
				</div>
			@endif

		</div>

		<div class="masonry">

			@if( $page->body_gallery )
				<div class="masonry__text">
					{!! $page->body_gallery !!}
				</div>
			@endif

			@if( count($page->images) )
				<div class="masonry__body js-waypoint is-preanimated is-transition-enabled" data-transitionend=".masonry__item:last-child">
					@foreach( $page->images as $item )
						@if( $item->type == 'gallery' )
							<div class="masonry__item">
								<a href="{{ URL::to('uploads/projects/images/original/' . $item->image) }}" class="masonry__link" data-fancybox="masonry">
									<img src="{{ URL::to('uploads/projects/images/middle/' . $item->image) }}" alt="{{{ !empty( $item->alt ) ? $item->alt : '' }}}" @if( $item->title ) title="{{{ $item->title }}}" @endif />
								</a>
							</div>
						@endif
					@endforeach
				</div>
			@endif
		</div>

		@if( $page->video_youtube )
			<div class="yt">
				<iframe src="https://www.youtube.com/embed/{!! App\Models\Project::video_code( $page->video_youtube ) !!}" frameborder="0" allowfullscreen></iframe>
			</div>
		@endif

		@if( $page->body_video )
			<div class="text">
				{!! $page->body_video !!}
			</div>
		@endif

	</div>

	@include('frontend.partials.footer', ['footerClass' => 'footer--dark'])
@endsection
