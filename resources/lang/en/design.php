<?php

return [
    
    // Main
    'projects'           => 'Projects',
    'about_me'           => 'About me',
    'publications'       => 'Publications',
    'rewards'            => 'Rewards',
    'all_rewards'        => 'All rewards',
    'completed_projects' => 'Completed<br>projects',
    'interior'           => 'Interior',
    'architecture'       => 'Architecture',
    'product_design'     => 'Product design',
    'photo_gallery'      => 'Photo gallery',
    'from_sketch'        => 'From sketch',
    'to_completion'      => 'To completion',
    'video'              => 'Video',
    'articles'           => 'Articles',
    'video_diary'        => 'Video diary',
    'interviews'         => 'Interviews',
    'contacts'           => 'Contacts',
	'main_footer_text_1' => 'Let&#39;s meet!',
	'main_footer_text_2' => 'It&#39;s easier than imagined before',

    // Categories
    'see_more'           => 'See more...',
    'places_on_map'      => '<span>Places on the map</span> with the completed projects',
    'filter_apartment'   => 'Apartment',
    'filter_house'       => 'House',
    'filter_office'      => 'Office',
    'filter_all'         => 'All',

    // Project
    'home'               => 'Home',
    'an_object'          => 'An Object',
    'location'           => 'Location',
    'space'              => 'Space',
    'client'             => 'Client',
	'object_year'		 => 'Year of project implementation',
	'photographer'		 => 'Photographer',
    'about_project'      => 'About project',

    // About page
    'year'               => 'Year',
    'philosophy'         => 'Philosophy',
    'how_work'           => 'How we are working',

    // Contacts
    'contact_us'         => 'Contact us',
    'social_media'       => 'Social media',
    'input_name'         => 'Name',
    'input_phone'        => 'Phone number',
    'input_email'        => 'E-mail',
    'input_comment'      => 'Type your message',
    'input_send'         => 'Send',
    'message_thank_you'  => 'Thank you!',
    'message_error'      => 'Error!',
    'message_sys_error'  => 'System error!',
    'message_success'    => 'Your message has been sent. Wait, you will be contacted',

    // Footer
    'all_projects'       => 'All projects',
	'rights_reserved'	 => 'All rights reserved',
	'copying_prohibited' => 'Copying of materials is prohibited',
    'footer_slogan'      => 'Create comfort in your home is easy! Call us!'
];
