<?php

return [
	
	// Main
	'projects'     		 => 'Проекты',
	'about_me'     		 => 'Обо мне',
	'publications' 		 => 'Публикации',
	'rewards'      		 => 'Награды',
	'all_rewards'        => 'Все награды',
	'completed_projects' => 'Завершенные<br>проекты',
	'interior'			 => 'Интерьер',
	'architecture'       => 'Архитектура',
	'product_design'     => 'Дизайн продукта',
	'photo_gallery'      => 'Фотогалерея',
	'from_sketch'        => 'От эскиза',
    'to_completion'      => 'До завершения',
    'video'				 => 'Видео',
    'articles'           => 'Статьи',
    'video_diary'        => 'Видео-дневник',
    'interviews'		 => 'Интервью',
    'contacts'           => 'Контакты',
	'main_footer_text_1' => 'Давайте встретимся!',
	'main_footer_text_2' => 'Это проще, чем представлялось раньше',

    // Categories
    'see_more'           => 'Узнать больше...',
    'places_on_map'      => '<span>Места на карте</span> с выполненными проектами',
    'filter_apartment'   => 'Апартаменты',
    'filter_house'       => 'Дома',
    'filter_office'      => 'Офисы',
    'filter_all'         => 'Все',

    // Project
    'home'               => 'Главная',
    'an_object'          => 'Объект',
    'location'           => 'Место нахождения',
    'space'              => 'Пространство',
    'client'             => 'Клиент',
	'object_year'		 => 'Год реализации проекта',
	'photographer'		 => 'Фотограф',
    'about_project'      => 'О проекте',

    // About page
    'year'               => 'Год',
    'philosophy'         => 'Философия',
    'how_work'           => 'Как мы работаем',

    // Contacts
    'contact_us'		 => 'Свяжитесь с нами',
    'social_media'       => 'Социальные сети',
    'input_name'         => 'Имя',
    'input_phone'        => 'Номер телефона',
    'input_email'        => 'E-mail',
    'input_comment'      => 'Введите текст сообщения',
    'input_send'         => 'Отправить',
    'message_thank_you'  => 'Спасибо!',
    'message_error'      => 'Ошибка!',
    'message_sys_error'  => 'Системная ошибка!',
    'message_success'    => 'Ваше сообщение было отправлено. Ожидайте, с Вами свяжутся',

	// Footer
    'all_projects' 		 => 'Все проекты',
	'rights_reserved'	 => 'Все права защищены',
	'copying_prohibited' => 'Копирование материалов запрещено',
    'footer_slogan'      => 'Создать уют в своем доме легко! Звоните!'
];
