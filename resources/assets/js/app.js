window.$ = window.jQuery = require('jquery');
var isMobile = require('ismobilejs'),
    waypoint = require('waypoints/lib/noframework.waypoints.js'),
    slick = require('slick-carousel');



//common
require('./common/lang');
require('./common/youtube');
require('./common/l-by-l.min');
require('fancybox/dist/jquery.fancybox');
require('./common/menu');
require('./common/scrollup');
import { initMap, initContactsMap } from './common/maps';
import audio from './common/audio';
import onEnd from './common/onend';
import { TimelineLite } from 'gsap';

if (isMobile.any){
  $('body').addClass('is-mobile');
} else {
  audio.init();
  $('body').addClass('is-desktop');
}



let $sectionInner = $('.section--inner');

if ($sectionInner.length) {
  // main section transition end
  onEnd.transition({
    block : '.section__line',
    callback : function() {
      $sectionInner.removeClass('is-transition-enabled');
    }
  });

  $(document).ready(function() {
    $sectionInner.removeClass('is-section-preanimated');
    $('.menu__open').removeClass('is-invisible');

    initPhotoslider();
    initVideo();
    initMap();
    initContactsMap();
    initFancyGallery();
    initScrollDown();
    initContactForm();
    initAboutAnimations();

  });
}


$(document).ready(function() {
  initWaypoints();
  initPublications();
});

  var t;
  $(window).resize(function() {
    clearTimeout(t);
    $('body').addClass('notransition');

    t = setTimeout(function() {
      $('body').removeClass('notransition');
    },200);
  });

// about anim
function initAboutAnimations(){
  function mainAnim() {
    let w = new Waypoint({
      element: document.querySelector('.js-main-anim'),
      handler: function(direction) {
        if (direction == 'down') {
          $('.about__logo').removeClass("is-preanimated");
          let htl = new TimelineLite().staggerTo($('.about__text p'), .61, {className: "+=is-animated"}, .61);
        } else if (direction == 'up') {
          $('.about__logo').addClass("is-preanimated");
          $('.about__text p').removeClass("is-animated");
        }
      },
      offset: '50%'
    });
  }

  function yearsAnim() {
    let w = new Waypoint({
      element: document.querySelector('.js-years-anim'),
      handler: function(direction) {
        if (direction == 'down') {
          $('.about__year').removeClass("is-preanimated")
        } else if (direction == 'up') {
          $('.about__year').addClass("is-preanimated")
        }
      },
      offset: '50%'
    });
  }

  function citeAnim() {
    let w = new Waypoint({
      element: document.querySelector('.js-cite-anim'),
      handler: function(direction) {
        if (direction == 'down') {
          $('.cite').removeClass("is-preanimated")
        } else {
          $('.cite').addClass("is-preanimated")
        }
      },
      offset: '50%'
    });
  }

  function listAnim() {
    let w = new Waypoint({
      element: document.querySelector('.js-list-anim'),
      handler: function(direction) {

          if (direction == 'down') {
            let htl = new TimelineLite()
            .to($('.list__item:last-child'), 1, {className: "-=is-preanimated-line"})
            .staggerTo($('.list__item'), .61, {className: "-=is-preanimated"}, .61);
          } else {
            $('.list__item:last-child').addClass('is-preanimated-line');
            $('.list__item').addClass('is-preanimated')
          }
      },
      offset: '50%'
    });
  }

  function howAnim() {
    let w = new Waypoint({
      element: document.querySelector('.js-how-anim'),
      handler: function(direction) {
        if (direction == 'down') {
          let htl = new TimelineLite()
            .to($('.how__line'), 1, {className: "-=is-preanimated"})
            .staggerTo($('.how__item'), 1, {delay: .5, className: "-=is-preanimated"}, 1);
        } else {
          $('.how__line').addClass('is-preanimated')
          $('.how__item').addClass('is-preanimated')
        }
      },
      offset: '50%'
    });
  }

  function init() {
    mainAnim();
    yearsAnim();
    citeAnim();
    listAnim();
    howAnim();

  }

  if ($('.about').length) {
    init();
  }
}


function initAboutTextAnim() {
  var $at = $('.about__text');
  var $aboutP = $('p', $at).first();
  var flag = true;

  if ($at.length) {
    $at.css({height : $at.outerHeight() })

    let w = new Waypoint({
      element: document.querySelector('.about__text'),
      handler: function(direction) {
        if (direction == 'down') {
          flag = true;
          initAbout($aboutP);
        } else {
          flag = false;
          $('p.is-visible', $at).removeClass('is-visible').text($(this).data('text'))
        }
      },
      offset: '50%'
    });
  }


 function initAbout(p) {
   if (p.length) {
     p.addClass('is-visible');

     if (!p.data('text')) {
       p.data('text' , p.text())
     }

     p.lbyl({
       content: p.text(),
       finished : function() {
         if (flag) {
           initAbout(p.next());
         }
       }
     });
   } else {
     $at.removeAttr('style');
   }
 }

}

// publication anim
function initPublications() {
  var $p = $('.publication');

  if ($p.length) {

    let w = new Waypoint({
      element: document.querySelector('.publications__list'),
      handler: function(direction) {
        let tl = new TimelineLite();
        tl.staggerTo($p , .7 , {scale : 1 } , .4)

        this.destroy();
      },
      offset: '50%'
    });


  }
}

// scroll down
function initScrollDown() {
  $('.js-scroll-down').click((e) => {
    e.preventDefault();
    $('html, body').animate({scrollTop: $('.js-scroll-down').closest('.section').next().offset().top}, 1000)
  });
}

function initPhotoslider() {
  var $ps = $('.js-ps'),
      $psPrev = $('.js-ps-prev'),
      $psNext = $('.js-ps-next'),
      $caption = $('.js-ps-caption'),
      $status = $('.js-ps-status');

  if ($ps) {
    // $ps.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    //   var i = (currentSlide ? currentSlide : 0) + 1;
    //   var c = currentSlide ? currentSlide : 0;
    //   var caption = $("[data-slick-index='" +c+ "']").data('caption');
    //
    //   $status.text(i + '/' + slick.slideCount);
    //   $caption.text(caption);
    // });

    $ps.slick({
      prevArrow : $psPrev,
      nextArrow : $psNext,
      infinite: false
    });

  }
}

// init catalog video
function initVideo() {
  var video =   document.getElementById('inner-video');
  if ((!isMobile.any || $(window).width() >= 768) && video)  {
    video.play();
  }
}

// photo gallery
function initFancyGallery() {
  $("[data-fancybox]").fancybox({
		// Options will go here
	});

  $('.js-fancy-gallery').click(function(e) {
    e.preventDefault();
    // must be changed to ajax request
    let data = $(this).data('gallery');

    $.fancybox.open( data, {
      loop : false
    });
  });

  $('.js-more-photos').on('click', function(e) {
    e.preventDefault();

    $(this).closest('.card').find('.js-fancy-gallery').click();
  });
}



//  intro animation
function initWaypoints() {
  var items = document.querySelectorAll('.js-waypoint');

  for (var x= 0, max = items.length; x < max; x++) {
    let w = new Waypoint({
      element: items[x],
      handler: function(direction) {
        var $item = $(this.element);

        if (direction === 'down') {
          $item.removeClass('is-preanimated');
        } else if (direction === 'up') {
          $item.addClass('is-preanimated');
        }
      },
      offset: items[x].getAttribute('data-offset') || '50%'
    });
  }
}

// contact form
function initContactForm() {
  var $open = $('.js-cf-open'),
      $close = $('.js-cf-close'),
      $form = $('.js-cf');

  $open.click(function() {
    $form.stop().slideDown(800);
  });

  $close.click(function(e) {
    e.preventDefault();
    $form.stop().slideUp(800);
  })
}
