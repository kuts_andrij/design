var $ = require('jquery');
var isMobile = require('ismobilejs');

let audioEl = $('.js-audio')[0],
    audioHover = $('.js-audio-hover')[0],
    $audioBtn = $('.js-audio-btn');

let audio = {
  on : function() {
    localStorage.setItem('audio', 'on');
    $audioBtn.addClass('is-played');
    audio.play();
  },
  off: function(){
    localStorage.setItem('audio', 'off');
    $audioBtn.removeClass('is-played');
    audio.stop();
  },
  play: function(){
    audioEl.play();
  },
  stop: function(){
    audioEl.pause();
  },
  paused : function() {
    return audioEl.paused;
  },
  init: function() {
    if (!isMobile.any) {
      $(document).on('mouseenter', '.btn', function() {
        audioHover.pause();
        audioHover.play();
      });

      //init click function
      $audioBtn.click(function() {
        if ( $audioBtn.is('.is-played') ) {
          audio.off();
        } else {
          audio.on();
        }
      });

      // check on load
      if (localStorage.getItem('audio') == 'on') {
        audio.on();
      }
    }
  }
}



export default audio;
