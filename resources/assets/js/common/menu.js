var $ = require('jquery');


let $body = $('body'),
    $menuBtn = $('.js-menu-btn'),
    $menuOpen = $('.js-menu-open'),
    $menuClose = $('.js-menu-close');


let menu = {
  open : function() {
    $body.addClass('is-menu-open');
  },
  close : function() {
    $body.removeClass('is-menu-open');
  },
  init : function() {
    $menuOpen.click(menu.open);
    $menuClose.click(menu.close);

    $menuBtn.click(function() {
      if ($body.is('.is-menu-open')) {
        menu.close();
      } else {
        menu.open();
      }
    })

    $('.menu__link').click(function(e) {
      var $submenu = $(this).next('.menu__sub');

      if ($submenu.length && $(window).width() < 992) {
        e.preventDefault();
        $submenu.slideToggle();
      }
    });

    $body.keydown(function(e) {
      if ($body.is('.is-menu-open') && e.which == 27) menu.close();
    });
  }
}

menu.init();
