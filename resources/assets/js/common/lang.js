var $ = require('jquery');

var $lang = $('.js-lang'),
    $langBtn = $lang.find('.btn');

$langBtn.on('click', function(e) {
  if ( $(this).is('.is-active') ) {
    e.preventDefault();
    $lang.toggleClass('is-lang-active')
  }
});

$('body').click(function(e) {
  if (!$(e.target).closest('.js-lang').length) {
    $lang.removeClass('is-lang-active');
  }
})
