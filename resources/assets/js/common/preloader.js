var $ = require('jquery');
var isMobile = require('ismobilejs');
// import { TimelineLite } from 'gsap';


let $body = $('body');

let preloader = {
  animation : new TimelineLite({
      paused : true,
      onComplete : function() {
        setTimeout(function(){
          preloader.hide();
          preloader.callback();
        }, 600);
      }
    }).to($('#dltrect-top'), .3 , {strokeDashoffset : 0, delay: .5, ease : Power0.easeOut})
    .to($('#dltrect-bottom'), .3 , {strokeDashoffset : 0,   ease : Power0.easeOut} )
    .to($('#dltrect-left'), .3 , {strokeDashoffset : 0,   ease : Power0.easeOut} )
    .to($('#dltrect-right'), .3 , {strokeDashoffset : 0,   ease : Power0.easeOut} )
    .to($('#dltdline'), .3 , {strokeDashoffset : 0,   ease : Power0.easeOut} )
    .to($('#dltvline'), .3 , {strokeDashoffset : 0,   ease : Power0.easeOut} )
    .to($('#dltcircle'), .7 , {strokeDashoffset : 0,   ease : Power0.easeOut} )

    .to($('#dlrect'), 1 , {strokeDashoffset : 0,  ease : Power0.easeNone})
    .to($('#dlg'), 1 , {strokeDashoffset : 0,  ease : Power0.easeOut})
    .to($('#dldsline'), 1 , {strokeDashoffset : 0,  ease : Power0.easeOut})
    .to($('#dlvline'), 1 , {strokeDashoffset : 0,  ease : Power0.easeOut} , "-=1")
    .to($('#dldline'), 1 , {strokeDashoffset : 0,  ease : Power0.easeOut} , "-=1"),
    // .to($('.preloader svg'), .6 , {autoAlpha : 0,  ease : Power0.easeOut} ),
  show : function() {
    $body.addClass('is-preloader-visible');
    preloader.animation.play();
  },
  hide : function() {
    $body.removeClass('is-preloader-visible');
  },
  init : function(callback) {
    // $body.addClass('is-preloader-visible');
    preloader.callback = callback;
    preloader.show();
  }
}

module.exports = preloader;
