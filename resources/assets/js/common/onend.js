var $ = require('jquery');

let onEnd = {
  animation : function({block, callback, delay = 0 }) {
    $(block).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
      setTimeout(() => {
        callback();
      }, delay);
    });
  },
  transition : function({block, callback, delay = 0, prop}) {
    $(block).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
    function(e){
      // console.log(e.originalEvent.propertyName);
      setTimeout(() => {
        callback();
      }, delay);
    });
  }
}

module.exports = onEnd;
