
// map
function initMap() {
  var mapEl = document.getElementById('map');

  if (mapEl) {
    if (typeof(google) != "undefined") {
      var InfoBox = require('./lib.infobox');
      var defaultData = [
          {
            location : {
              lat: 49.263856,
              lng: 31.644370
            },
            title : 'House in Kiev',
            url : '#'
          }
        ];
      var data = typeof(agMapData) == "undefined" ? defaultData : agMapData;

      var centerCoords = {lat: 51.707688,  lng: 31.253213};

      var markers = [];
      var ibs = [];

      var map = new google.maps.Map(mapEl, {
        center: centerCoords,
        zoom: 5,
        scrollwheel : false
      });


      var closeInfoBox = function() {
          for (var i in ibs) {
              ibs[i].close();
          }
      }


      // add places
      for (var i = 0 , max = data.length; i < max; i++) {
        var item = data[i];

          ibs[i] = new InfoBox({
            content: `<div class="infobox"><a href="${item.url}">${item.title}</a></div>`,
            disableAutoPan: false,
            maxWidth: 0,
            zIndex: null,
            closeBoxMargin: "0",
            alignBottom : true,
            closeBoxURL: "",
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            pane: "floatPane",
            enableEventPropagation: false
        });

          markers[i] = new google.maps.Marker({
            position: new google.maps.LatLng(item.location.lat, item.location.lng),
            map: map,
            flat: true,
            title: item.title,
            draggable: false,
            icon : {
              url: '/images/map-pin.png',
              size: new google.maps.Size(32, 32),
              origin: new google.maps.Point(0, 0),
            }
          });

        google.maps.event.addListener(markers[i], 'click', function(ii) {
          return function() {
            closeInfoBox();
            ibs[ii].open(map, markers[ii]);
          }
         }(i));

        var t;
        google.maps.event.addDomListener(window, 'resize', function() {
          clearTimeout(t);

          t = setTimeout(function() {
            map.panTo(centerCoords);
          },10);
        });
      }
    } else {
      var mapParent = document.querySelector('.map');
      mapParent.style.display = "none";
    }
  }
}

// contacts map
function initContactsMap() {
  var mapEl = document.getElementById('cmap');

  if (mapEl) {
    if (typeof(google) != "undefined") {
      var InfoBox = require('./lib.infobox');
      var lat = +mapEl.getAttribute('data-lat');
      var lng = +mapEl.getAttribute('data-lng');
      var content = mapEl.getAttribute('data-title');

      var map = new google.maps.Map(mapEl, {
        center: {lat , lng},
        zoom: 17,
        scrollwheel : false
      });

      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        flat: true,
        draggable: false,
        icon : {
          url: '/images/map-pin-green.png',
          size: new google.maps.Size(32, 32),
          origin: new google.maps.Point(0, 0),
        }
      });

      var ib = new InfoBox({
        content: `<div class="infobox"><div class="infobox__content">${content}</div></div>`,
        disableAutoPan: false,
        maxWidth: 250,
        zIndex: null,
        closeBoxMargin: "0",
        alignBottom : true,
        closeBoxURL: "",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: false,
        pane: "floatPane",
        enableEventPropagation: false
    });

      ib.open(map, marker);

      var t;
       google.maps.event.addDomListener(window, 'resize', function() {
         clearTimeout(t);

         t = setTimeout(function() {
           map.panTo({lat , lng});
         },10);
       });
    } else {
      var mapParent = document.querySelector('.contacts__map');
      mapParent.style.display = "none";
    }
  }
}

export {initMap, initContactsMap};
