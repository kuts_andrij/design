var $ = require('jquery');
import onEnd from './onend';
import audio from './audio';

var $trigger = $('.js-youtube'),
    $overlay = $('.youtube__overlay'),
    $close = $('.youtube__close'),
    $iframe = $('.youtube iframe'),
    $body = $('body');

function openYt() {
  var videoSrc = "https://www.youtube.com/embed/" + $(this).data('id') + "?autoplay=1&autohide=1&vq=hd720";
  $iframe.attr('src' , videoSrc);

  setTimeout(() => {
    if (!audio.paused()) {
      audio.stop();
    }
    $body.addClass('is-youtube-open');
  }, 250);
}

function closeYt() {
  setTimeout(() => {
    $iframe.attr('src' , '');

    if (localStorage.getItem('audio') == 'on') {
      audio.play();
    }
  }, 400);
  $body.removeClass('is-youtube-open');
}


$trigger.click(openYt);
$overlay.click(closeYt);
$close.click(closeYt);
