var $ = require('jquery');
var isMobile = require('ismobilejs');
var $scrollUp = $('.js-scroll-up');

if (!$('.js-fp').length || isMobile.any) {
  $(window).scroll(function(){
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
      $scrollUp.removeClass('is-invisible')
    } else {
      $scrollUp.addClass('is-invisible')
    }
  })


  $scrollUp.click(function(e) {
    $('html, body').animate({scrollTop: 0}, 1000, function() {
      $scrollUp.addClass('is-invisible')
    });
  });
}
