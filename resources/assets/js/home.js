window.$ = window.jQuery = require('jquery');
var  isMobile = require('ismobilejs');
import preloader from './common/preloader';
import onEnd from './common/onend';
import fp from './main/fullpage.js';
import vSlider from './main/video-slider';
require('./main/projects');

vSlider.init()

let minWidth = 768;


function initHomepage() {
  fp.init();

  if ($(window).width() >= minWidth)  {
    onEnd.transition({
      block : '.menu__open',
      callback : function() {
          vSlider.play();
      }
    });
  }

  $('.menu__open').removeClass('is-invisible')
}



if (sessionStorage.getItem('preloader') === 'off') {
  initHomepage();
} else {
  preloader.init(function() {
    initHomepage();
    sessionStorage.setItem('preloader', 'off');
  });
}
