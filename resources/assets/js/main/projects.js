window.$ = window.jQuery = require('jquery');
import onEnd from '../common/onend';


var $body = $('body'),
    $projectsOpen = $('.js-projects-open'),
    $projectsClose = $('.projects__close'),
    $item = $('.projects__item'),
    $overlay = $('.projects__overlay'),
    $overlayWhite = $overlay.find('.overlay__item--white');


onEnd.transition({
  block : $overlayWhite,
  callback: function() {
    if (!$body.is('.is-projects-open')) {
      $body.removeClass('is-projects-transition');
    }
  }
});


var projects = {
  open : function() {
    $body.addClass('is-projects-transition is-projects-open');
  },
  close : function() {
    $body.removeClass('is-projects-open');
  },
  openProject : function(href) {
    projects.close();

    setTimeout(() => {
      location.href = href;
    },500);
  },
  init : function() {
    $projectsOpen.click(projects.open);
    $projectsClose.click(projects.close);

    $overlay.click(projects.close);

    $body.keydown(function(e) {
      if ($body.is('.is-projects-open') && e.which == 27) projects.close();
    });

    $item.click(function(e) {
      e.preventDefault();
      projects.openProject(this.getAttribute('href'));
    });
  }
}

projects.init();

// module.exports = projects;
