window.$ = window.jQuery = require('jquery');
var slick = require('slick-carousel');

import onEnd from '../common/onend';

let $slider = $('.js-video-slider'),
    $prev =  $('.js-video-slider-prev'),
    $next =  $('.js-video-slider-next'),
    $overlay,
    $link = $('.js-video-slider-link'),
    video = $slider.find('video')[0];
let minWidth = 768;

let vSlider = {
  play: function(){
    setTimeout(function() {
        video.play();
        video.setAttribute("data-autoplay", "");
    }, 200);
  },
  pause: function() {
    video.pause();
    video.removeAttribute("data-autoplay");
  },
  overlayAnim : function(direction , callback ) {
    $overlay = $('.slick-active .slider__overlay');

    $overlay.addClass(direction);
    setTimeout(()=> {
      $overlay.addClass('is-animated');
    },100);

    onEnd.transition({
      block : $overlay.find('.overlay__item--green'),
      callback
    });
  },
  updateLink : function(slideIndex) {
    let $slide = $slider.find(`[data-slick-index="${slideIndex}"]`),
        text = $slide.data('text'),
        url = $slide.data('url');

    $link.text(text).attr('href', url);
  },
  prev : function() {
    vSlider.overlayAnim('prev', () => {$slider.slick('slickPrev')} );
  },
  next : function() {
    vSlider.overlayAnim('next', () => {$slider.slick('slickNext')} );
  },
  init : function(){
    // init slider
    $slider.slick({
      arrows: false,
      draggable : false,
      swipe : false,
      speed : 1000,
      cssEase : 'ease-out'
    });

    $prev.click(vSlider.prev)
    $next.click(vSlider.next)

    // before slide change
    $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
      if ($(window).width() >= minWidth)  {
        vSlider.pause();
      }

      $link.addClass('is-invisible');
      vSlider.updateLink(nextSlide);
    });

    //after slide change
    $slider.on('afterChange', function(event, slick, currentSlide){
      if ($(window).width() >= minWidth)  {
        video = $slider.find('.slick-active video')[0];
        vSlider.play();
      }

      $overlay.removeClass('is-animated prev next');
      $link.removeClass('is-invisible');
    });

    $link.click(function(e) {
      e.preventDefault();
      let href = this.getAttribute('href');

      vSlider.overlayAnim('next',() => { location.href = href; });
    });

  }
}

export default vSlider;
