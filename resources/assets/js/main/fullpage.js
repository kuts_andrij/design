window.$ = window.jQuery = require('jquery');
let fullpage = require('fullpage.js/dist/jquery.fullpage.min');
let isMobile = require('ismobilejs');
let waypoint = require('waypoints/lib/noframework.waypoints.js');
import onEnd from '../common/onend';

let scrollAllowed = false,
    $scrollDown = $('.js-fp-down'),
    $scrollUp = $('.js-scroll-up'),
    $overlay = $('.section__overlay').eq(0),
    $body = $('body');

let aboutSectionTL = new TimelineLite({paused : true}).staggerTo($('.section--about blockquote p'), 1, {className: "+=is-visible"}, 1);

let fp = {
  initScrollDown : function() {
    $scrollDown.click(function(e) {
      if (isMobile.any) {
        e.preventDefault();
        $('html, body').animate({scrollTop: $(this).closest('.section').next().offset().top}, 1000)
      } else {
        scrollAllowed = true;
        $.fn.fullpage.moveSectionDown();
      }
    });
  },
  initScrollUp : function() {
    $scrollUp.click(function(e) {
      if (!isMobile.any) {
        scrollAllowed = true;
        $.fn.fullpage.moveTo(1);
        $scrollUp.addClass('is-invisible')
      }
    });
  },
  initFp : function() {
    if (isMobile.any) {
      var sections = document.querySelectorAll('.section');

      for (var x= 0, max = sections.length; x < max; x++) {
        let w = new Waypoint({
          element: sections[x],
          handler: function(direction) {
            var $section = $(this.element);

            onEnd.transition({
              block : $($section.data('transitionend')),
              callback : function() {
                $section.removeClass('is-transition-enabled');
              }
            });

            $section.removeClass('is-section-preanimated');
            this.destroy();
          },
          offset: '50%'
        });
      }
    } else {
      $('.js-fp').fullpage({
        responsiveWidth: 1024,
        responsiveHeight: 600,
        verticalCentered: false,
        scrollingSpeed : 1000,
        // css3: false, // for fixing disapering of elements on home page cover section
        onLeave: function(index, nextIndex, direction) {

          if ($body.is('.is-projects-open') || $body.is('.is-menu-open')) {
            return false;
          } else {
            if ( $(window).width() <= 1024 || $(window).height() < 600) {
              scrollAllowed = true;
            }

            if (!scrollAllowed) {
              let $section = $('.section').eq(index-1);
              $overlay = $('.section__overlay').eq(index-1);

              $overlay.addClass(direction);
              setTimeout(()=> {
                $overlay.addClass('is-animated');
              },10);

              onEnd.transition({
                block : $overlay.find('.overlay__item--green'),
                callback: function() {
                  scrollAllowed = true;
                  $.fn.fullpage.moveTo(nextIndex);
                  $section.addClass('is-section-preanimated is-transition-enabled');

                  if ($section.is('.section--about')) {
                    aboutSectionTL.reverse();
                  }

                  setTimeout(() => {
                    $section.addClass('notransition')
                  },400);
                  setTimeout(() => {
                    $section.removeClass('notransition')
                  },500);
                }
              });
            }
          }

          return scrollAllowed ;
        },
        afterLoad : function(a,i) {
          let $section = $('.section').eq(i-1);
          scrollAllowed = false;

          $overlay.removeClass('is-animated up down');
          $section.removeClass('is-section-preanimated');

          if ($section.is('.is-transition-enabled')) {
            onEnd.transition({
              block     : $section.data('transitionend'),
              callback  : function() {
                            $section.removeClass('is-transition-enabled')
                          }
            });
          }


          if ($section.is('.section--contacts')) {
            $scrollUp.removeClass('is-invisible');
          } else {
            $scrollUp.addClass('is-invisible');
          }

          if ($section.is('.section--about')) {
            setTimeout(function() {
              aboutSectionTL.play();
            }, 2500)
          }
        }
      });
    }

  },
  init : function() {
    fp.initFp();
    fp.initScrollDown();
    fp.initScrollUp();
  }
}



export default fp;
