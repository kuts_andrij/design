.menu {
  position: fixed;
  z-index: 5;
  bottom: 100%;
  left: 0;
  width: 100%;

  &__icon {
    position: relative;
    display: block;
    margin: 0 auto;
    width: 25px;
    height: 25px;

    span  {
      display: block;
      position: absolute;
      height: 1px;
      width: 100%;
      background: #fff;
      left: 0;
      transform: rotate(0deg);
      transition: .25s ease-in-out;

      &:nth-child(1) {
        top: 4px;
      }

      &:nth-child(2),
      &:nth-child(3) {
        top: 12px;
      }

      &:nth-child(4) {
        top: 20px;
      }

      .is-menu-open & {
        &:nth-child(1) {
          top: 12px;
          width: 0%;
          left: 50%;
        }

        &:nth-child(2) {
          transform: rotate(45deg);
        }

        &:nth-child(3) {
          transform: rotate(-45deg);
        }

        &:nth-child(4) {
          top: 12px;
          width: 0%;
          left: 50%;
        }
      }
    }
  }

  &__open {
    position: fixed;
    z-index: 6;
    right: 15px;
    top:20px;


    &.is-invisible {
      transform: translateX(200%);
    }

    @include media-breakpoint-up(md) {
      transition:all 1.5s ease;
    }
    @include media-breakpoint-up(lg) {
      right: 20px;
      top: 20px;
    }
    @include media-breakpoint-up(xl) {
      right: 30px;
      top: 30px;
    }
  }

  &__inner {
    position: relative;
    z-index: 2;
    background: #000;
    padding: 20px 15px;
    display: flex;
    justify-content: space-between;
    height: 100vh;

    @include media-breakpoint-up(lg){
      height: auto;
      padding: 20px;
    }

    @include media-breakpoint-up(xl) {
      align-items: center;
      padding: 30px;
    }
  }

  &__logo {
    flex-shrink: 0;
    width: 44px;
    transition: opacity .3s ease-out;

    &:hover {opacity: .8; }

    @include media-breakpoint-down(md){
      position: absolute;
      left: 15px;
      top: 20px;
    }
  }

  &__nav {
    flex-grow:1;
    margin-top: 50px;
    text-align: center;

    @include media-breakpoint-down(md){
      overflow: auto;
    }

    @include media-breakpoint-up(lg){
      padding: 0 20px;
      margin-top: 0;
    }
  }

  &__close {
    visibility: hidden;
    flex-shrink: 0;

    @include media-breakpoint-down(md){
      position: absolute;
      right: 15px;
      top: 20px;
    }
  }

  &__list {
    position: relative;
    font-size: 18px;

    @include media-breakpoint-down(md){
      padding-bottom: 20px;
      margin-bottom: 20px;
      border-bottom: 1px solid #3c3c3c;
    }
    @include media-breakpoint-up(lg){
      font-size: 16px;
    }
    @include media-breakpoint-up(xl){
      font-size: 18px;
    }
  }

  &__item {
    color: #fff;

    @include media-breakpoint-up(lg) {
      display: inline-block;
      vertical-align: middle;

      & + & {
        &:before {
          margin: 0 5px;
          content: '|';
          color: #666;
        }
      }
    }
  }

  &__link {
    padding: 5px 0;
    display: block;
    color: inherit;
    text-transform: uppercase;
    transition: color .3s ease-out;

    &:hover,
    &.is-active { color: $brand-primary; }

    @include media-breakpoint-up(lg) {
      display: inline-block;
      padding: 0;
    }

    @include media-breakpoint-up(xl){
      display: inline;
      padding: 0;
    }
  }


  &__sub {
    font-size: 12px;
    display: none;

    @include media-breakpoint-up(lg){
      display: block !important;
      position: absolute;
      left: 0;
      top: 100%;
      width: 100%;
      padding-top: 10px;
      opacity: 0;
      visibility: hidden;
      transition: all .4s ease;

      li:hover > & {
        visibility: visible;
        opacity: 1;
      }
    }
  }
  &__subitem {
    color: #9d9d9d;

    @include media-breakpoint-up(lg) {
      display: inline;
      & + & {
        &:before {
          margin: 0 5px;
          content: '|';
          color: #484848;
        }
      }
    }
  }


  &__overlay {
    height: 100vh;
    top: auto;
    bottom: 0;
    visibility: visible;

    .overlay__item--green {
      transform: translateY(-100%);
    }
  }
}

//menu animation
$menu-transition: .5s;
@include media-breakpoint-down(sm){
  .menu__inner {
    opacity: 0;
    transition: all $menu-transition ease-out;

    .is-menu-open & {
      transform: translateY(100%);
      opacity: 1;
    }
  }

  .menu__overlay {
    display: none;
  }
}


@include media-breakpoint-up(md){
  .menu__inner {transition: transform $menu-transition ease $menu-transition}
  .menu__overlay {
    opacity: 0;
    transition: all $menu-transition ease $menu-transition;
  }
  .menu__overlay  .overlay__item--green {transition: transform $menu-transition*2 ease }


  .is-menu-open {
    .menu__overlay,
    .menu__inner ,
    .menu__overlay .overlay__item--green {transform: translateY(100%);}

    .menu__inner {transition: transform $menu-transition ease $menu-transition}
    .menu__overlay {
      opacity: 1;
      transition: all $menu-transition ease
    }
    .menu__overlay .overlay__item--green {transition: transform $menu-transition*2 ease $menu-transition}
  }
}
