<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('as_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('video_file');
            $table->string('image');
            $table->string('name_ru');
            $table->string('name_en');
            $table->text('annotation_ru');
            $table->text('annotation_en');
            $table->text('body_ru');
            $table->text('body_en');
            $table->string('meta_title_ru');
            $table->string('meta_title_en');
            $table->string('meta_keywords_ru');
            $table->string('meta_keywords_en');
            $table->string('meta_description_ru');
            $table->string('meta_description_en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('as_pages');
    }
}
