<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('as_projects_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('as_projects')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('type', ['slider', 'gallery', 'honors']);
            $table->string('image');
            $table->string('alt_ru');
            $table->string('alt_en');
            $table->string('title_ru');
            $table->string('title_en');
            $table->string('caption_ru');
            $table->string('caption_en');
            $table->integer('position');
            $table->tinyInteger('visible')->default(1);
            $table->tinyInteger('active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('as_projects_images');
    }
}
