<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('as_publications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('alt_ru');
            $table->string('alt_en');
            $table->string('title_ru');
            $table->string('title_en');
            $table->integer('position');
            $table->tinyInteger('visible')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('as_publications');
    }
}
