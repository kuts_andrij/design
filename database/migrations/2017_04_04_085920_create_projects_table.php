<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('as_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('as_projects_categories')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('type', ['appartment', 'house', 'office']);
            $table->string('slug');
            $table->string('video_file');
            $table->string('video_screen');
            $table->string('video_youtube');
            $table->string('object_point');
            $table->string('name_ru');
            $table->string('name_en');
            $table->string('object_ru');
            $table->string('object_en');
            $table->string('location_ru');
            $table->string('location_en');
            $table->string('space_ru');
            $table->string('space_en');
            $table->string('client_ru');
            $table->string('client_en');
            $table->text('annotation_ru');
            $table->text('annotation_en');
            $table->text('body_slider_ru');
            $table->text('body_slider_en');
            $table->text('body_gallery_ru');
            $table->text('body_gallery_en');
            $table->text('body_video_ru');
            $table->text('body_video_en');
            $table->string('meta_title_ru');
            $table->string('meta_title_en');
            $table->string('meta_keywords_ru');
            $table->string('meta_keywords_en');
            $table->string('meta_description_ru');
            $table->string('meta_description_en');
            $table->integer('position');
            $table->integer('position_photos');
            $table->integer('position_videos');
            $table->integer('position_honors');
            $table->tinyInteger('visible')->default(1);
            $table->tinyInteger('visible_main_slider')->default(0);
            $table->tinyInteger('visible_main_thumbs')->default(0);
            $table->tinyInteger('visible_main_honors')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('as_projects');
    }
}
