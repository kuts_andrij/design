<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Project;
use App\Models\ProjectCategory;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Types
        $types = ['appartment', 'house', 'office'];

        // Categories ids
        $categories = ProjectCategory::all();
        $categories_ids = [];
        foreach( $categories as $category )
          $categories_ids[] = $category->id;
        
        // RU
        $faker_ru = Faker\Factory::create('ru_RU');

        // EN
        $faker_en = Faker\Factory::create();

        // Streem
        for( $i = 1; $i <= 12; $i++ )
        {
            $category_random_id = array_rand($categories_ids, 1);
            $type_random = array_rand($types, 1);

            $project_name_ru = $faker_ru->company;
            $project_name_en = $faker_en->company;

            Project::create([
                'category_id'     => intval( $categories_ids[$category_random_id] ),
                'type'            => $types[$type_random],
                'slug'            => \Slug::make( $project_name_ru ),
                'name_ru'         => $project_name_ru,
                'name_en'         => $project_name_en,
                'object_ru'       => $project_name_ru,
                'object_en'       => $project_name_en,
                'location_ru'     => $faker_ru->address,
                'location_en'     => $faker_en->address,
                'client_ru'       => $faker_ru->name,
                'client_en'       => $faker_en->name,
                'annotation_ru'   => $faker_ru->paragraph(5),
                'annotation_en'   => $faker_en->paragraph(5),
                'body_slider_ru'  => $faker_ru->paragraph(15),
                'body_slider_en'  => $faker_en->paragraph(15),
                'body_gallery_ru' => $faker_ru->paragraph(15),
                'body_gallery_en' => $faker_en->paragraph(15),
                'body_video_ru'   => $faker_ru->paragraph(15),
                'body_video_en'   => $faker_en->paragraph(15),
                'meta_title_ru'   => $project_name_ru,
                'meta_title_en'   => $project_name_en
            ]);
        }
    }
}
