-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 18 2017 г., 14:02
-- Версия сервера: 5.6.31
-- Версия PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `grabovskiy`
--

-- --------------------------------------------------------

--
-- Структура таблицы `as_about_page`
--

CREATE TABLE IF NOT EXISTS `as_about_page` (
  `id` int(10) unsigned NOT NULL,
  `year_1_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year_1_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `year_1_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `year_2_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year_2_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `year_2_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `year_3_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year_3_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `year_3_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `education_dev_title_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `education_dev_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `education_dev_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `education_dev_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `philosophy_1_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `philosophy_1_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `philosophy_2_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `philosophy_2_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `philosophy_3_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `philosophy_3_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `philosophy_4_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `philosophy_4_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_1_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_1_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_2_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_2_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_3_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_3_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_4_text_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `how_work_4_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_about_page`
--

INSERT INTO `as_about_page` (`id`, `year_1_number`, `year_1_text_ru`, `year_1_text_en`, `year_2_number`, `year_2_text_ru`, `year_2_text_en`, `year_3_number`, `year_3_text_ru`, `year_3_text_en`, `education_dev_title_ru`, `education_dev_title_en`, `education_dev_text_ru`, `education_dev_text_en`, `philosophy_1_text_ru`, `philosophy_1_text_en`, `philosophy_2_text_ru`, `philosophy_2_text_en`, `philosophy_3_text_ru`, `philosophy_3_text_en`, `philosophy_4_text_ru`, `philosophy_4_text_en`, `how_work_1_text_ru`, `how_work_1_text_en`, `how_work_2_text_ru`, `how_work_2_text_en`, `how_work_3_text_ru`, `how_work_3_text_en`, `how_work_4_text_ru`, `how_work_4_text_en`, `created_at`, `updated_at`) VALUES
(1, '2009', 'подарил мне победу в конкурсе «ЛУЧШИЙ ИНТЕРЬЕР ГОДА»', 'gave me the victory in the contest "BEST INTERIOR OF THE YEAR"', '2010', 'стал новой ступенькой в профессиональной карьере. Премия Национального Союза архитекторов Украины - приз «ЗОЛОТАЯ ВАЛЮТА».', 'also became a step in a professional career. Prize of the National Union of Architects of Ukraine - prize "GOLDEN CURRENCIES".', '2012', 'начала свою самостоятельную жизнь студия «Alexandr Grabovskiy»', 'began its independent life studio "AlexandrGrabovskiy"', 'Мое обучение и развитие продолжаются', 'My training and development continues.', '<br>\r\n<p>Страсть к работе означает всегда расти и совершенствоваться.</p>\r\n', '<p>Professionalism is reflected in my work and future projects.</p>\r\n<p>I continue to fill with new information, which is collected from diverse workshops, schools, exhibitions, lectures, held by masters of modern architectural and design art.</p>', 'Будьте смелее в своих желаниях! Иногда очень нужно изменить окружение.\r\nКогда человека окружает красота, когда пространство продуманно до мелочей, когда каждое помещение наполнено уютом и комфортом - все это вдохновляет и заряжает нас положительными эмоциями и хорошим настроением на весь день, желанием жить и радоваться жизни. \r\nПроектируя для наших клиентов, мы меняем их жизнь к лучшему.', 'Be braver in your desires!', 'Каждый проект - это как чистый лист. У него есть своя история. История одного человека, большой семьи или целой компании.....Он индивидуален.', 'And the natural desire of a person to decorate his life with beautiful events, objects, environment, clothing. This idea became important in my work.', '<p>Эта идея и стала основой моей работе. Внимание к деталям  и  мелочам, которыми наполнен проект. Представьте, что даже маленький светильник может озарить помещение, придав ему торжественность, серьезность момента или мягкую уютную атмосферу.</p>', 'Imagine that even a small lamp can illuminate the room, giving it solemnity, seriousness of the moment or a soft comfortable base.', '<p>Мы хотим и стремимся создавать пространства которые будут существовать вне времени и не будут зависеть от  тенденций и моды.</p>\r\n<p>Мне интересны проекты, в которых есть возможность реализовать самые смелые идеи и выстроить теплые и основанные на доверии взаимоотношения с заказчиком.</p>', 'I''m interested in projects in which simple things give us a feeling of working or relaxation, the joy of active recreation surrounded by friends or the seriousness of a business conversation with partners.', '<b>Постановка задачи.</b>\r\n<p>Перед началом работы над проектом мы проводим одну или несколько встреч с клиентом. На первой встрече мы определим ваши ожидания, цели и задачи. Наметим первые ориентиры дизайна, отражающие ваш образ жизни, настроение темперамент.</p>', 'Meeting with the customer. At the first meeting we will determine your expectations, preferences in style and colors. Let''s outline the first design guidelines, reflecting the lifestyle, the temperament of the temperament, the customer. So that it is your desires, perfected by the designer''s experience, that would create the interior of your dreams.', '<b>Визуализация.</b>\r\n<br>Без визуализации интерьера достаточно сложно представить, какой образ примет дом, квартира или офис по окончанию работ. Для заказчика это приятная необходимость, которая только подтвердит, что выбор сделан правильно.', 'Visualization. The opportunity to plunge into reality will provide 3D visualization. You will look at all the details of the design, feel the comfort zones and realize the additional potencies of some interior items. And we will make all the necessary changes.', '<strong>Презентация проекта.</strong> \r\n<p>После согласования всех правок с заказчиком, предоставляем уже готовый для успешной реализации дизайн-проект с перечнем используемого в нем оборудования, отделочных материалов и мебели.</p>', 'Presentation of the project. By agreeing all the children, we will present a ready-to-implement design project with a list of equipment, decoration materials and furniture. Such a project will provide an opportunity to take into account all the costs of the budget and plan your budget. This document will insure you from the "experimental" approach at the construction site.', '<b>Авторский надзор.</b> \r\n<p>Быть ответственным за результат - вот, что действительно важно. Проконтролировать процесс до мельчайших деталей, проследить за тем, чтобы конечный итог в точности соответствовал дизайн-проекту - всё это является неотъемлемой частью нашей работы.</p>', 'Author''s supervision. Responsibility for the result is our credo at the stage of implementation of the architectural project. We control the process to the smallest detail, follow the correspondence of the design project to the final result.', '0000-00-00 00:00:00', '2017-08-22 08:21:35');

-- --------------------------------------------------------

--
-- Структура таблицы `as_feedbacks`
--

CREATE TABLE IF NOT EXISTS `as_feedbacks` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_feedbacks`
--

INSERT INTO `as_feedbacks` (`id`, `name`, `phone`, `email`, `comment`, `created_at`, `updated_at`) VALUES
(6, 'Test', '0662303604', 'ivanhope@mail.ru', '11111111111111111', '2017-09-12 06:57:00', '2017-09-12 06:57:00');

-- --------------------------------------------------------

--
-- Структура таблицы `as_main_page`
--

CREATE TABLE IF NOT EXISTS `as_main_page` (
  `id` int(10) unsigned NOT NULL,
  `section_2_photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_2_content_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `section_2_content_en` text COLLATE utf8_unicode_ci NOT NULL,
  `section_3_image_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_3_image_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_3_image_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_3_image_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_6_photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_6_content_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `section_6_content_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_main_page`
--

INSERT INTO `as_main_page` (`id`, `section_2_photo`, `section_2_content_ru`, `section_2_content_en`, `section_3_image_1`, `section_3_image_2`, `section_3_image_3`, `section_3_image_4`, `section_6_photo`, `section_6_content_ru`, `section_6_content_en`, `created_at`, `updated_at`) VALUES
(1, '1488286543_sabout.jpg', '<p>Быть влюблённым в своё дело – значит быть частью его! Я создаю не только пространство для жизни, но и настроение. Пространство, которое окрыляет и вдохновляет, в котором хочется жить,отдыхать, и творить.</p>\r\n<p class="text-cite">Главное для меня - это подарить заказчику эмоцию, <br/>создать нечто большее, чем просто проект.</p>\r\n<p>И в своих работах я дополняю её завораживающей игрой света и цвета, гармоничным сочетанием натуральных материалов, сохраняя при этом целостность всего помещения. Прекрасное не любит тишину. Оно должно быть реализовано. И я не боюсь воплощения нестандартных идей - это мой любимый процесс, который я всегда довожу до совершенства!</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque auctor ex et eros pulvinar, ac aliquam mi ullamcorper. Nulla sit amet lectus semper, facilisis purus at, imperdiet ipsum. Suspendisse potenti. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla et nulla erat. Vestibulum at erat et tortor semper dictum at a elit. Aenean sed nibh ante. Integer turpis felis, dignissim ac ornare et, tincidunt id neque. Sed dui nulla, fermentum hendrerit dapibus eget, ornare nec arcu. Proin dignissim massa non ipsum volutpat molestie.', '1488205360_t-photocard2.jpg', '1488205491_t-photocard1.jpg', '1488205613_t-photocard3.jpg', '1488205715_t-photocard4.jpg', '1488204132_t-interviews.jpg', 'Задача организации, в особенности же постоянное информационно-пропагандистское обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.', '<strong>Esquire:</strong> While the notion that structural and aesthetic?<br/>\r\n<strong>Alexandr:</strong> Onsiderations should be entirely subject to functionality was met with both popularity and skepticism, it had the effect of introducing the concept of "function" in place of Vitruvius'' "utility".', '0000-00-00 00:00:00', '2017-04-26 12:21:31');

-- --------------------------------------------------------

--
-- Структура таблицы `as_menu`
--

CREATE TABLE IF NOT EXISTS `as_menu` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_menu`
--

INSERT INTO `as_menu` (`id`, `parent_id`, `lft`, `rgt`, `depth`, `slug`, `name_ru`, `name_en`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 2, 0, '', 'Главная', 'Home', '2017-02-27 13:14:09', '2017-04-11 08:58:39'),
(2, NULL, 3, 4, 0, 'about', 'Александр Грабовский', 'Alexandr Grabovskiy', '2017-02-27 13:17:38', '2017-03-13 06:13:29'),
(3, NULL, 5, 14, 0, 'category/projects-all', 'Проекты', 'Projects', '2017-02-27 13:18:35', '2017-04-11 10:52:57'),
(5, NULL, 19, 20, 0, 'projects-honors', 'Награды', 'Honors', '2017-02-27 13:23:24', '2017-04-10 09:05:12'),
(6, NULL, 21, 22, 0, 'publications', 'Медиа', 'Media', '2017-02-27 13:25:33', '2017-04-10 09:05:51'),
(7, NULL, 23, 24, 0, 'contacts', 'Контакты', 'Contacts', '2017-02-27 13:26:18', '2017-03-13 06:15:43'),
(8, 3, 6, 7, 1, 'category/completed-projects', 'Завершенные проекты', 'Completed projects', '2017-02-27 13:27:49', '2017-04-03 12:37:42'),
(9, 3, 8, 9, 1, 'category/interior-design', 'Дизайн интерьера', 'Interior design', '2017-02-27 13:32:22', '2017-04-03 12:37:53'),
(10, 3, 10, 11, 1, 'category/architecture', 'Архитектура', 'Architecture', '2017-02-27 13:33:32', '2017-04-03 12:38:04'),
(11, 3, 12, 13, 1, 'category/product-design', 'Дизайн продукта', 'Product design', '2017-02-27 13:37:35', '2017-04-03 12:38:16'),
(12, NULL, 15, 16, 0, 'projects-photo', 'Фото', 'Photo', '2017-03-13 06:14:51', '2017-04-10 09:04:33'),
(13, NULL, 17, 18, 0, 'projects-video', 'Видео', 'Video', '2017-03-13 06:15:37', '2017-04-10 09:04:50');

-- --------------------------------------------------------

--
-- Структура таблицы `as_pages`
--

CREATE TABLE IF NOT EXISTS `as_pages` (
  `id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `annotation_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `annotation_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `body_en` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_pages`
--

INSERT INTO `as_pages` (`id`, `slug`, `video_file`, `image`, `name_ru`, `name_en`, `annotation_ru`, `annotation_en`, `body_ru`, `body_en`, `meta_title_ru`, `meta_title_en`, `meta_keywords_ru`, `meta_keywords_en`, `meta_description_ru`, `meta_description_en`, `created_at`, `updated_at`) VALUES
(2, '', '', '', 'Главная', 'Home', '', '', '', '', 'Дизайн интерьера в Днепре - Студия Александра Грабовского', 'Interior Design in Dnipro - Alexandr Grabovskiy Studio', 'дизайн интерьера, Днепр, Александр, Грабовский', 'Interior design, Dnepr, Alexandr, Grabovskiy', '✎____Студия Александра Грабовского: современный и стильный дизайн помещений. Услуги дизайна интерьера, авторский надзор. \r\n☎ +38 (098) 071-70-70 Днепр, Украина ', '✎ ____ Alexandr Grabovskiy studio: modern and stylish design. Interior design services, author supervision.\r\n☎ +38 (098) 071-70-70 Dnepr, Ukraine', '2017-04-10 08:56:26', '2017-08-29 11:59:33'),
(3, '404', '', '1492101256_b-404error-2kx9iwf8.jpg', '404', '404', '', '', '<p>Страница не найдена...</p>', '<p>Page not found...</p>', '404', '404', '', '', '', '', '2017-04-10 08:57:37', '2017-04-24 11:08:06'),
(4, 'about', '', '1491825604_bg-about.jpg', 'Александр Грабовский', 'Alexandr Grabovskiy', '<p><strong>Воплощение нестандартных идей - это наш любимый процесс, который мы всегда доводим до совершенства!</strong></p>', '<p>The embodiment of non-standard ideas is our favorite process, which we always bring to perfection!</p>', '<p>Яркое желание передать красоту мира в красках подарил мне отец.</p>\r\n<p>Бабушка сохранила его рисунки, ставшие для меня вдохновением, музой в профессиональной деятельности. Творчество отца показало мне образец личностного развития: "Быть требовательным к эстетике и к самому себе".</p>\r\n<p>Мое школьное увлечение рисованием, математикой, физикой, геометрией выросло в убежденность при выборе будущего образования. И, конечно, архитектура, как гармония творчества и точных наук, совместила мою любовь к прекрасному, форме, материалам, свету.</p>\r\n<p>После окончания школы продолжил учебу в Приднепровской Государственной Академии строительства и архитектуры.</p>\r\n<p>Практика в крупном архитектурном бюро в Польше дала возможность получить значительный опыт и, конечно же, незабываемые воспоминания о работе в студии Мильмана Михаила.</p>', '<p>A bright desire to convey the beauty of the world in colors was presented to me by my father.</p>\r\n<p>Grandmother preserved his drawings, which became for me an inspiration, a muse in professional work. My father''s work showed me a model of personal development: "Be demanding of aesthetics and to yourself."</p>\r\n<p>My school passion for drawing, mathematics, physics, geometry has grown in conviction in choosing the future of education. And, of course, architecture, like the harmony of creativity and exact sciences, combined my love for beautiful, form, materials, light.</p>\r\n<p>After graduation, he continued his studies at the Pridneprovsky State Academy of Construction and Architecture.</p>\r\n<p>Many interesting experiences and ideas were given by practice in a large architectural bureau in Poland, Poznan. And, of course, grateful memories of the work in the studio Milman Michael.</p>', 'Александр Грабовский - Дизайнер интерьеров ', 'Alexandr Grabovskiy', '', '', '✔Создать уют в своем доме легко! Звоните! ☎ +38 (098) 071-70-70', '✔Creating comfort in your home is easy! Call us! ☎ +38 (098) 071-70-70', '2017-04-10 09:00:04', '2017-08-29 12:02:58'),
(5, 'projects-photo', '1500460670_reel-photo-5.mp4', '1503316419_img-2293-2-min.jpg', 'Фото', 'Photo', '', '', '', '', 'Фото дизайна интерьеров помещений, домов, офисов от студии Александра Грабовского', 'Photo', '', '', '✍ Фото галерея дизайна интерьера: огромный выбор дизайнов домов и офисов от студии Александра Грабовского ☎ 38 (098) 071-70-70', '', '2017-04-10 09:02:18', '2017-09-11 07:34:09'),
(6, 'projects-video', '1500190808_reel-5.mp4', '1503316469_img-1768-min.jpg', 'Видео', 'Video', '', '', '', '', 'Видео дизайна интерьеров от студии Александра Грабовского', 'Video', '', '', '▀▄ Современные идеи дизайна интерьера на видео ✔ Проектирование дизайна помещений от студии Александра Грабовского', '', '2017-04-10 09:03:47', '2017-09-11 07:38:20'),
(7, 'projects-honors', '', '1503222863_diplome.jpg', 'Награды', 'Honors', '', '', '', '', 'Награды и дипломы студии дизайна Александра Грабовского', 'Honors', '', '', '⭐ Дипломы и почетные грамоты студии дизайна Александра Грабовского ✔ Лучшие работы', '', '2017-04-10 09:08:51', '2017-09-11 07:42:45'),
(8, 'contacts', '', '1491826201_bg-contacts.jpg', 'Контакты', 'Contacts', '', '', '', '', 'Контакты ✔ Студия дизайна Александра Грабовского', 'Contacts', '', '', '✍ Контакты студии дизайна интерьера Александра Грабовского ► Телефон, адрес, e-mail', '', '2017-04-10 09:10:01', '2017-09-11 07:45:29'),
(9, 'projects-all', '1502605051_1500190808-reel-5.mp4', '1503387827_332.jpg', 'Проекты', 'Projects', '', '', '', '', 'Проекты студии дизайна интерьера ➣ Александра Грабовского', 'Projects', '', '', '⬔ Стильные и современные проекты студии дизайна ➣ Александра Грабовского по оформлению интерьеров квартир и домов ⭐ большинство из которых отмечены престижными наградами', '', '2017-04-11 10:54:01', '2017-09-11 07:52:13'),
(10, 'publications', '', '1492096454_bg-awards.jpg', 'Публикации', 'Publications', '', '', '<p>Нишевый проект по-прежнему устойчив к изменениям спроса. Бренд методически концентрирует потребительский выставочный стенд. Бизнес-стратегия масштабирует эмпирический медиамикс.</p>\r\n<p>Основная стадия проведения рыночного исследования,&nbsp;следовательно, программирует бизнес-план, учитывая современные тенденции. Бренд обоснован необходимостью. Итак, ясно, что conversion rate однообразно продуцирует креативный диктат потребителя.</p>', '', 'Публикации ✏ Студия дизайна Александра Грабовского', 'Publications', '', '', '☝ Интересные факты из мира дизайна и архитектуры ✏ от студии дизайна Александра Грабовского', '', '2017-04-13 11:52:19', '2017-09-11 07:58:41');

-- --------------------------------------------------------

--
-- Структура таблицы `as_projects`
--

CREATE TABLE IF NOT EXISTS `as_projects` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `type` enum('appartment','house','office') COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_screen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `object_point` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `object_year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `object_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `object_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `space_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `space_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photographer_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photographer_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `annotation_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `annotation_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_slider_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `body_slider_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_gallery_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `body_gallery_en` text COLLATE utf8_unicode_ci NOT NULL,
  `body_video_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `body_video_en` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `position_photos` int(11) NOT NULL,
  `position_videos` int(11) NOT NULL,
  `position_honors` int(11) NOT NULL,
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  `visible_main_slider` tinyint(4) NOT NULL DEFAULT '0',
  `visible_main_thumbs` tinyint(4) NOT NULL DEFAULT '0',
  `visible_main_honors` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_projects`
--

INSERT INTO `as_projects` (`id`, `category_id`, `type`, `slug`, `video_file`, `video_screen`, `video_youtube`, `object_point`, `object_year`, `name_ru`, `name_en`, `object_ru`, `object_en`, `location_ru`, `location_en`, `space_ru`, `space_en`, `client_ru`, `client_en`, `photographer_ru`, `photographer_en`, `annotation_ru`, `annotation_en`, `body_slider_ru`, `body_slider_en`, `body_gallery_ru`, `body_gallery_en`, `body_video_ru`, `body_video_en`, `meta_title_ru`, `meta_title_en`, `meta_keywords_ru`, `meta_keywords_en`, `meta_description_ru`, `meta_description_en`, `position`, `position_photos`, `position_videos`, `position_honors`, `visible`, `visible_main_slider`, `visible_main_thumbs`, `visible_main_honors`, `created_at`, `updated_at`) VALUES
(2, 6, 'office', 'soedinyaya-kontinenty', '1504244937_connecting-continents---31082017-30fps.mp4', '1506004826_1503316247-completed-projects-min1.jpg', 'https://www.youtube.com/watch?v=J8zslVvUdic', '48.52 34.98322', '2014', 'Connecting continents', 'Connecting continents', 'Connecting continents', 'Connecting continents', 'Днепр, Украина', 'Днепр,Украина', '600 м.кв.', '', '', '', 'Авдеенко Андрей.', 'Авдеенко Андрей', 'Перед  нами была поставлена задача создать уютный и комфортный интерьер офиса - который будет новым "домом " для дружного коллектива талантливых людей.', 'Перед  нами была поставлена задача создать уютный и комфортный интерьер офиса - который будет новым "домом " для дружного коллектива талантливых людей.', '<p>Данный проект - это реконструкция существующего&nbsp; здания. Поэтому определенные ограничения существовали.</p>\r\n<p>На первых двух этажах расположились : рецепция, кухня - столовая, замечательная терраса, где можно отдохнуть и полюбоваться прекрасным видом на город, ну и конечно &nbsp;&nbsp;"рабочий" коллектив команды.</p>\r\n<p>Интерьер&nbsp; достаточно лаконичен, в выдержанной природной цветовой гамме. С расставленными акцентами во входной зоне, зоне рецепции - с логотипами компании.</p>', '<p>Данный проект - это реконструкция существующего&nbsp; здания. Поэтому определенные ограничения существовали.</p>\r\n<p>На первых двух этажах расположились : рецепция, кухня - столовая, замечательная терраса, где можно отдохнуть и полюбоваться прекрасным видом на город, ну и конечно &nbsp;&nbsp;"рабочий" коллектив команды.</p>', '<p>Третий этаж это мансардное помещение &nbsp;- на нем расположились руководители компании , зал для совещаний , зона ожидания для посетителей.</p>\r\n<p>Из кабинета руководителей , через витражное остекление, открывается потрясающая панорама города и реки Днепр. Мебель изготовлена из натурального дерева по авторским чертежам местными мастерами.&nbsp; Деревянные панели - облегчает скользящий скрытый свет, подчеркивающий декоративную фактуру стен.&nbsp; Подвешенные на балках&nbsp; светильники - как парящие в небе чайки , римские шторы - как приспущенные паруса&nbsp; яхты, уютная зона отдыха - создают комфортную среду для продуктивного рабочего дня и покорения новых вершин.</p>', '<p>Интерьер&nbsp; достаточно лаконичен, в выдержанной природной цветовой гамме. С расставленными акцентами во входной зоне, зоне рецепции - с логотипами компании.</p>\r\n<p>Третий этаж это мансардное помещение &nbsp;- на нем расположились руководители компании , зал для совещаний , зона ожидания для посетителей.</p>', '', '<p>Из кабинета руководителей , через витражное остекление, открывается потрясающая панорама города и реки Днепр. Мебель изготовлена из натурального дерева по авторским чертежам местными мастерами.&nbsp; Деревянные панели - облегчает скользящий скрытый свет, подчеркивающий декоративную фактуру стен.&nbsp; Подвешенные на балках&nbsp; светильники - как парящие в небе чайки , римские шторы - как приспущенные паруса&nbsp; яхты, уютная зона отдыха - создают комфортную среду для продуктивного рабочего дня и покорения новых вершин.</p>', 'Connecting continents', 'Connecting continents', 'Connecting continents', 'Connecting continents', 'Connecting continents', 'Connecting continents', 3, 4, 3, 1, 1, 1, 1, 0, '2017-04-11 14:37:28', '2017-09-21 11:40:26'),
(8, 6, 'appartment', 'dream-nest', '1503139121_dream-nest-mute.mp4', '1504245353_^2d1dfe569ab183ec81108afb0b0a55d12fc0c5e7a2bbb5af06^pimgpsh-fullsize-distr.jpg', 'https://www.youtube.com/watch?v=Y8GFMAi8gbw&feature=youtu.be', '48.52 34.98322', '', 'Dream nest', 'Dream nest', 'Dream nest', 'Dream nest', 'Днепр, Украина', 'Dnipro, Ukraine', '80 м.кв.', '', '', '', 'Ангеловский Александр', '', '80 кв.м - со свободной планировкой, не считая двух металлических колонн.\r\n\r\nКвартира имеет хорошую высоту потолков, большие окна. Хозяйка квартиры,\r\nочень приятная , состоявшаяся как личность женщина - хотела получить "уютное гнездо" в центре города. Мы попытались его таким и создать. \r\n', 'Et rerum error consectetur nulla vero. Sit dolores non voluptatem nesciunt velit. Debitis eaque velit omnis accusantium. Animi sit quisquam illo perferendis deserunt nihil vel. Labore laboriosam et delectus dolorem quis. Incidunt ipsum atque reiciendis porro officia. Quia voluptatem deserunt alias aut labore possimus libero porro.', '<p>Сразу было решено оставить "ощущение воздуха" и максимально использовать высоту потолка - наполнить естественным освещением входную группу (коридор и с.у.) через псевдоокна, которые стирают границы потолка, перетекающего в другие помещения. Они же переходят в окна-светильники на смежные стены.</p>\r\n<p>Есть разделение на две основные зоны - одна зона&nbsp; это входная группа&nbsp; (коридор, постирочная, с.у.) переходящая&nbsp; в объединенное пространство кухни и гостинной, и вторая - приватная - &nbsp;спальня и кабинет.</p>', '<p>Odit dolor nulla voluptatibus voluptas nam aliquam laudantium. Et dolorem veniam tempora eligendi. Quibusdam quo assumenda qui in voluptatem. Perferendis ipsum quasi reprehenderit deleniti illum nesciunt labore. Deserunt rerum nam enim dolorem id illo. A enim in perspiciatis expedita. Sunt quia quibusdam rem fuga porro doloribus qui officiis. Consequuntur est similique ducimus eaque. Ad consectetur laudantium et blanditiis et a architecto. Magnam quam sed tempora quia accusamus atque. Quibusdam eum quasi iusto expedita.</p>', '<p>В интерьере много внимания уделено функциональности, отделке, фактурам материалов.</p>', '<p>Pariatur autem autem minima ipsam quia. Rerum atque aut iste eius ea earum. Minima sed commodi et. Voluptatum voluptatem et enim ullam et sed. Excepturi culpa reprehenderit quia inventore quos nostrum. Harum numquam mollitia quis fugit. Quas et voluptatem animi amet. Nemo alias aspernatur fuga nesciunt ipsam. Nulla et magnam ipsum qui. Voluptate laudantium blanditiis quos consequuntur. Velit dolore provident et sunt. Unde assumenda natus occaecati aut molestias iusto est optio. Aut atque voluptate molestiae sed ipsam omnis. Esse esse tempore vitae ea eaque aut enim labore. Omnis et dignissimos earum aliquam.</p>', '<p>Натуральный камень, текстуры дерева, фактуры стен - умышленно подчеркнуты интересным освещением. В квартире продуманны множества световых сценариев - которые создают очень уютную, теплую атмосферу для любого настроения хозяйки. Вся мебель для хранения - визуально интегрирована в стены.</p>', '<p>Ea consequatur ipsum odio qui ducimus sequi commodi. Occaecati voluptatibus rerum et sit ut consequatur. Libero nisi esse maiores assumenda. Quis sequi dolor mollitia ea quaerat esse iusto at. Et voluptatem asperiores molestiae non. Ab et pariatur dicta iure dignissimos eum a. Tempora sunt quis dolores aut vero expedita. Aspernatur tempore labore voluptas impedit vero velit et. Nemo molestiae minus distinctio quam veritatis sunt accusamus. Distinctio qui error nostrum animi et quia ea. Vel odio qui eum. Assumenda ipsam eaque est tempore. Voluptatem incidunt est quas rerum eaque doloribus dolorem. Vel sed aut occaecati porro.</p>', 'Dream nest', 'Dream nest', 'Dream nest', 'Dream nest', 'Dream nest', 'Dream nest', 2, 1, 2, 8, 1, 1, 1, 0, '2017-04-11 14:37:28', '2017-09-02 12:56:20'),
(10, 6, 'house', 'house-in-the-forest', '1497347306_video---tsiklichnoe.mp4', '1504177733_1.jpg', 'https://www.youtube.com/watch?v=-y94lIMoy_M&t=11s', '50.45466 30.5238', '2015', 'House in the forest', 'House in the forest', 'House in the forest', 'House in the forest', 'Киев, Украина', '', '400 м.кв.', '', '', '', 'Авдеенко Андрей, Ангеловский Александр', '', 'При первой встрече на участке с заказчиком - место, атмосфера, воздух - сразу вдохновили на общую идею интерьера.\r\nУчасток расположен в сосновом живописном лесу.\r\nМесто, взаимопонимание и доверие между заказчиком и архитектором создали гармонию и уют в интерьере, и грань с экстерьером практически исчезла.\r\nМного внимания было уделено материалам, фактурам. Дерево, камень, природная цветовая гамма.\r\nБольшую роль в интерьере играет освещение. Светом подчеркнута "живая фактура" материалов. Разные его комбинации создают совершенно разное настроение в интерьере.', '', '<p>Переступив порог дома попадаешь в просторный холл, перетекающий&nbsp; в зону кухни - столовой, и уютной гостиной с большим камином.</p>\r\n<p>Объемное панно из натурального дерева (изготовлено по авторским чертежам - местной столярной мастерской, как и практически вся мебель в доме) - является основным акцентом - т.к. находится на транзите между "Дневной и Ночной жизнью" в доме + оно полностью просматривается с уличной террасы и кухни.</p>\r\n<p></p>', '', '', '', '<p>На второй этаж ведет лестница с воздушным стеклянным ограждением.</p>\r\n<p>В детских комнатах - для мальчика и девочки было создано свое индивидуальное пространство. Структура древесины дуба - из которого изготовлена вся мебель, - только добавила тепла и атмосферы. Между детскими комнатами находится яркий санузел.</p>\r\n<p>В почти квадратной в плане - спальне, и боковым ленточным остеклением с двух сторон - кровать сразу заняла свое место в центре. А панели с подсвеченным Ониксом - добавили &nbsp;романтики вечернему освещению помещения.</p>\r\n<p>Бильярдная на первом этаже - немного брутальна - бельгийская плитка ручной формовки и панели из состаренного дуба, очень удобные кожаная&nbsp; мягкая мебель - отличное место отдохнуть в хорошей компании друзей.</p>\r\n<p>Из бильярдной -&nbsp; можно подняться по камерной лестнице в кинозал.</p>\r\n<p>Дополнительный уют создают &nbsp;текстиль и авторская керамика Сергея Горбаня, созданная автором под данный объект.</p>', '', 'House in the forest', 'Erdman-McClure', '', '', '', '', 1, 11, 1, 10, 1, 1, 1, 1, '2017-04-11 14:37:28', '2017-09-01 12:29:18'),
(13, 6, 'appartment', 'step-forward', '1504280106_step-forward---31082017-mute-30fps.mp4', '1502604595_foto-na-glavnoy.jpg', 'https://www.youtube.com/watch?v=M839LDQfMsc&t=1s', '56.946 24.10589', '2016', 'Step Forward', 'Step Forward', 'Step Forward', '', 'г. Рига, Латвия', '', '130 м.кв.', '', '', '', 'Сергей Гетников', '', 'Квартира интересна своей локацией - последний этаж современного здания , которое расположено в старой части Риги. \r\nИзюминкой этого объекта конечно же стала лестница  - которая ведет на собственную террасу на крыше , откуда открывается замечательный вид на старую часть Риги.', '', '<p>Задача была создать комфортный и лаконичный интерьер. &nbsp;Это была не первая наша совместная работа с заказчиком, поэтому процесс проходил в полном доверии.</p>\r\n<p></p>', '', '<p>Много внимания было уделено подбору отделочных материалов, сочетанию фактур. В интерьере присутствует природная цветовая гамма, освещение подчеркивает&nbsp; цвета и фактуры материалов.</p>\r\n<p>Лестница - является главным украшением дома. &nbsp;Сочетание металла и дерева получилось очень стильным и лаконичным.</p>', '', '', '', 'Step Forward', 'Step Forward', '', '', 'Step Forward', 'Step Forward', 4, 0, 4, 0, 1, 1, 0, 0, '2017-08-13 03:09:55', '2017-09-05 10:54:12'),
(14, 6, 'house', 'house-in-the-river', '1502953182_rybalskoe-cycle-mute-2(1).mp4', '1502605340_foto-na-glavnoy.jpg', 'https://www.youtube.com/watch?v=vY4WJYsezec', '48.52 34.98322', '', 'House by the river', '', 'House by the river', '', 'Днепр, Украина', '', '100 м.кв.', '', '', '', 'Ангеловский Александр', '', 'Дом находится на берегу реки. Из его окон открывается прекрасная панорама на правый берег Днепра.', '', '<p>Перед нами стояла задача создать уютный, просторный Домик для гостей, для приема близких друзей, для отдыха.</p>\r\n<p><strong>&nbsp;</strong>Эти условия и диктовали основное планировочное решение. Дом разделен на две зоны.</p>', '', '<p>Первая зона &ndash; основная - объединяет столовую, кухню и гостиную. Вторая &ndash; зона отдыха, состоит из комнаты отдыха, душевой, сауны, и с.у.&nbsp;</p>', '', '<p>Интерьер выдержан&nbsp; в стиле прованс. Важно было в основной зоне получить максимально открытое пространство &ndash; чтобы наслаждаться видом на реку, но создать зонирование. Его и решают деревянные колонны, которым составили компанию очень интересно собранные шторы, &nbsp;создающие дополнительный уют.&nbsp; Всю мебель изготавливали по авторским чертежам, светильники &ndash; керамика с росписью. Много внимания уделено&nbsp; отделке, текстилю, подбору материалов.</p>', '', 'House by the river', '', '', '', '', '', 5, 0, 5, 0, 1, 1, 0, 0, '2017-08-13 03:22:20', '2017-09-05 12:28:10');

-- --------------------------------------------------------

--
-- Структура таблицы `as_projects_categories`
--

CREATE TABLE IF NOT EXISTS `as_projects_categories` (
  `id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_screen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body_ru` text COLLATE utf8_unicode_ci NOT NULL,
  `body_en` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_projects_categories`
--

INSERT INTO `as_projects_categories` (`id`, `slug`, `video_file`, `video_screen`, `name_ru`, `name_en`, `body_ru`, `body_en`, `meta_title_ru`, `meta_title_en`, `meta_keywords_ru`, `meta_keywords_en`, `meta_description_ru`, `meta_description_en`, `created_at`, `updated_at`) VALUES
(6, 'completed-projects', '1491233666_video1.mp4', '1503316247_completed-projects-min.jpg', 'Завершенные проекты', 'Completed projects', '', '', 'Завершенные проекты', 'Completed projects', '', '', '', '', '2017-04-03 12:26:51', '2017-08-21 08:50:47'),
(7, 'interior-design', '1503224036_interior-design-mute.mp4', '1503316209_13lm-min.jpg', 'Дизайн интерьера', 'Interior design', '', '', 'Дизайн интерьера', 'Interior design', '', '', '', '', '2017-04-11 10:43:35', '2017-08-21 08:50:09'),
(8, 'architecture', '1502953877_riga-ext-mute.mp4', '1503316175_3-min.jpg', 'Архитектура', 'Architecture', '', '', 'Архитектура', 'Architecture', '', '', '', '', '2017-04-11 10:45:54', '2017-08-21 08:49:36'),
(9, 'product-design', '1503223980_product-design-mute.mp4', '1503316041_img-2485-min.jpg', 'Дизайн продукта', 'Product design', '', '', 'Дизайн продукта', 'Product design', '', '', '', '', '2017-04-11 10:47:57', '2017-08-21 08:47:21');

-- --------------------------------------------------------

--
-- Структура таблицы `as_projects_images`
--

CREATE TABLE IF NOT EXISTS `as_projects_images` (
  `id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `type` enum('slider','gallery','honors') COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alt_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alt_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=716 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_projects_images`
--

INSERT INTO `as_projects_images` (`id`, `project_id`, `type`, `image`, `url`, `alt_ru`, `alt_en`, `title_ru`, `title_en`, `caption_ru`, `caption_en`, `position`, `visible`, `active`, `created_at`, `updated_at`) VALUES
(238, 10, 'gallery', '1497347403_1.jpg', '', '', '', '', '', '', '', 1, 1, 1, '2017-06-13 06:50:03', '2017-09-01 12:24:02'),
(239, 10, 'gallery', '1497347403_2.jpg', '', '', '', '', '', '', '', 2, 1, 0, '2017-06-13 06:50:03', '2017-09-01 12:24:02'),
(240, 10, 'gallery', '1497347403_3.jpg', '', '', '', '', '', '', '', 3, 1, 0, '2017-06-13 06:50:04', '2017-09-01 12:18:40'),
(241, 10, 'gallery', '1497347404_4.jpg', '', '', '', '', '', '', '', 4, 1, 0, '2017-06-13 06:50:04', '2017-09-01 12:18:40'),
(242, 10, 'gallery', '1497347404_5.jpg', '', '', '', '', '', '', '', 5, 1, 0, '2017-06-13 06:50:04', '2017-09-01 12:18:40'),
(243, 10, 'gallery', '1497347404_6.jpg', '', '', '', '', '', '', '', 6, 1, 0, '2017-06-13 06:50:05', '2017-09-01 12:18:40'),
(244, 10, 'gallery', '1497347405_7.jpg', '', '', '', '', '', '', '', 7, 1, 0, '2017-06-13 06:50:05', '2017-09-01 12:18:40'),
(245, 10, 'gallery', '1497347405_8.jpg', '', '', '', '', '', '', '', 8, 1, 0, '2017-06-13 06:50:05', '2017-09-01 12:18:40'),
(420, 10, 'slider', '1497353588_1.jpg', '', '', '', '', '', '', '', 0, 1, 1, '2017-06-13 08:33:08', '2017-06-13 08:33:08'),
(421, 10, 'slider', '1497353588_2.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:08', '2017-06-13 08:33:08'),
(422, 10, 'slider', '1497353588_3.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:08', '2017-06-13 08:33:08'),
(423, 10, 'slider', '1497353588_4.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:09', '2017-06-13 08:33:09'),
(424, 10, 'slider', '1497353589_5.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:09', '2017-06-13 08:33:09'),
(425, 10, 'slider', '1497353589_6.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:09', '2017-06-13 08:33:09'),
(426, 10, 'slider', '1497353589_7.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:09', '2017-06-13 08:33:09'),
(427, 10, 'slider', '1497353589_8.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:10', '2017-06-13 08:33:10'),
(428, 10, 'slider', '1497353590_9.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:10', '2017-06-13 08:33:10'),
(429, 10, 'slider', '1497353590_10.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:10', '2017-06-13 08:33:10'),
(430, 10, 'slider', '1497353590_11.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:10', '2017-06-13 08:33:10'),
(431, 10, 'slider', '1497353590_12.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:10', '2017-06-13 08:33:10'),
(432, 10, 'slider', '1497353590_13.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:11', '2017-06-13 08:33:11'),
(433, 10, 'slider', '1497353591_14.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:11', '2017-06-13 08:33:11'),
(434, 10, 'slider', '1497353591_15.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:11', '2017-06-13 08:33:11'),
(435, 10, 'slider', '1497353591_16.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:11', '2017-06-13 08:33:11'),
(436, 10, 'slider', '1497353591_17.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:12', '2017-06-13 08:33:12'),
(437, 10, 'slider', '1497353592_18.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:12', '2017-06-13 08:33:12'),
(438, 10, 'slider', '1497353592_19.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:12', '2017-06-13 08:33:12'),
(439, 10, 'slider', '1497353592_20.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:12', '2017-06-13 08:33:12'),
(440, 10, 'slider', '1497353592_21.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:12', '2017-06-13 08:33:12'),
(441, 10, 'slider', '1497353592_22.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:12', '2017-06-13 08:33:12'),
(442, 10, 'slider', '1497353592_23.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:13', '2017-06-13 08:33:13'),
(443, 10, 'slider', '1497353593_24.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:13', '2017-06-13 08:33:13'),
(444, 10, 'slider', '1497353593_25.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:13', '2017-06-13 08:33:13'),
(445, 10, 'slider', '1497353593_26.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:13', '2017-06-13 08:33:13'),
(446, 10, 'slider', '1497353593_27.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:13', '2017-06-13 08:33:13'),
(447, 10, 'slider', '1497353593_28.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:14', '2017-06-13 08:33:14'),
(448, 10, 'slider', '1497353594_29.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:14', '2017-06-13 08:33:14'),
(449, 10, 'slider', '1497353594_30.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:14', '2017-06-13 08:33:14'),
(450, 10, 'slider', '1497353594_31.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:14', '2017-06-13 08:33:14'),
(451, 10, 'slider', '1497353594_32.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:14', '2017-06-13 08:33:14'),
(452, 10, 'slider', '1497353594_33.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:15', '2017-06-13 08:33:15'),
(453, 10, 'slider', '1497353595_34.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:15', '2017-06-13 08:33:15'),
(454, 10, 'slider', '1497353595_35.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:15', '2017-06-13 08:33:15'),
(455, 10, 'slider', '1497353595_36.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:15', '2017-06-13 08:33:15'),
(456, 10, 'slider', '1497353595_37.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:15', '2017-06-13 08:33:15'),
(457, 10, 'slider', '1497353595_38.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:16', '2017-06-13 08:33:16'),
(458, 10, 'slider', '1497353596_39.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:16', '2017-06-13 08:33:16'),
(459, 10, 'slider', '1497353596_40.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:16', '2017-06-13 08:33:16'),
(460, 10, 'slider', '1497353596_41.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:16', '2017-06-13 08:33:16'),
(461, 10, 'slider', '1497353596_42.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:17', '2017-06-13 08:33:17'),
(462, 10, 'slider', '1497353597_43.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:17', '2017-06-13 08:33:17'),
(463, 10, 'slider', '1497353597_44.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:17', '2017-06-13 08:33:17'),
(464, 10, 'slider', '1497353597_45.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:17', '2017-06-13 08:33:17'),
(465, 10, 'slider', '1497353597_46.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:17', '2017-06-13 08:33:17'),
(466, 10, 'slider', '1497353597_47.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:18', '2017-06-13 08:33:18'),
(467, 10, 'slider', '1497353598_48.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:18', '2017-06-13 08:33:18'),
(468, 10, 'slider', '1497353598_49.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:18', '2017-06-13 08:33:18'),
(469, 10, 'slider', '1497353598_50.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:18', '2017-06-13 08:33:18'),
(470, 10, 'slider', '1497353598_51.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:18', '2017-06-13 08:33:18'),
(471, 10, 'slider', '1497353598_52.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:18', '2017-06-13 08:33:18'),
(472, 10, 'slider', '1497353598_53.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:19', '2017-06-13 08:33:19'),
(473, 10, 'slider', '1497353599_54.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:19', '2017-06-13 08:33:19'),
(474, 10, 'slider', '1497353599_55.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:19', '2017-06-13 08:33:19'),
(475, 10, 'slider', '1497353599_56.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:19', '2017-06-13 08:33:19'),
(476, 10, 'slider', '1497353599_57.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:20', '2017-06-13 08:33:20'),
(477, 10, 'slider', '1497353600_58.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:20', '2017-06-13 08:33:20'),
(478, 10, 'slider', '1497353600_59.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:20', '2017-06-13 08:33:20'),
(479, 10, 'slider', '1497353600_60.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:20', '2017-06-13 08:33:20'),
(480, 10, 'slider', '1497353600_61.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:20', '2017-06-13 08:33:20'),
(481, 10, 'slider', '1497353600_62.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:21', '2017-06-13 08:33:21'),
(482, 10, 'slider', '1497353601_63.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:21', '2017-06-13 08:33:21'),
(483, 10, 'slider', '1497353601_64.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:21', '2017-06-13 08:33:21'),
(484, 10, 'slider', '1497353601_65.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:21', '2017-06-13 08:33:21'),
(485, 10, 'slider', '1497353601_66.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:21', '2017-06-13 08:33:21'),
(486, 10, 'slider', '1497353601_67.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:21', '2017-06-13 08:33:21'),
(487, 10, 'slider', '1497353601_68.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:22', '2017-06-13 08:33:22'),
(488, 10, 'slider', '1497353602_69.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:22', '2017-06-13 08:33:22'),
(489, 10, 'slider', '1497353602_70.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:22', '2017-06-13 08:33:22'),
(490, 10, 'slider', '1497353602_71.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:22', '2017-06-13 08:33:22'),
(491, 10, 'slider', '1497353602_72.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:22', '2017-06-13 08:33:22'),
(492, 10, 'slider', '1497353602_73.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:23', '2017-06-13 08:33:23'),
(493, 10, 'slider', '1497353603_74.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:23', '2017-06-13 08:33:23'),
(494, 10, 'slider', '1497353603_75.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:23', '2017-06-13 08:33:23'),
(495, 10, 'slider', '1497353603_76.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:23', '2017-06-13 08:33:23'),
(496, 10, 'slider', '1497353603_77.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:23', '2017-06-13 08:33:23'),
(497, 10, 'slider', '1497353603_78.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:24', '2017-06-13 08:33:24'),
(498, 10, 'slider', '1497353604_79.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:24', '2017-06-13 08:33:24'),
(499, 10, 'slider', '1497353604_80.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:24', '2017-06-13 08:33:24'),
(500, 10, 'slider', '1497353604_81.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:24', '2017-06-13 08:33:24'),
(501, 10, 'slider', '1497353604_82.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:24', '2017-06-13 08:33:24'),
(502, 10, 'slider', '1497353604_83.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:24', '2017-06-13 08:33:24'),
(503, 10, 'slider', '1497353604_84.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:25', '2017-06-13 08:33:25'),
(504, 10, 'slider', '1497353605_85.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:25', '2017-06-13 08:33:25'),
(505, 10, 'slider', '1497353605_86.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:25', '2017-06-13 08:33:25'),
(506, 10, 'slider', '1497353605_87.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:25', '2017-06-13 08:33:25'),
(507, 10, 'slider', '1497353605_88.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:26', '2017-06-13 08:33:26'),
(508, 10, 'slider', '1497353606_89.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:26', '2017-06-13 08:33:26'),
(509, 10, 'slider', '1497353606_90.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-06-13 08:33:26', '2017-06-13 08:33:26'),
(510, 8, 'gallery', '1499246971_1.jpg', '', '', '', '', '', '', '', 0, 1, 1, '2017-07-05 06:29:32', '2017-07-05 06:29:32'),
(511, 8, 'gallery', '1499246972_2.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:29:32', '2017-07-05 06:29:32'),
(512, 8, 'gallery', '1499246972_3.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:29:32', '2017-07-05 06:29:32'),
(513, 8, 'gallery', '1499246972_4.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:29:33', '2017-07-05 06:29:33'),
(514, 8, 'gallery', '1499246973_5.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:29:33', '2017-07-05 06:29:33'),
(515, 8, 'gallery', '1499246973_6.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:29:33', '2017-07-05 06:29:33'),
(516, 8, 'gallery', '1499246973_7.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:29:34', '2017-07-05 06:29:34'),
(517, 8, 'gallery', '1499246974_8.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:29:34', '2017-07-05 06:29:34'),
(518, 8, 'slider', '1499247113_1+.jpg', '', '', '', '', '', '', '', 0, 1, 1, '2017-07-05 06:31:53', '2017-07-05 06:31:53'),
(519, 8, 'slider', '1499247113_2+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:54', '2017-07-05 06:31:54'),
(520, 8, 'slider', '1499247114_3+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:54', '2017-07-05 06:31:54'),
(521, 8, 'slider', '1499247114_4+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:55', '2017-07-05 06:31:55'),
(522, 8, 'slider', '1499247115_5+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:55', '2017-07-05 06:31:55'),
(523, 8, 'slider', '1499247115_6+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:56', '2017-07-05 06:31:56'),
(524, 8, 'slider', '1499247116_7+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:56', '2017-07-05 06:31:56'),
(525, 8, 'slider', '1499247116_8+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:56', '2017-07-05 06:31:56'),
(526, 8, 'slider', '1499247116_9+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:57', '2017-07-05 06:31:57'),
(527, 8, 'slider', '1499247117_10+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:57', '2017-07-05 06:31:57'),
(528, 8, 'slider', '1499247117_11+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:58', '2017-07-05 06:31:58'),
(529, 8, 'slider', '1499247118_12+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:58', '2017-07-05 06:31:58'),
(530, 8, 'slider', '1499247118_13+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:59', '2017-07-05 06:31:59'),
(531, 8, 'slider', '1499247119_14+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:59', '2017-07-05 06:31:59'),
(532, 8, 'slider', '1499247119_15+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:31:59', '2017-07-05 06:31:59'),
(533, 8, 'slider', '1499247119_16+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:00', '2017-07-05 06:32:00'),
(534, 8, 'slider', '1499247120_17+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:00', '2017-07-05 06:32:00'),
(535, 8, 'slider', '1499247120_18+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:00', '2017-07-05 06:32:00'),
(536, 8, 'slider', '1499247120_19+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:01', '2017-07-05 06:32:01'),
(537, 8, 'slider', '1499247121_20+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:01', '2017-07-05 06:32:01'),
(538, 8, 'slider', '1499247121_21+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:01', '2017-07-05 06:32:01'),
(539, 8, 'slider', '1499247121_22+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:02', '2017-07-05 06:32:02'),
(540, 8, 'slider', '1499247122_23+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:02', '2017-07-05 06:32:02'),
(541, 8, 'slider', '1499247122_24+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:02', '2017-07-05 06:32:02'),
(542, 8, 'slider', '1499247122_25+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:03', '2017-07-05 06:32:03'),
(543, 8, 'slider', '1499247123_26+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:03', '2017-07-05 06:32:03'),
(544, 8, 'slider', '1499247123_27+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:03', '2017-07-05 06:32:03'),
(545, 8, 'slider', '1499247123_28+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:03', '2017-07-05 06:32:03'),
(546, 8, 'slider', '1499247123_29+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:04', '2017-07-05 06:32:04'),
(547, 8, 'slider', '1499247124_30+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:04', '2017-07-05 06:32:04'),
(548, 8, 'slider', '1499247124_31+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:04', '2017-07-05 06:32:04'),
(549, 8, 'slider', '1499247124_32+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:04', '2017-07-05 06:32:04'),
(550, 8, 'slider', '1499247124_33+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:05', '2017-07-05 06:32:05'),
(551, 8, 'slider', '1499247125_34+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:05', '2017-07-05 06:32:05'),
(552, 8, 'slider', '1499247125_35+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:05', '2017-07-05 06:32:05'),
(553, 8, 'slider', '1499247125_36+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:06', '2017-07-05 06:32:06'),
(554, 8, 'slider', '1499247126_37+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:06', '2017-07-05 06:32:06'),
(555, 8, 'slider', '1499247126_38+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:07', '2017-07-05 06:32:07'),
(556, 8, 'slider', '1499247127_39+.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-07-05 06:32:07', '2017-07-05 06:32:07'),
(557, 13, 'gallery', '1502604619_1-min.jpg', '', '', '', '', '', '', '', 0, 1, 1, '2017-08-13 03:10:19', '2017-08-13 03:10:19'),
(558, 13, 'gallery', '1502604619_2-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:10:19', '2017-08-13 03:10:19'),
(559, 13, 'gallery', '1502604619_3-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:10:19', '2017-08-13 03:10:19'),
(560, 13, 'gallery', '1502604619_4-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:10:20', '2017-08-13 03:10:20'),
(561, 13, 'gallery', '1502604620_5-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:10:20', '2017-08-13 03:10:20'),
(562, 13, 'gallery', '1502604620_6-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:10:20', '2017-08-13 03:10:20'),
(563, 13, 'gallery', '1502604620_7-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:10:21', '2017-08-13 03:10:21'),
(564, 13, 'gallery', '1502604621_8-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:10:21', '2017-08-13 03:10:21'),
(565, 13, 'slider', '1502604718_foto-nije-glavnoy.jpg', '', '', '', '', '', '', '', 0, 1, 1, '2017-08-13 03:12:03', '2017-08-13 03:12:11'),
(566, 13, 'slider', '1502604788_1-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:08', '2017-08-13 03:13:08'),
(567, 13, 'slider', '1502604788_2-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:09', '2017-08-13 03:13:09'),
(568, 13, 'slider', '1502604789_3-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:09', '2017-08-13 03:13:09'),
(569, 13, 'slider', '1502604789_4-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:09', '2017-08-13 03:13:09'),
(570, 13, 'slider', '1502604789_5-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:09', '2017-08-13 03:13:09'),
(571, 13, 'slider', '1502604789_6-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:10', '2017-08-13 03:13:10'),
(572, 13, 'slider', '1502604790_7-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:10', '2017-08-13 03:13:10'),
(573, 13, 'slider', '1502604790_8-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:10', '2017-08-13 03:13:10'),
(574, 13, 'slider', '1502604790_9-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:10', '2017-08-13 03:13:10'),
(575, 13, 'slider', '1502604790_10-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:11', '2017-08-13 03:13:11'),
(576, 13, 'slider', '1502604791_11-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:11', '2017-08-13 03:13:11'),
(577, 13, 'slider', '1502604791_12-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:11', '2017-08-13 03:13:11'),
(578, 13, 'slider', '1502604791_13-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:11', '2017-08-13 03:13:11'),
(579, 13, 'slider', '1502604791_14-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:12', '2017-08-13 03:13:12'),
(580, 13, 'slider', '1502604792_15-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:12', '2017-08-13 03:13:12'),
(581, 13, 'slider', '1502604792_16-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:12', '2017-08-13 03:13:12'),
(582, 13, 'slider', '1502604792_17-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:12', '2017-08-13 03:13:12'),
(583, 13, 'slider', '1502604792_18-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:13', '2017-08-13 03:13:13'),
(584, 13, 'slider', '1502604793_19-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:13', '2017-08-13 03:13:13'),
(585, 13, 'slider', '1502604793_20-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:13:13', '2017-08-13 03:13:13'),
(598, 14, 'gallery', '1502605694_1-min.jpg', '', '', '', '', '', '', '', 0, 1, 1, '2017-08-13 03:28:18', '2017-08-13 03:28:18'),
(599, 14, 'gallery', '1502605698_2-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:28:23', '2017-08-13 03:28:23'),
(600, 14, 'gallery', '1502605703_3-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:28:27', '2017-08-13 03:28:27'),
(601, 14, 'gallery', '1502605707_4-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:28:32', '2017-08-13 03:28:32'),
(602, 14, 'gallery', '1502605730_5-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:28:55', '2017-08-13 03:28:55'),
(603, 14, 'gallery', '1502605735_6-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:28:59', '2017-08-13 03:28:59'),
(604, 14, 'gallery', '1502605739_7-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:29:03', '2017-08-13 03:29:03'),
(605, 14, 'gallery', '1502605743_8-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:29:06', '2017-08-13 03:29:06'),
(606, 14, 'slider', '1502605787_foto-nije-glavnoy.jpg', '', '', '', '', '', '', '', 0, 1, 1, '2017-08-13 03:29:47', '2017-08-13 03:29:47'),
(607, 14, 'slider', '1502605801_1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:02', '2017-08-13 03:30:02'),
(608, 14, 'slider', '1502605802_2-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:02', '2017-08-13 03:30:02'),
(609, 14, 'slider', '1502605802_3-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:02', '2017-08-13 03:30:02'),
(610, 14, 'slider', '1502605802_4-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:03', '2017-08-13 03:30:03'),
(611, 14, 'slider', '1502605803_5f-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:03', '2017-08-13 03:30:03'),
(612, 14, 'slider', '1502605803_5-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:03', '2017-08-13 03:30:03'),
(613, 14, 'slider', '1502605803_6f-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:03', '2017-08-13 03:30:03'),
(614, 14, 'slider', '1502605803_6-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:04', '2017-08-13 03:30:04'),
(615, 14, 'slider', '1502605804_7-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:04', '2017-08-13 03:30:04'),
(616, 14, 'slider', '1502605804_7-2-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:04', '2017-08-13 03:30:04'),
(617, 14, 'slider', '1502605804_7-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:05', '2017-08-13 03:30:05'),
(618, 14, 'slider', '1502605805_8-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:05', '2017-08-13 03:30:05'),
(619, 14, 'slider', '1502605805_9-1-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:05', '2017-08-13 03:30:05'),
(620, 14, 'slider', '1502605805_9-2-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:06', '2017-08-13 03:30:06'),
(621, 14, 'slider', '1502605806_10-min.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:30:06', '2017-08-13 03:30:06'),
(622, 14, 'slider', '1502605870_16.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:10', '2017-08-13 03:31:10'),
(623, 14, 'slider', '1502605870_16-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:10', '2017-08-13 03:31:10'),
(624, 14, 'slider', '1502605870_17.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:11', '2017-08-13 03:31:11'),
(625, 14, 'slider', '1502605871_19.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:11', '2017-08-13 03:31:11'),
(626, 14, 'slider', '1502605871_19-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:11', '2017-08-13 03:31:11'),
(627, 14, 'slider', '1502605871_20.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:12', '2017-08-13 03:31:12'),
(628, 14, 'slider', '1502605872_20-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:12', '2017-08-13 03:31:12'),
(629, 14, 'slider', '1502605872_21.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:12', '2017-08-13 03:31:12'),
(630, 14, 'slider', '1502605872_22-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:12', '2017-08-13 03:31:12'),
(631, 14, 'slider', '1502605872_23-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:13', '2017-08-13 03:31:13'),
(632, 14, 'slider', '1502605873_24.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:13', '2017-08-13 03:31:13'),
(633, 14, 'slider', '1502605873_43.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:13', '2017-08-13 03:31:13'),
(634, 14, 'slider', '1502605873_44.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:13', '2017-08-13 03:31:13'),
(635, 14, 'slider', '1502605873_45.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:14', '2017-08-13 03:31:14'),
(636, 14, 'slider', '1502605874_46.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:14', '2017-08-13 03:31:14'),
(637, 14, 'slider', '1502605874_46-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:14', '2017-08-13 03:31:14'),
(638, 14, 'slider', '1502605874_48.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:14', '2017-08-13 03:31:14'),
(639, 14, 'slider', '1502605874_49.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:15', '2017-08-13 03:31:15'),
(640, 14, 'slider', '1502605875_60.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:15', '2017-08-13 03:31:15'),
(641, 14, 'slider', '1502605875_61.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:15', '2017-08-13 03:31:15'),
(642, 14, 'slider', '1502605875_62.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:31:16', '2017-08-13 03:31:16'),
(643, 2, 'gallery', '1502606154_1.jpg', '', '', '', '', '', '', '', 0, 1, 1, '2017-08-13 03:35:54', '2017-08-13 03:35:54'),
(644, 2, 'gallery', '1502606154_2.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:35:55', '2017-08-13 03:35:55'),
(645, 2, 'gallery', '1502606155_3.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:35:55', '2017-08-13 03:35:55'),
(646, 2, 'gallery', '1502606155_4.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:35:55', '2017-08-13 03:35:55'),
(647, 2, 'gallery', '1502606155_5.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:35:56', '2017-08-13 03:35:56'),
(648, 2, 'gallery', '1502606156_6.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:35:56', '2017-08-13 03:35:56'),
(649, 2, 'gallery', '1502606156_7.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:35:56', '2017-08-13 03:35:56'),
(650, 2, 'gallery', '1502606156_8.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:35:57', '2017-08-13 03:35:57'),
(651, 2, 'slider', '1502606175_foto-nije-glavnoy.jpg', '', '', '', '', '', '', '', 0, 1, 1, '2017-08-13 03:36:16', '2017-08-13 03:36:16'),
(652, 2, 'slider', '1502606185_1-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:26', '2017-08-13 03:36:26'),
(653, 2, 'slider', '1502606186_2-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:26', '2017-08-13 03:36:26'),
(654, 2, 'slider', '1502606186_3-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:26', '2017-08-13 03:36:26'),
(655, 2, 'slider', '1502606186_4-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:27', '2017-08-13 03:36:27'),
(656, 2, 'slider', '1502606187_5-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:27', '2017-08-13 03:36:27'),
(657, 2, 'slider', '1502606187_6-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:27', '2017-08-13 03:36:27'),
(658, 2, 'slider', '1502606187_7-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:28', '2017-08-13 03:36:28'),
(659, 2, 'slider', '1502606188_8-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:28', '2017-08-13 03:36:28'),
(660, 2, 'slider', '1502606188_8-3.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:28', '2017-08-13 03:36:28'),
(661, 2, 'slider', '1502606188_8-5.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:28', '2017-08-13 03:36:28'),
(662, 2, 'slider', '1502606188_9-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:29', '2017-08-13 03:36:29'),
(663, 2, 'slider', '1502606189_10-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:29', '2017-08-13 03:36:29'),
(664, 2, 'slider', '1502606200_11-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:40', '2017-08-13 03:36:40'),
(665, 2, 'slider', '1502606200_20-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:41', '2017-08-13 03:36:41'),
(666, 2, 'slider', '1502606201_21-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:41', '2017-08-13 03:36:41'),
(667, 2, 'slider', '1502606201_22-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:41', '2017-08-13 03:36:41'),
(668, 2, 'slider', '1502606201_23-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:42', '2017-08-13 03:36:42'),
(669, 2, 'slider', '1502606202_24-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:42', '2017-08-13 03:36:42'),
(670, 2, 'slider', '1502606202_25-1-.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:42', '2017-08-13 03:36:42'),
(671, 2, 'slider', '1502606202_30-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:42', '2017-08-13 03:36:42'),
(672, 2, 'slider', '1502606202_31-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:43', '2017-08-13 03:36:43'),
(673, 2, 'slider', '1502606203_32-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:43', '2017-08-13 03:36:43'),
(674, 2, 'slider', '1502606203_33-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:43', '2017-08-13 03:36:43'),
(675, 2, 'slider', '1502606203_40-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:43', '2017-08-13 03:36:43'),
(676, 2, 'slider', '1502606203_41-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:44', '2017-08-13 03:36:44'),
(677, 2, 'slider', '1502606204_50-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-08-13 03:36:44', '2017-08-13 03:36:44'),
(686, 13, 'slider', '1504700583_21-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:03', '2017-09-06 09:23:03'),
(687, 13, 'slider', '1504700596_22-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:17', '2017-09-06 09:23:17'),
(688, 13, 'slider', '1504700615_23-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:35', '2017-09-06 09:23:35'),
(690, 13, 'slider', '1504700637_25-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:57', '2017-09-06 09:23:57'),
(691, 13, 'slider', '1504700637_26-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:57', '2017-09-06 09:23:57'),
(692, 13, 'slider', '1504700637_27-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:58', '2017-09-06 09:23:58'),
(693, 13, 'slider', '1504700638_28-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:58', '2017-09-06 09:23:58'),
(694, 13, 'slider', '1504700638_29-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:58', '2017-09-06 09:23:58'),
(695, 13, 'slider', '1504700638_30-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:58', '2017-09-06 09:23:58'),
(696, 13, 'slider', '1504700638_31-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:59', '2017-09-06 09:23:59'),
(697, 13, 'slider', '1504700639_32-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:59', '2017-09-06 09:23:59'),
(698, 13, 'slider', '1504700639_33-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:59', '2017-09-06 09:23:59'),
(699, 13, 'slider', '1504700639_34-1.jpg', '', '', '', '', '', '', '', 0, 1, 0, '2017-09-06 09:23:59', '2017-09-06 09:23:59'),
(713, 2, 'honors', '1505124123_diplom1-1.jpg', '', '', '', '', '', 'Connecting continents', 'Connecting continents', 0, 1, 1, '2017-09-11 07:02:03', '2017-10-05 08:17:47'),
(714, 10, 'honors', '1505129064_diplom2.jpg', '', '', '', '', '', 'Тест - 1', 'Test - 1', 0, 1, 0, '2017-09-11 08:24:25', '2017-10-05 08:19:12'),
(715, 10, 'honors', '1505129084_diplom3.jpg', '#', '', '', '', '', 'Тест - 2', 'Тest - 2', 0, 1, 0, '2017-09-11 08:24:44', '2017-10-05 08:32:19');

-- --------------------------------------------------------

--
-- Структура таблицы `as_publications`
--

CREATE TABLE IF NOT EXISTS `as_publications` (
  `id` int(10) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alt_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alt_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_publications`
--

INSERT INTO `as_publications` (`id`, `image`, `alt_ru`, `alt_en`, `title_ru`, `title_en`, `position`, `visible`, `created_at`, `updated_at`) VALUES
(9, '1492096563_t-article1-lg.jpg', '', '', '', '', 1, 1, '2017-04-13 12:16:03', '2017-04-27 07:25:14'),
(11, '1492096563_t-article3-lg.jpg', '', '', '', '', 3, 1, '2017-04-13 12:16:03', '2017-04-27 07:25:14'),
(12, '1492096563_t-article4-lg.jpg', '', '', '', '', 4, 1, '2017-04-13 12:16:04', '2017-04-13 12:37:30'),
(14, '1492096564_t-article6-lg.jpg', '', '', '', '', 2, 1, '2017-04-13 12:16:04', '2017-04-27 07:25:14');

-- --------------------------------------------------------

--
-- Структура таблицы `as_settings`
--

CREATE TABLE IF NOT EXISTS `as_settings` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_settings`
--

INSERT INTO `as_settings` (`id`, `email`, `phone`, `address_ru`, `address_en`, `facebook`, `youtube`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 'algrabovskiy@gmail.com', '+38 (098) 071-70-70', 'Днепр, Д. Яворницкого 42', 'Dnipro, D. Yavornytsky 42', 'https://www.facebook.com/algrabovskiy/', 'https://www.youtube.com/channel/UCRgbmuCwxNu8MpdIVZ9Hm0A', 'https://www.instagram.com/aleksgrabovskiy/', '0000-00-00 00:00:00', '2017-08-28 12:48:36');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_01_31_140111_create_settings_table', 1),
('2017_01_31_140911_create_main_page_table', 1),
('2017_02_04_134843_create_menu_table', 1),
('2017_04_03_080248_create_projects_categories_table', 2),
('2017_04_04_085920_create_projects_table', 3),
('2017_04_06_103447_create_projects_images_table', 4),
('2017_04_10_100514_create_pages_table', 5),
('2017_04_10_125106_create_publications_table', 6),
('2017_04_11_091043_create_feedbacks_table', 7),
('2017_04_24_071655_create_about_page_table', 8);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'test@test.com', '$2y$10$24qDrt5KTpLHxJoMyert.OCj7goTKovY0qeJPKv1hswuga2dk196C', 'PBmvsqcufRHOW424EOSvSW91vraMwuSJqaqHI7WXemPuwdBjDi9qVWCB3aqh', '2017-02-27 10:30:44', '2017-09-19 14:14:19');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `as_about_page`
--
ALTER TABLE `as_about_page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_feedbacks`
--
ALTER TABLE `as_feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_main_page`
--
ALTER TABLE `as_main_page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_menu`
--
ALTER TABLE `as_menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_pages`
--
ALTER TABLE `as_pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_projects`
--
ALTER TABLE `as_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_projects_category_id_foreign` (`category_id`);

--
-- Индексы таблицы `as_projects_categories`
--
ALTER TABLE `as_projects_categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_projects_images`
--
ALTER TABLE `as_projects_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_projects_images_project_id_foreign` (`project_id`);

--
-- Индексы таблицы `as_publications`
--
ALTER TABLE `as_publications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_settings`
--
ALTER TABLE `as_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `as_about_page`
--
ALTER TABLE `as_about_page`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `as_feedbacks`
--
ALTER TABLE `as_feedbacks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `as_main_page`
--
ALTER TABLE `as_main_page`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `as_menu`
--
ALTER TABLE `as_menu`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `as_pages`
--
ALTER TABLE `as_pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `as_projects`
--
ALTER TABLE `as_projects`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `as_projects_categories`
--
ALTER TABLE `as_projects_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `as_projects_images`
--
ALTER TABLE `as_projects_images`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=716;
--
-- AUTO_INCREMENT для таблицы `as_publications`
--
ALTER TABLE `as_publications`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `as_settings`
--
ALTER TABLE `as_settings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `as_projects`
--
ALTER TABLE `as_projects`
  ADD CONSTRAINT `as_projects_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `as_projects_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_projects_images`
--
ALTER TABLE `as_projects_images`
  ADD CONSTRAINT `as_projects_images_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `as_projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
